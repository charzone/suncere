package com.suncere.redis.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

/**
 * redis配置类
 * 
 * @since	2020年1月15日 下午3:38:34	<br>
 * @author	chencz
 */
@Configuration
public class RedisConfig {
	private static final Log log = LogFactory.getLog(RedisConfig.class);

	@Autowired
	private RedisConfigProperties redisConfigProperties;

	/**
	 * 配置JedisPoolConfig
	 * 
	 * @return JedisPoolConfig实体
	 */
	@Bean(name = "jedisPoolConfig")
	public JedisPoolConfig jedisPoolConfig() {
		log.info("初始化JedisPoolConfig");
		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
		jedisPoolConfig.setMaxTotal(redisConfigProperties.getPoolMaxActive()); // 连接池最大连接数（使用负值表示没有限制）
		jedisPoolConfig.setMaxWaitMillis(redisConfigProperties.getPoolMaxWait()); // 连接池最大阻塞等待时间（使用负值表示没有限制）
		jedisPoolConfig.setMaxIdle(redisConfigProperties.getPoolMaxIdle()); // 连接池中的最大空闲连接
		jedisPoolConfig.setMinIdle(redisConfigProperties.getPoolMinIdle()); // 连接池中的最小空闲连接
//        jedisPoolConfig.setTestOnBorrow(true);
//        jedisPoolConfig.setTestOnCreate(true);
//        jedisPoolConfig.setTestWhileIdle(true);
		return jedisPoolConfig;
	}

	/**
	 * 实例化 RedisConnectionFactory 对象
	 * 
	 * @param poolConfig
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@Bean(name = "jedisConnectionFactory")
	public RedisConnectionFactory jedisConnectionFactory(
			@Qualifier(value = "jedisPoolConfig") JedisPoolConfig poolConfig) {
		log.info("初始化RedisConnectionFactory");
		JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory(poolConfig);
		jedisConnectionFactory.setHostName(redisConfigProperties.getHost());
		jedisConnectionFactory.setPort(redisConfigProperties.getPort());
		jedisConnectionFactory.setDatabase(redisConfigProperties.getDatabase());
		jedisConnectionFactory.setPassword(redisConfigProperties.getPassword());
		return jedisConnectionFactory;
	}

	/**
	 * 实例化 RedisTemplate 对象
	 * 
	 * @return
	 */
	@Bean(name = "redisTemplate")
	public StringRedisTemplate functionDomainRedisTemplate(
			@Qualifier(value = "jedisConnectionFactory") RedisConnectionFactory factory) {
		log.info("初始化RedisTemplate");
		StringRedisTemplate redisTemplate = new StringRedisTemplate();
		redisTemplate.setConnectionFactory(factory);
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		redisTemplate.setHashKeySerializer(new StringRedisSerializer());
//		redisTemplate.setHashValueSerializer(new EntityRedisSerializer());
//		redisTemplate.setValueSerializer(new EntityRedisSerializer());
		redisTemplate.afterPropertiesSet();
		redisTemplate.setEnableTransactionSupport(true);
		return redisTemplate;
	}
}
