package com.suncere.redis.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * redis配置文件映射类
 * 
 * @since	2020年1月15日 下午3:39:02	<br>
 * @author	chencz
 */
@Component
@ConfigurationProperties(prefix = "redis")
@PropertySource(value = "classpath:redis.properties")
public class RedisConfigProperties {

	/**
	 * 主机
	 */
	private String host;

	/**
	 * 端口
	 */
	private Integer port;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 数据库索引
	 */
	private Integer database;

	/**
	 * 连接池最大连接数（使用负值表示没有限制）
	 */
	private Integer poolMaxActive;

	/**
	 * 连接池最大阻塞等待时间（使用负值表示没有限制）
	 */
	private Integer poolMaxWait;

	/**
	 * 连接池中的最大空闲连接
	 */
	private Integer poolMaxIdle;

	/**
	 * 连接池中的最小空闲连接
	 */
	private Integer poolMinIdle;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getDatabase() {
		return database;
	}

	public void setDatabase(Integer database) {
		this.database = database;
	}

	public Integer getPoolMaxActive() {
		return poolMaxActive;
	}

	public void setPoolMaxActive(Integer poolMaxActive) {
		this.poolMaxActive = poolMaxActive;
	}

	public Integer getPoolMaxWait() {
		return poolMaxWait;
	}

	public void setPoolMaxWait(Integer poolMaxWait) {
		this.poolMaxWait = poolMaxWait;
	}

	public Integer getPoolMaxIdle() {
		return poolMaxIdle;
	}

	public void setPoolMaxIdle(Integer poolMaxIdle) {
		this.poolMaxIdle = poolMaxIdle;
	}

	public Integer getPoolMinIdle() {
		return poolMinIdle;
	}

	public void setPoolMinIdle(Integer poolMinIdle) {
		this.poolMinIdle = poolMinIdle;
	}
}
