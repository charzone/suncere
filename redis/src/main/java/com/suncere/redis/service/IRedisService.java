package com.suncere.redis.service;

import org.springframework.data.geo.Point;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis接口服务类
 * 
 * @since 2020年1月15日 下午3:32:45 <br>
 * @author chencz
 */
public interface IRedisService {

	/**
	   *存储值
	 * 
	 * @param key
	 * @param value
	 */
	public void setValue(String key, String value);

	/**
	 * 获取值
	 * 
	 * @param key
	 * @return
	 */
	public String getValue(String key);

	/**
	 * 得到list所有数据
	 * 
	 * @param key
	 * @return
	 */
	public List<String> getList(String key);

	/**
	 * list 左边出
	 * 
	 * @Title: leftPopList
	 * @Description:
	 * @param key
	 * @return
	 */
	public String leftPopList(String key);

	/**
	 * list 右边出
	 * 
	 * @Title: rightPopList
	 * @Description:
	 * @param key
	 * @return
	 */
	public String rightPopList(String key);

	/**
	 * 得到所有数据
	 * 
	 * @param key
	 * @return
	 */
	public Set<String> smembers(String key);

	/**
	 * list 左边入
	 * 
	 * @Title: leftPopList
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param key
	 * @param value
	 * @return
	 */
	public void leftPushList(String key, String value);

	/**
	 * list 右边入
	 * 
	 * @Title: rightPushList
	 * @Description:
	 * @param key
	 * @param value
	 * @return
	 */
	public void rightPushList(String key, String value);

	/**
	 * 判断 member 元素是否是集合 key 的成员
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	public Boolean sismember(String key, String member);

	/**
	 * 判断是否存在当前key
	 * 
	 * @param key
	 * @return
	 */
	public Boolean exists(String key);

	/**
	 * 给key设定值，设定保存时间（单位：毫秒）
	 * 
	 * @param key
	 * @param value
	 * @param pointOfflineTime 值存在多久
	 * @param unit	时间单位，参考{@link java.util.concurrent.TimeUnit}，不填默认毫秒
	 */
	public void setValueExistSomeTime(String key, String value, Long pointOfflineTime, TimeUnit ...unit);

	/**
	 * 获取指定key的过期时间，可指定换算时间单位
	 *  @param key
	 * @param unit    时间单位，参考{@link TimeUnit}，不填默认毫秒
	 * @return 如果该值没有设置过期时间，就返回-1;
	 *         如果没有该值，就返回-2;
	 */
	public Long getKeyExpireTime(String key, TimeUnit ...unit);
	/**
	 * 将key的set替换，原来的会丢失
	 * 
	 * @Title: setSet
	 * @Description:
	 * @param key key
	 * @param set
	 */
	public void setSet(String key, Set<String> set);

	/**
	 * 删除key
	 * 
	 * @param key
	 */
	public void deleteByKey(String key);

	/**
	 * 得到listd size
	 * 
	 * @param key
	 * @return
	 */
	public Long getListSize(String key);

	/**
	 * 得到某个key的所有属性及其值的Map<key,value>
	 * 
	 * @param key
	 * @return
	 */
	public Map<Object, Object> getHashKeyMap(String key);

	/**
	 * 设置谋个key的某个属性的值
	 * 
	 * @param key
	 * @param fieldValueMap
	 */
	public void setHashKeyMap(String key, Map<String, String> fieldValueMap);

	/**
	 * 添加地理点位信息
	 * 
	 * @param key
	 * @param memberCoordinateMap <名字，点位坐标>
	 */
	public void addGeoPoint(String key, Map<String, Point> memberCoordinateMap);
}