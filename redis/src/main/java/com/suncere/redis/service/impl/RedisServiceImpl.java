package com.suncere.redis.service.impl;

import com.suncere.redis.service.IRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.GeoOperations;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author luoleijun
 */
@Service
public class RedisServiceImpl implements IRedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 存储值
     *
     * @param key
     * @param value
     */
    @Override
    public void setValue(String key, String value) {
        stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * 获取值
     *
     * @param key
     * @return
     */
    @Override
    public String getValue(String key) {
        return stringRedisTemplate.opsForValue().get(key) == null ? "" : stringRedisTemplate.opsForValue().get(key);
    }

    @Override
    public List<String> getList(String key) {
        BoundListOperations<String, String> ops = stringRedisTemplate.boundListOps(key);
        List<String> values = ops.range(0, -1);
        return values;
    }

    @Override
    public String leftPopList(String key) {
        BoundListOperations<String, String> ops = stringRedisTemplate.boundListOps(key);
        return ops.leftPop();
    }

    @Override
    public String rightPopList(String key) {
        BoundListOperations<String, String> ops = stringRedisTemplate.boundListOps(key);
        return ops.rightPop();
    }

    @Override
    public Long getListSize(String key) {
        BoundListOperations<String, String> ops = stringRedisTemplate.boundListOps(key);
        return ops.size();
    }

    @Override
    public void leftPushList(String key, String value) {
        BoundListOperations<String, String> ops = stringRedisTemplate.boundListOps(key);
        ops.leftPush(value);
    }

    @Override
    public void rightPushList(String key, String value) {
        BoundListOperations<String, String> ops = stringRedisTemplate.boundListOps(key);
        ops.rightPush(value);
    }

    @Override
    public Boolean exists(String key) {
        return stringRedisTemplate.hasKey(key);
    }

    @Override
    public Set<String> smembers(String key) {
        Set<String> members = (Set<String>) stringRedisTemplate.opsForSet().members(key);
        return members;
    }

    @Override
    public Boolean sismember(String key, String member) {
        Boolean flag = stringRedisTemplate.boundSetOps(key).isMember(member);
        return flag;
    }

    @Override
    public void setValueExistSomeTime(String key, String value, Long pointOfflineTime, TimeUnit... unit) {
        if (unit.length <= 0) {
            stringRedisTemplate.opsForValue().set(key, value, pointOfflineTime, TimeUnit.MILLISECONDS);
        } else {
            stringRedisTemplate.opsForValue().set(key, value, pointOfflineTime, unit[0]);
        }
    }

    @Override
    public Long getKeyExpireTime(String key, TimeUnit... unit) {
        long expireTime;
        if (unit.length <= 0) {
            expireTime = stringRedisTemplate.getExpire(key, TimeUnit.MILLISECONDS);
        } else {
            expireTime = stringRedisTemplate.getExpire(key, unit[0]);
        }
        return expireTime;
    }

    @Override
    public void setSet(String key, Set<String> set) {
        Long size = stringRedisTemplate.opsForSet().size(key);
        if (size == null) {
            size = (long) 0;
        }
        stringRedisTemplate.opsForSet().pop(key, size);

        if (set != null && set.size() != 0) {
            Iterator<String> it = set.iterator();
            while (it.hasNext()) {
                stringRedisTemplate.opsForSet().add(key, it.next());
            }
        }

    }

    @Override
    public void deleteByKey(String key) {
        stringRedisTemplate.delete(key);
    }

    @Override
    public void setHashKeyMap(String key, Map<String, String> fieldValueMap) {
        HashOperations<String, Object, Object> opsForHash = stringRedisTemplate.opsForHash();
        opsForHash.putAll(key, fieldValueMap);
    }

    @Override
    public Map<Object, Object> getHashKeyMap(String key) {
        HashOperations<String, Object, Object> opsForHash = stringRedisTemplate.opsForHash();
        Map<Object, Object> ret = opsForHash.entries(key);
        return ret;
    }

    @Override
    public void addGeoPoint(String key, Map<String, Point> memberCoordinateMap) {
        GeoOperations<String, String> opsForGeo = stringRedisTemplate.opsForGeo();
        opsForGeo.add(key, memberCoordinateMap);
    }
}