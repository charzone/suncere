package com.suncere.statistic.mapper;

import java.util.ArrayList;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suncere.statistic.entity.ForecastPollutantRange;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author panxinkun
 * @since 2019-12-25
 */
public interface ForecastPollutantRangeMapper extends BaseMapper<ForecastPollutantRange> {

	ArrayList<ForecastPollutantRange> list();

}
