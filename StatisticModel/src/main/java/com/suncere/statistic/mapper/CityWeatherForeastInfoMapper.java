package com.suncere.statistic.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suncere.statistic.entity.CityWeatherForeastInfo;
import com.suncere.statistic.serivce.po.CityWeatherForeastInfoPo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author panxinkun
 * @since 2019-12-25
 */
public interface CityWeatherForeastInfoMapper extends BaseMapper<CityWeatherForeastInfo> {

	List<CityWeatherForeastInfo> list(CityWeatherForeastInfoPo po);

}
