package com.suncere.statistic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suncere.statistic.entity.CityMonitorDayPollutant;
import com.suncere.statistic.serivce.po.CityMonitorDayPollutantPo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author panxinkun
 * @since 2019-12-25
 */
public interface CityMonitorDayPollutantMapper extends BaseMapper<CityMonitorDayPollutant> {

    List<CityMonitorDayPollutant> list(CityMonitorDayPollutantPo po);

    List<CityMonitorDayPollutant> listCityAqiHistoryDayPublish(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("cityCode") String cityCode);
}
