package com.suncere.statistic.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suncere.statistic.entity.CityMonitorDayWeather;
import com.suncere.statistic.serivce.po.CityMonitorDayWeatherPo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author panxinkun
 * @since 2019-12-25
 */
public interface CityMonitorDayWeatherMapper extends BaseMapper<CityMonitorDayWeather> {

	List<CityMonitorDayWeather> list(CityMonitorDayWeatherPo po);

}
