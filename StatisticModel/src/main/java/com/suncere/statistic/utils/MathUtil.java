package com.suncere.statistic.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 数学计算工具类
 * </p>
 *
 * @author panxinkun
 * @date 2019年12月31日
 */
public class MathUtil {
	//此值表示空缺数据
	private static double nullValue = -99;
	
	/**
	 * 基于Comparable.compareTo的min
	 * @param <T>
	 * @param b1
	 * @param b2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T min(Comparable<T> b1, Comparable<T> b2) {
		if(b1.compareTo((T) b2) <= 0) {
			return (T) b1;
		}else {
			return (T) b2;
		}
	}
	/**
	 * 基于Comparable.compareTo的min
	 * @param <T>
	 * @param b1
	 * @param b2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T max(Comparable<T> b1, Comparable<T> b2) {
		if(b1.compareTo((T) b2) <= 0) {
			return (T) b2;
		}else {
			return (T) b1;
		}
	}
	
	/**
	 * 四舍六入五成双计算法
	 * 
	 * @param value 需要科学计算的数据
	 * @param digit 保留的小数位
	 * @return
	 */
	public static BigDecimal roundBank(double value, int digit) {
		double ratio = Math.pow(10, digit);
		double _num = value * ratio;
		double mod = _num % 1;
		double integer = Math.floor(_num);//向舍入直接取整100.675-->100.0
		double returnNum;
		if (mod > 0.5) {
			returnNum = (integer + 1) / ratio;
		} else if (mod < 0.5) {
			returnNum = integer / ratio;
		} else {
			returnNum = (integer % 2 == 0 ? integer : integer + 1) / ratio;
		}
		BigDecimal bg = new BigDecimal(returnNum);
		return bg.setScale((int) digit, BigDecimal.ROUND_HALF_UP);//四舍五入,2.35保留1位，变成2.4
	}
	
    /**
     * 计算方差
     * @param data
     * @return
     */
    public static double Variance(double[] data)
    {
		List<Double> dataList = new ArrayList<>();
		for(double d : data) {
			if(d!=nullValue) {//排除空值组成新数组
				dataList.add(d);
			}
		}
		double[] arr = new double[dataList.size()];
		for(int i=0; i<dataList.size(); i++) {
			arr[i] = dataList.get(i);
		}
		
		double sum = 0;
		for(double d : arr) {
			sum += d;
		}
		
        //排除空值
        double variance = 0, temp = 0;
        double count = arr.length;
        double avg = sum/count;
        //全部为空值 返回0
        if (count == 0)
            return variance;
        for (int i = 0; i < count; i++)
        {
            temp += Math.pow(arr[i] - avg, 2);
        }

        variance = temp / count;
        return variance;
    }
    
    /**
     * 计算标准差
     * @param data
     * @return
     */
    public static double StandardDeviation(double[] data)
    {
        double variance = Variance(data);
        double sd = Math.pow(variance, 0.5);
        return sd;
    }
}
