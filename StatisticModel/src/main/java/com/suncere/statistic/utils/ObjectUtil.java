package com.suncere.statistic.utils;

import org.springframework.beans.BeanUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 对象工具类
 * </p>
 *
 * @author panxinkun
 * @date 2019年11月13日
 */
public class ObjectUtil {
	/**
	 * 判断两个对象值是否相等：如果是BigDecimal则stripTrailingZeros后再比较
	 * @param obj1
	 * @param obj2
	 * @return
	 */
	public static boolean equals(Object obj1, Object obj2) {
		if(obj1 == null && obj2 == null) {
			return true;
		}
		if(obj1 == null && obj2 != null) {
			return false;
		}
		if(obj1 instanceof BigDecimal) {
			obj1 = ((BigDecimal) obj1).stripTrailingZeros();
		}
		if(obj2 instanceof BigDecimal) {
			obj2 = ((BigDecimal) obj2).stripTrailingZeros();
		}
		if (!obj1.equals(obj2)) {
			return false;
		}
		return true;
	}
	/**
	 * 判断两个对象值是否不相等
	 * @param obj1
	 * @param obj2
	 * @return
	 */
	public static boolean notEquals(Object obj1, Object obj2) {
		return !equals(obj1, obj2);
	}
	/**
	 * 去除后面零
	 * @param objs
	 * @return
	 */
	public static BigDecimal[] stripTrailingZeros(BigDecimal[] objs) {
		for(BigDecimal obj : objs) {
			if(obj!=null) {
				obj = obj.stripTrailingZeros(); 
			}
		}
		return objs;
	}
	
	/**
	 * 复制属性
	 * @param
	 * @return
	 */
	public static void copyProperties(Object source, Object target) {
		BeanUtils.copyProperties(source, target);
		//处理date转localDateTime
		Field[] sourceFields = source.getClass().getDeclaredFields();
		Field[] targetFields = target.getClass().getDeclaredFields();
		for (Field sourceField : sourceFields) {
			sourceField.setAccessible(true);
			try {
				if (sourceField.getGenericType().toString().equals("class java.util.Date")){
					for (Field targetField : targetFields) {
						targetField.setAccessible(true);
						if (targetField.getName().equals(sourceField.getName())){
							if (targetField.getGenericType().toString().equals("class java.time.LocalDateTime")){
								Date sourceDate = (Date) sourceField.get(source);
								targetField.set(target, Instant.ofEpochMilli(sourceDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime());
							}
						}
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 复制转换列表
	 * @param <T>
	 * @param <B>
	 * @param list
	 * @param
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	public static <T,B> List<B> copyList(List<T> list, Class<B> cls) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		List<B> bList = new ArrayList<>();
		for(T t : list) {
			B obj = cls.newInstance();
			copyProperties(t, obj);
			bList.add(obj);
		}
		return bList;
	}
	
}
