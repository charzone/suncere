package com.suncere.statistic.utils;

import java.time.*;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>
 * 时间工具类
 * </p>
 *
 * @author panxinkun
 * @date 2019年12月25日
 */
public class DateUtils {

	/**
	 * 时区
	 */
	public static final ZoneOffset ZONEOFFSET = ZoneOffset.of("+8");

	/**
	 * 将时间戳转换成LocalDateTime对象
	 *
	 * @param timestamp 时间戳
	 * @return
	 */
	public static LocalDateTime exchangeToLocalDateTime(Long timestamp) {
		return LocalDateTime.ofEpochSecond(timestamp / 1000, 0, ZONEOFFSET);
	}

	/**
	 * 将LocalDateTime转为Date
	 * @param date	LocalDateTime
	 * @return
	 */
	public static Date localDateTimeToDate(LocalDateTime date) {
		return Date.from( date.atZone( ZoneId.systemDefault()).toInstant());
	}

	public static boolean isSameDay(LocalDateTime date1, LocalDateTime date2) {
		if(date1 != null && date2 != null) {
			return date1.getYear() ==date2.getYear() && date1.getMonthValue() == date2.getMonthValue() && date1.getDayOfMonth() == date2.getDayOfMonth();
		} else {
			throw new IllegalArgumentException("The date must not be null");
		}
	}
    public static boolean isSameDay(Date date1, Date date2) {
        if(date1 != null && date2 != null) {
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(date1);
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date2);
            return isSameDay(cal1, cal2);
        } else {
            throw new IllegalArgumentException("The date must not be null");
        }
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if(cal1 != null && cal2 != null) {
            return cal1.get(0) == cal2.get(0) && cal1.get(1) == cal2.get(1) && cal1.get(6) == cal2.get(6);
        } else {
            throw new IllegalArgumentException("The date must not be null");
        }
    }

	/**
	 * Date 和 LocalDate,LocalDateTime的相互转换
	 * @param localDate
	 * @return
	 */
	public static Date asDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public static Date asDate(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	public static LocalDate asLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public static LocalDateTime asLocalDateTime(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
	}
}
