package com.suncere.statistic.utils;

import java.math.BigDecimal;

public final class StringExtension {
	/// #region 类型转换

	/**
	 * 转换为实数
	 * 
	 * @param source
	 * @param defaultValue
	 * @return
	 */
	public static BigDecimal GetDecemal(String source, BigDecimal defaultValue) {
		try {
			BigDecimal d = BigDecimal.valueOf(Double.parseDouble(source));
			return d;
		}catch (Exception e) {
			return defaultValue;
		}
	}
	
	/// #region 保留小数位
	/**
	 * 保留小数位
	 * 
	 * @param item         原始数值
	 * @param round        要保留的位数
	 * @param defaultValue 默认输出值
	 * @return
	 */
	public static BigDecimal DecimalRound(BigDecimal item, int round, BigDecimal defaultValue) {
		if (item == null) {
			return StringExtension.GetDecemal("0.000", new BigDecimal(0));
		}
		return MathUtil.roundBank(GetDecemal(item.toString(), defaultValue).doubleValue(), round);

	}

}