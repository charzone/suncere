package com.suncere.statistic.entity.EntityModel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class WeatherMonitorData {
    private LocalDateTime timePoint;
    private BigDecimal windDirection;
    private BigDecimal windSpeed;
    private BigDecimal pressure;
    private BigDecimal airTemperature;
    private BigDecimal humidity;
    private BigDecimal rainfall;
    private String mark;
    private BigDecimal apparent;

	public LocalDateTime getTimePoint() {
		return timePoint;
	}

	public void setTimePoint(LocalDateTime timePoint) {
		this.timePoint = timePoint;
	}

	public BigDecimal getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(BigDecimal windDirection) {
		this.windDirection = windDirection;
	}

	public BigDecimal getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(BigDecimal windSpeed) {
		this.windSpeed = windSpeed;
	}

	public BigDecimal getPressure() {
		return pressure;
	}

	public void setPressure(BigDecimal pressure) {
		this.pressure = pressure;
	}

	public BigDecimal getAirTemperature() {
		return airTemperature;
	}

	public void setAirTemperature(BigDecimal airTemperature) {
		this.airTemperature = airTemperature;
	}

	public BigDecimal getHumidity() {
		return humidity;
	}

	public void setHumidity(BigDecimal humidity) {
		this.humidity = humidity;
	}

	public BigDecimal getRainfall() {
		return rainfall;
	}

	public void setRainfall(BigDecimal rainfall) {
		this.rainfall = rainfall;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public BigDecimal getApparent() {
		return apparent;
	}

	public void setApparent(BigDecimal apparent) {
		this.apparent = apparent;
	}
}