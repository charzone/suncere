package com.suncere.statistic.entity.EntityModel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PollutantForecastMetaData {
	private LocalDateTime timePoint;
	private LocalDateTime forTime;
	private String polllutantName;
	private BigDecimal value;
	private BigDecimal floatingValue;
	public LocalDateTime getTimePoint() {
		return timePoint;
	}
	public void setTimePoint(LocalDateTime timePoint) {
		this.timePoint = timePoint;
	}
	public LocalDateTime getForTime() {
		return forTime;
	}
	public void setForTime(LocalDateTime forTime) {
		this.forTime = forTime;
	}
	public String getPolllutantName() {
		return polllutantName;
	}
	public void setPolllutantName(String polllutantName) {
		this.polllutantName = polllutantName;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public BigDecimal getFloatingValue() {
		return floatingValue;
	}
	public void setFloatingValue(BigDecimal floatingValue) {
		this.floatingValue = floatingValue;
	}
}