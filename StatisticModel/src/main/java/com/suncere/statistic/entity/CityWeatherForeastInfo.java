package com.suncere.statistic.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @since 2019-12-25
 */
public class CityWeatherForeastInfo extends Model<CityWeatherForeastInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.INPUT)
    private Integer Id;

    private Date timePoint;

    private Date forTime;

    private Date createTime;

    private String cityName;

    private String cityCode;

    private BigDecimal airTemp;

    private BigDecimal highAirTemp;

    private BigDecimal lowHighAirTemp;

    private String dayCondition;

    private String nightCondition;

    private BigDecimal dayWindDirection;

    private BigDecimal nightWindDirection;

    private String dayWindSpeed;

    private String nightWindSpeed;

    private BigDecimal windDirection;

    private BigDecimal windSpeed;

    private BigDecimal airPress;

    private BigDecimal relativeHumidity;

    private BigDecimal cloundCover;

    private BigDecimal visibility;

    private BigDecimal rainFall;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }
    public Date getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(Date timePoint) {
        this.timePoint = timePoint;
    }
    public Date getForTime() {
        return forTime;
    }

    public void setForTime(Date forTime) {
        this.forTime = forTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
    public BigDecimal getAirTemp() {
        return airTemp;
    }

    public void setAirTemp(BigDecimal airTemp) {
        this.airTemp = airTemp;
    }
    public BigDecimal getHighAirTemp() {
        return highAirTemp;
    }

    public void setHighAirTemp(BigDecimal highAirTemp) {
        this.highAirTemp = highAirTemp;
    }
    public BigDecimal getLowHighAirTemp() {
        return lowHighAirTemp;
    }

    public void setLowHighAirTemp(BigDecimal lowHighAirTemp) {
        this.lowHighAirTemp = lowHighAirTemp;
    }
    public String getDayCondition() {
        return dayCondition;
    }

    public void setDayCondition(String dayCondition) {
        this.dayCondition = dayCondition;
    }
    public String getNightCondition() {
        return nightCondition;
    }

    public void setNightCondition(String nightCondition) {
        this.nightCondition = nightCondition;
    }
    public BigDecimal getDayWindDirection() {
        return dayWindDirection;
    }

    public void setDayWindDirection(BigDecimal dayWindDirection) {
        this.dayWindDirection = dayWindDirection;
    }
    public BigDecimal getNightWindDirection() {
        return nightWindDirection;
    }

    public void setNightWindDirection(BigDecimal nightWindDirection) {
        this.nightWindDirection = nightWindDirection;
    }
    public String getDayWindSpeed() {
        return dayWindSpeed;
    }

    public void setDayWindSpeed(String dayWindSpeed) {
        this.dayWindSpeed = dayWindSpeed;
    }
    public String getNightWindSpeed() {
        return nightWindSpeed;
    }

    public void setNightWindSpeed(String nightWindSpeed) {
        this.nightWindSpeed = nightWindSpeed;
    }
    public BigDecimal getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(BigDecimal windDirection) {
        this.windDirection = windDirection;
    }
    public BigDecimal getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(BigDecimal windSpeed) {
        this.windSpeed = windSpeed;
    }
    public BigDecimal getAirPress() {
        return airPress;
    }

    public void setAirPress(BigDecimal airPress) {
        this.airPress = airPress;
    }
    public BigDecimal getRelativeHumidity() {
        return relativeHumidity;
    }

    public void setRelativeHumidity(BigDecimal relativeHumidity) {
        this.relativeHumidity = relativeHumidity;
    }
    public BigDecimal getCloundCover() {
        return cloundCover;
    }

    public void setCloundCover(BigDecimal cloundCover) {
        this.cloundCover = cloundCover;
    }
    public BigDecimal getVisibility() {
        return visibility;
    }

    public void setVisibility(BigDecimal visibility) {
        this.visibility = visibility;
    }
    public BigDecimal getRainFall() {
        return rainFall;
    }

    public void setRainFall(BigDecimal rainFall) {
        this.rainFall = rainFall;
    }

    @Override
    protected Serializable pkVal() {
        return this.Id;
    }

    @Override
    public String toString() {
        return "CityWeatherforeastinfo{" +
        "Id=" + Id +
        ", timePoint=" + timePoint +
        ", forTime=" + forTime +
        ", createTime=" + createTime +
        ", cityName=" + cityName +
        ", cityCode=" + cityCode +
        ", airTemp=" + airTemp +
        ", highAirTemp=" + highAirTemp +
        ", lowHighAirTemp=" + lowHighAirTemp +
        ", dayCondition=" + dayCondition +
        ", nightCondition=" + nightCondition +
        ", dayWindDirection=" + dayWindDirection +
        ", nightWindDirection=" + nightWindDirection +
        ", dayWindSpeed=" + dayWindSpeed +
        ", nightWindSpeed=" + nightWindSpeed +
        ", windDirection=" + windDirection +
        ", windSpeed=" + windSpeed +
        ", airPress=" + airPress +
        ", relativeHumidity=" + relativeHumidity +
        ", cloundCover=" + cloundCover +
        ", visibility=" + visibility +
        ", rainFall=" + rainFall +
        "}";
    }
}
