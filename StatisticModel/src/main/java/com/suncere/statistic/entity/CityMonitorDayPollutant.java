package com.suncere.statistic.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @since 2019-12-25
 */
public class CityMonitorDayPollutant extends Model<CityMonitorDayPollutant> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.AUTO)
    private Integer Id;
    private String cityCode;
    private LocalDateTime timePoint;
    private LocalDateTime createTime;
    private BigDecimal so2;
    private BigDecimal no2;
    private BigDecimal no;
    private BigDecimal co;
    private BigDecimal nox;
    private BigDecimal o3;
    private BigDecimal pm2_5;
    private BigDecimal pm10;
    private BigDecimal o3_8h;
    
    private Integer aqi;
    private String aqiLevel;
    private String primaryPollutant;
    private String mark;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }
    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
    public LocalDateTime getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(LocalDateTime timePoint) {
        this.timePoint = timePoint;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public BigDecimal getSo2() {
        return so2;
    }

    public void setSo2(BigDecimal so2) {
        this.so2 = so2;
    }
    public BigDecimal getNo2() {
        return no2;
    }

    public void setNo2(BigDecimal no2) {
        this.no2 = no2;
    }
    public BigDecimal getNo() {
        return no;
    }

    public void setNo(BigDecimal no) {
        this.no = no;
    }
    public BigDecimal getCo() {
        return co;
    }

    public void setCo(BigDecimal co) {
        this.co = co;
    }
    public BigDecimal getNox() {
        return nox;
    }

    public void setNox(BigDecimal nox) {
        this.nox = nox;
    }
    public BigDecimal getO3() {
        return o3;
    }

    public void setO3(BigDecimal o3) {
        this.o3 = o3;
    }
    
    public BigDecimal getPm2_5() {
		return pm2_5;
	}

	public void setPm2_5(BigDecimal pm2_5) {
		this.pm2_5 = pm2_5;
	}

	public BigDecimal getPm10() {
        return pm10;
    }

    public void setPm10(BigDecimal pm10) {
        this.pm10 = pm10;
    }
    public BigDecimal getO3_8h() {
		return o3_8h;
	}

	public void setO3_8h(BigDecimal o3_8h) {
		this.o3_8h = o3_8h;
	}

	public Integer getAqi() {
        return aqi;
    }

    public void setAqi(Integer aqi) {
        this.aqi = aqi;
    }
    public String getAqiLevel() {
        return aqiLevel;
    }

    public void setAqiLevel(String aqiLevel) {
        this.aqiLevel = aqiLevel;
    }
    public String getPrimaryPollutant() {
        return primaryPollutant;
    }

    public void setPrimaryPollutant(String primaryPollutant) {
        this.primaryPollutant = primaryPollutant;
    }
    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    protected Serializable pkVal() {
        return this.Id;
    }

    @Override
    public String toString() {
        return "CityMonitorDayPollutant{" +
        "Id=" + Id +
        ", cityCode=" + cityCode +
        ", timePoint=" + timePoint +
        ", createTime=" + createTime +
        ", so2=" + so2 +
        ", no2=" + no2 +
        ", no=" + no +
        ", co=" + co +
        ", nox=" + nox +
        ", o3=" + o3 +
        ", pm2_5=" + pm2_5 +
        ", pm10=" + pm10 +
        ", o3_8h=" + o3_8h +
        ", aqi=" + aqi +
        ", aqiLevel=" + aqiLevel +
        ", primaryPollutant=" + primaryPollutant +
        ", mark=" + mark +
        "}";
    }
}
