package com.suncere.statistic.entity.EntityModel;

import java.math.BigDecimal;

/**
 * 污染物预报浓度值范围设置
 */
public class ForecastPollutantRangeDto {
	private String pollutant;
	private BigDecimal upperLimit;
	private BigDecimal lowerLimit;
	private Boolean status;
	public String getPollutant() {
		return pollutant;
	}
	public void setPollutant(String pollutant) {
		this.pollutant = pollutant;
	}
	public BigDecimal getUpperLimit() {
		return upperLimit;
	}
	public void setUpperLimit(BigDecimal upperLimit) {
		this.upperLimit = upperLimit;
	}
	public BigDecimal getLowerLimit() {
		return lowerLimit;
	}
	public void setLowerLimit(BigDecimal lowerLimit) {
		this.lowerLimit = lowerLimit;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
}