package com.suncere.statistic.entity.EntityModel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PollutantMonitorData {

	private LocalDateTime timePoint;
    private BigDecimal so2;
    private BigDecimal no2;
    private BigDecimal no;
    private BigDecimal co;
    private BigDecimal nox;
    private BigDecimal o3;
    private BigDecimal pm2_5;
    private BigDecimal pm10;
    private BigDecimal o3_8h;
    
    private Integer aqi;
    private String aqiLevel;
    private String primaryPollutant;
	public LocalDateTime getTimePoint() {
		return timePoint;
	}
	public void setTimePoint(LocalDateTime timePoint) {
		this.timePoint = timePoint;
	}
	public BigDecimal getSo2() {
		return so2;
	}
	public void setSo2(BigDecimal so2) {
		this.so2 = so2;
	}
	public BigDecimal getNo2() {
		return no2;
	}
	public void setNo2(BigDecimal no2) {
		this.no2 = no2;
	}
	public BigDecimal getNo() {
		return no;
	}
	public void setNo(BigDecimal no) {
		this.no = no;
	}
	public BigDecimal getCo() {
		return co;
	}
	public void setCo(BigDecimal co) {
		this.co = co;
	}
	public BigDecimal getNox() {
		return nox;
	}
	public void setNox(BigDecimal nox) {
		this.nox = nox;
	}
	public BigDecimal getO3() {
		return o3;
	}
	public void setO3(BigDecimal o3) {
		this.o3 = o3;
	}
	public BigDecimal getPm2_5() {
		return pm2_5;
	}
	public void setPm2_5(BigDecimal pm2_5) {
		this.pm2_5 = pm2_5;
	}
	public BigDecimal getPm10() {
		return pm10;
	}
	public void setPm10(BigDecimal pm10) {
		this.pm10 = pm10;
	}
	public BigDecimal getO3_8h() {
		return o3_8h;
	}
	public void setO3_8h(BigDecimal o3_8h) {
		this.o3_8h = o3_8h;
	}
	public Integer getAqi() {
		return aqi;
	}
	public void setAqi(Integer aqi) {
		this.aqi = aqi;
	}
	public String getAqiLevel() {
		return aqiLevel;
	}
	public void setAqiLevel(String aqiLevel) {
		this.aqiLevel = aqiLevel;
	}
	public String getPrimaryPollutant() {
		return primaryPollutant;
	}
	public void setPrimaryPollutant(String primaryPollutant) {
		this.primaryPollutant = primaryPollutant;
	}
}