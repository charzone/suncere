package com.suncere.statistic.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @since 2019-12-25
 */
public class CityMonitorDayWeather extends Model<CityMonitorDayWeather> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;
    private String cityCode;
    private Date timePoint;
    private Date createTime;
    private BigDecimal windDirection;
    private BigDecimal windSpeed;
    private BigDecimal pressure;
    private BigDecimal airTemperature;
    private BigDecimal humidity;
    private BigDecimal rainfall;
    private String mark;
    private BigDecimal apparent;
    private BigDecimal lowTemperature;
    private BigDecimal highTemperature;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
    public Date getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(Date timePoint) {
        this.timePoint = timePoint;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public BigDecimal getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(BigDecimal windDirection) {
        this.windDirection = windDirection;
    }
    public BigDecimal getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(BigDecimal windSpeed) {
        this.windSpeed = windSpeed;
    }
    public BigDecimal getPressure() {
        return pressure;
    }

    public void setPressure(BigDecimal pressure) {
        this.pressure = pressure;
    }
    public BigDecimal getAirTemperature() {
        return airTemperature;
    }

    public void setAirTemperature(BigDecimal airTemperature) {
        this.airTemperature = airTemperature;
    }
    public BigDecimal getHumidity() {
        return humidity;
    }

    public void setHumidity(BigDecimal humidity) {
        this.humidity = humidity;
    }
    public BigDecimal getRainfall() {
        return rainfall;
    }

    public void setRainfall(BigDecimal rainfall) {
        this.rainfall = rainfall;
    }
    public BigDecimal getApparent() {
        return apparent;
    }

    public void setApparent(BigDecimal apparent) {
        this.apparent = apparent;
    }
    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public BigDecimal getLowTemperature() {
		return lowTemperature;
	}

	public void setLowTemperature(BigDecimal lowTemperature) {
		this.lowTemperature = lowTemperature;
	}

	public BigDecimal getHighTemperature() {
		return highTemperature;
	}

	public void setHighTemperature(BigDecimal highTemperature) {
		this.highTemperature = highTemperature;
	}

	@Override
    protected Serializable pkVal() {
        return this.id;
    }

	@Override
	public String toString() {
		return "CityMonitorDayWeather [id=" + id + ", cityCode=" + cityCode + ", timePoint=" + timePoint
				+ ", createTime=" + createTime + ", windDirection=" + windDirection + ", windSpeed=" + windSpeed
				+ ", pressure=" + pressure + ", airTemperature=" + airTemperature + ", humidity=" + humidity
				+ ", rainfall=" + rainfall + ", mark=" + mark + ", apparent=" + apparent + ", lowTemperature="
				+ lowTemperature + ", highTemperature=" + highTemperature + "]";
	}
}
