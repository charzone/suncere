package com.suncere.statistic.entity.EntityModel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PollutantForecastData {
	private int forecastModelId;
	private String code;
	private LocalDateTime timePoint;
	private LocalDateTime createTime;
	private LocalDateTime forTime;
	
    private BigDecimal so2;
    private BigDecimal no2;
    private BigDecimal no;
    private BigDecimal co;
    private BigDecimal nox;
    private BigDecimal o3;
    private BigDecimal pm2_5;
    private BigDecimal pm10;
    private BigDecimal o3_8h;
	
	private Integer aqi;
	private Integer aqiLow;
	private Integer aqiHigh;
	private String aqiLevel;
	private String primaryPollutant;
	
	private BigDecimal so2_FloatingValue;
	private BigDecimal pm2_5_FloatingValue;
	private BigDecimal pm10_FloatingValue;
	private BigDecimal o3_FloatingValue;
	private BigDecimal nox_FloatingValue;
	private BigDecimal no2_FloatingValue;
	private BigDecimal no_FloatingValue;
	private BigDecimal co_FloatingValue;
	private BigDecimal o3_8h_FloatingValue;
	public int getForecastModelId() {
		return forecastModelId;
	}
	public void setForecastModelId(int forecastModelId) {
		this.forecastModelId = forecastModelId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public LocalDateTime getTimePoint() {
		return timePoint;
	}
	public void setTimePoint(LocalDateTime timePoint) {
		this.timePoint = timePoint;
	}
	public LocalDateTime getCreateTime() {
		return createTime;
	}
	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}
	public LocalDateTime getForTime() {
		return forTime;
	}
	public void setForTime(LocalDateTime forTime) {
		this.forTime = forTime;
	}
	public BigDecimal getSo2() {
		return so2;
	}
	public void setSo2(BigDecimal so2) {
		this.so2 = so2;
	}
	public BigDecimal getNo2() {
		return no2;
	}
	public void setNo2(BigDecimal no2) {
		this.no2 = no2;
	}
	public BigDecimal getNo() {
		return no;
	}
	public void setNo(BigDecimal no) {
		this.no = no;
	}
	public BigDecimal getCo() {
		return co;
	}
	public void setCo(BigDecimal co) {
		this.co = co;
	}
	public BigDecimal getNox() {
		return nox;
	}
	public void setNox(BigDecimal nox) {
		this.nox = nox;
	}
	public BigDecimal getO3() {
		return o3;
	}
	public void setO3(BigDecimal o3) {
		this.o3 = o3;
	}
	public BigDecimal getPm2_5() {
		return pm2_5;
	}
	public void setPm2_5(BigDecimal pm2_5) {
		this.pm2_5 = pm2_5;
	}
	public BigDecimal getPm10() {
		return pm10;
	}
	public void setPm10(BigDecimal pm10) {
		this.pm10 = pm10;
	}
	public BigDecimal getO3_8h() {
		return o3_8h;
	}
	public void setO3_8h(BigDecimal o3_8h) {
		this.o3_8h = o3_8h;
	}
	public Integer getAqi() {
		return aqi;
	}
	public void setAqi(Integer aqi) {
		this.aqi = aqi;
	}
	public Integer getAqiLow() {
		return aqiLow;
	}
	public void setAqiLow(Integer aqiLow) {
		this.aqiLow = aqiLow;
	}
	public Integer getAqiHigh() {
		return aqiHigh;
	}
	public void setAqiHigh(Integer aqiHigh) {
		this.aqiHigh = aqiHigh;
	}
	public String getAqiLevel() {
		return aqiLevel;
	}
	public void setAqiLevel(String aqiLevel) {
		this.aqiLevel = aqiLevel;
	}
	public String getPrimaryPollutant() {
		return primaryPollutant;
	}
	public void setPrimaryPollutant(String primaryPollutant) {
		this.primaryPollutant = primaryPollutant;
	}
	public BigDecimal getSo2_FloatingValue() {
		return so2_FloatingValue;
	}
	public void setSo2_FloatingValue(BigDecimal so2_FloatingValue) {
		this.so2_FloatingValue = so2_FloatingValue;
	}
	public BigDecimal getPm2_5_FloatingValue() {
		return pm2_5_FloatingValue;
	}
	public void setPm2_5_FloatingValue(BigDecimal pm2_5_FloatingValue) {
		this.pm2_5_FloatingValue = pm2_5_FloatingValue;
	}
	public BigDecimal getPm10_FloatingValue() {
		return pm10_FloatingValue;
	}
	public void setPm10_FloatingValue(BigDecimal pm10_FloatingValue) {
		this.pm10_FloatingValue = pm10_FloatingValue;
	}
	public BigDecimal getO3_FloatingValue() {
		return o3_FloatingValue;
	}
	public void setO3_FloatingValue(BigDecimal o3_FloatingValue) {
		this.o3_FloatingValue = o3_FloatingValue;
	}
	public BigDecimal getNox_FloatingValue() {
		return nox_FloatingValue;
	}
	public void setNox_FloatingValue(BigDecimal nox_FloatingValue) {
		this.nox_FloatingValue = nox_FloatingValue;
	}
	public BigDecimal getNo2_FloatingValue() {
		return no2_FloatingValue;
	}
	public void setNo2_FloatingValue(BigDecimal no2_FloatingValue) {
		this.no2_FloatingValue = no2_FloatingValue;
	}
	public BigDecimal getNo_FloatingValue() {
		return no_FloatingValue;
	}
	public void setNo_FloatingValue(BigDecimal no_FloatingValue) {
		this.no_FloatingValue = no_FloatingValue;
	}
	public BigDecimal getCo_FloatingValue() {
		return co_FloatingValue;
	}
	public void setCo_FloatingValue(BigDecimal co_FloatingValue) {
		this.co_FloatingValue = co_FloatingValue;
	}
	public BigDecimal getO3_8h_FloatingValue() {
		return o3_8h_FloatingValue;
	}
	public void setO3_8h_FloatingValue(BigDecimal o3_8h_FloatingValue) {
		this.o3_8h_FloatingValue = o3_8h_FloatingValue;
	}
}