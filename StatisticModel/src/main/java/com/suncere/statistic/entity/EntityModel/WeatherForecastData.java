package com.suncere.statistic.entity.EntityModel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class WeatherForecastData {

    private LocalDateTime timePoint;

    private LocalDateTime forTime;

    private String cityName;

    private String cityCode;

    private BigDecimal airTemp;

    private BigDecimal highAirTemp;

    private BigDecimal lowHighAirTemp;

    private String dayCondition;

    private String nightCondition;

    private BigDecimal dayWindDirection;

    private BigDecimal nightWindDirection;

    private String dayWindSpeed;

    private String nightWindSpeed;

    private BigDecimal windDirection;

    private BigDecimal windSpeed;

    private BigDecimal airPress;

    private BigDecimal relativeHumidity;

    private BigDecimal cloundCover;

    private BigDecimal visibility;

    private BigDecimal rainFall;


	public LocalDateTime getTimePoint() {
		return timePoint;
	}

	public void setTimePoint(LocalDateTime timePoint) {
		this.timePoint = timePoint;
	}

	public LocalDateTime getForTime() {
		return forTime;
	}

	public void setForTime(LocalDateTime forTime) {
		this.forTime = forTime;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public BigDecimal getAirTemp() {
		return airTemp;
	}

	public void setAirTemp(BigDecimal airTemp) {
		this.airTemp = airTemp;
	}

	public BigDecimal getHighAirTemp() {
		return highAirTemp;
	}

	public void setHighAirTemp(BigDecimal highAirTemp) {
		this.highAirTemp = highAirTemp;
	}

	public BigDecimal getLowHighAirTemp() {
		return lowHighAirTemp;
	}

	public void setLowHighAirTemp(BigDecimal lowHighAirTemp) {
		this.lowHighAirTemp = lowHighAirTemp;
	}

	public String getDayCondition() {
		return dayCondition;
	}

	public void setDayCondition(String dayCondition) {
		this.dayCondition = dayCondition;
	}

	public String getNightCondition() {
		return nightCondition;
	}

	public void setNightCondition(String nightCondition) {
		this.nightCondition = nightCondition;
	}

	public BigDecimal getDayWindDirection() {
		return dayWindDirection;
	}

	public void setDayWindDirection(BigDecimal dayWindDirection) {
		this.dayWindDirection = dayWindDirection;
	}

	public BigDecimal getNightWindDirection() {
		return nightWindDirection;
	}

	public void setNightWindDirection(BigDecimal nightWindDirection) {
		this.nightWindDirection = nightWindDirection;
	}

	public String getDayWindSpeed() {
		return dayWindSpeed;
	}

	public void setDayWindSpeed(String dayWindSpeed) {
		this.dayWindSpeed = dayWindSpeed;
	}

	public String getNightWindSpeed() {
		return nightWindSpeed;
	}

	public void setNightWindSpeed(String nightWindSpeed) {
		this.nightWindSpeed = nightWindSpeed;
	}

	public BigDecimal getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(BigDecimal windDirection) {
		this.windDirection = windDirection;
	}

	public BigDecimal getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(BigDecimal windSpeed) {
		this.windSpeed = windSpeed;
	}

	public BigDecimal getAirPress() {
		return airPress;
	}

	public void setAirPress(BigDecimal airPress) {
		this.airPress = airPress;
	}

	public BigDecimal getRelativeHumidity() {
		return relativeHumidity;
	}

	public void setRelativeHumidity(BigDecimal relativeHumidity) {
		this.relativeHumidity = relativeHumidity;
	}

	public BigDecimal getCloundCover() {
		return cloundCover;
	}

	public void setCloundCover(BigDecimal cloundCover) {
		this.cloundCover = cloundCover;
	}

	public BigDecimal getVisibility() {
		return visibility;
	}

	public void setVisibility(BigDecimal visibility) {
		this.visibility = visibility;
	}

	public BigDecimal getRainFall() {
		return rainFall;
	}

	public void setRainFall(BigDecimal rainFall) {
		this.rainFall = rainFall;
	}
}