package com.suncere.statistic.model;

import com.suncere.statistic.entity.EntityModel.*;
import com.suncere.statistic.model.cnemcassembly.AQICalculator;
import com.suncere.statistic.model.cnemcassembly.RangeAqiCalculator;
import com.suncere.statistic.model.cnemcassembly.aircriteria.IAQI;
import com.suncere.statistic.model.cnemcassembly.aircriteria.IndividualAirBaseIndexOpreate;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.AirBaseIndex;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.IndividualAirBaseIndex;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.RangeAirIndex;
import com.suncere.statistic.serivce.enums.EPollutants;
import com.suncere.statistic.serivce.enums.EvaluationCriteria;
import com.suncere.statistic.serivce.enums.PollutantParam;
import com.suncere.statistic.utils.DateUtils;
import com.suncere.statistic.utils.MathUtil;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class BaseStatisticModel {

    protected String[] pollutantNames = new String[]{
            PollutantParam.SO2.name(),
            PollutantParam.NO2.name(),
//			PollutantParam.NO.name(),
            PollutantParam.CO.name(),
//			PollutantParam.O3_1H.name(),
            PollutantParam.PM2_5.name(),
            PollutantParam.PM10.name(),
            PollutantParam.O3_8H.name()};

    protected ArrayList<PollutantForecastMetaData> forMetaDataList = new ArrayList<PollutantForecastMetaData>();
    /**
     * 最小下限常量浮动
     */
    private static final BigDecimal lowerLimitFloat = new BigDecimal(0.001);
    private static final BigDecimal THOUSAND_BIG_DECIMAL = new BigDecimal(1000);
    /**
     * 编码（城市、区域、站点）
     */
    protected String code;

    /**
     * 所属城市编码
     */
    protected String cityCode;
    /**
     * 样本数
     */
    protected int sampleCount;
    /**
     * 自变量个数
     */
    protected int factorCount;
    /**
     * 模型Id
     */
    protected int forecastModelId;

    /**
     * 预报日期
     */
    protected LocalDateTime timePoint;
    /**
     * 预报天数
     */
    protected int forecastDayCount;
    /**
     * 污染物预报浓度值范围设置
     */
    protected List<ForecastPollutantRangeDto> pollutantRangeList;
    /**
     * 允许AQI预报的最大范围
     */
    protected Integer AQIHighRange;
    /**
     * 允许AQI预报的最小范围
     */
    protected Integer AQILowRange;
    /**
     * 获取污染物/气象监测数据的开始时间
     */
    protected LocalDateTime beginTime;
    /**
     * 获取污染物/气象监测数据的结束时间
     */
    protected LocalDateTime endTime;
    /**
     * 是否要使用总站标准计算AQI预报范围，默认为不使用
     */
    protected Boolean isAccordingNationStandard;
    /**
     * 污染物监测数据
     */
    protected List<PollutantMonitorData> pollutantMonitorList;
    /**
     * 气象监测数据
     */
    protected List<WeatherMonitorData> weatherMonitorList;
    /**
     * 气象预报数据
     */
    protected List<WeatherForecastData> weatherForecastList;
    /**
     * 测试用的属性输入
     */
    protected double[][] testInput;
    /**
     * 测试用的属性输出
     */
    protected double[][] testOutput;

    private static final BigDecimal ONE = new BigDecimal(1);

    public BaseStatisticModel() {
    }

    public BaseStatisticModel(String code, String cityCode, LocalDateTime timePoint) {
        this.code = code;
        this.cityCode = cityCode;
        this.timePoint = timePoint;
    }

    /**
     * 统计模型基类构造函数
     *
     * @param code             编码（城市、区域、站点）
     * @param cityCode         所属城市编码
     * @param sampleCount      样本数
     * @param factorCount      自变量个数
     * @param timePoint        日期
     * @param forecastDayCount 预报天数
     */
    public BaseStatisticModel(String code, String cityCode, int sampleCount, int factorCount, LocalDateTime timePoint,
                              int forecastDayCount) {
        this.code = code;
        this.cityCode = cityCode;
        this.sampleCount = sampleCount;
        this.factorCount = factorCount;
        this.timePoint = timePoint;
        this.forecastDayCount = forecastDayCount;
    }

    /**
     * 获取合并封装各个污染物值的预报结果
     *
     * @param forTime
     */
    private PollutantForecastData getCalculatePollutant(LocalDateTime forTime) {
        PollutantForecastData forData = new PollutantForecastData();
        forData.setCode(this.code);
        forData.setForecastModelId(this.forecastModelId);
        forData.setTimePoint(this.timePoint);
        forData.setForTime(forTime);
        forData.setCreateTime(LocalDateTime.now());

        for (String pollutantName : pollutantNames) {
            PollutantForecastMetaData forMetaData = null;
            if (forMetaDataList != null) {
                forMetaData = forMetaDataList.stream()
                        .filter(p -> pollutantName.equals(p.getPolllutantName())
                                && DateUtils.isSameDay(this.timePoint, p.getTimePoint())
                                && DateUtils.isSameDay(forTime, p.getForTime()))
                        .findFirst().orElse(null);
            }
            if (forMetaData != null) {
                forMetaData = GetPollutantLimitValue(forMetaData); // 根据上下限值对污染物值做限制
            } else {// 当污染物不存在时
                forMetaData = new PollutantForecastMetaData();
                forMetaData.setValue(lowerLimitFloat);
                forMetaData.setFloatingValue(new BigDecimal(0));
            }

            if (PollutantParam.SO2.name().equals(pollutantName)) {
                forData.setSo2(forMetaData.getValue());
                forData.setSo2_FloatingValue(forMetaData.getFloatingValue());
            } else if (PollutantParam.NO2.name().equals(pollutantName)) {
                forData.setNo2(forMetaData.getValue());
                forData.setNo2_FloatingValue(forMetaData.getFloatingValue());
            } else if (PollutantParam.CO.name().equals(pollutantName)) {
                forData.setCo(forMetaData.getValue());
                forData.setCo_FloatingValue(forMetaData.getFloatingValue());
            } else if (PollutantParam.PM2_5.name().equals(pollutantName)) {
                forData.setPm2_5(forMetaData.getValue());
                forData.setPm2_5_FloatingValue(forMetaData.getFloatingValue());
            } else if (PollutantParam.PM10.name().equals(pollutantName)) {
                forData.setPm10(forMetaData.getValue());
                forData.setPm10_FloatingValue(forMetaData.getFloatingValue());
            } else if (PollutantParam.O3_8H.name().equals(pollutantName)) {
                forData.setO3_8h(forMetaData.getValue());
                forData.setO3_8h_FloatingValue(forMetaData.getFloatingValue());
            } else {
            }
        }
        return forData;
    }

    /**
     * 根据污染物的名字获取污染物的上下限值并吃饭预报数据值进行限制设置
     */
    private PollutantForecastMetaData GetPollutantLimitValue(final PollutantForecastMetaData forMetaData) {
        if (forMetaData != null) {
            if (this.pollutantRangeList != null) {
                ForecastPollutantRangeDto pollutantRange = pollutantRangeList.stream()
                        .filter(o -> forMetaData.getPolllutantName().equals(o.getPollutant())).findFirst().orElse(null);// 获取数据库配置的污染物的上下限值
                if (pollutantRange != null && pollutantRange.getStatus() && forMetaData.getValue() != null) { // 规则启用才根据上下限制做限制
                    if (forMetaData.getValue().compareTo(pollutantRange.getLowerLimit()) <= 0) {
                        forMetaData.setValue(pollutantRange.getLowerLimit().add(lowerLimitFloat));
                        forMetaData.setFloatingValue(lowerLimitFloat);
                    }
                    if (forMetaData.getValue().compareTo(pollutantRange.getUpperLimit()) >= 0) {
                        forMetaData.setValue(pollutantRange.getUpperLimit().subtract(lowerLimitFloat));
                        forMetaData.setFloatingValue(lowerLimitFloat);
                    } else {
                        // 判断浮动值
                        BigDecimal floatingValue1 = new BigDecimal(0);
                        BigDecimal floatingValue2 = new BigDecimal(0);
                        if (forMetaData.getValue().subtract(forMetaData.getFloatingValue())
                                .compareTo(pollutantRange.getLowerLimit()) == -1) {// a<b
                            floatingValue1 = forMetaData.getValue().subtract(pollutantRange.getLowerLimit());
                            forMetaData.setFloatingValue(floatingValue1);
                        }
                        if (forMetaData.getValue().subtract(forMetaData.getFloatingValue())
                                .compareTo(pollutantRange.getUpperLimit()) == 1) {// a>b
                            floatingValue2 = pollutantRange.getUpperLimit().subtract(forMetaData.getValue());
                            forMetaData.setFloatingValue(MathUtil.min(floatingValue1, floatingValue2));
                        }
                    }
                }
            }
        }
        return forMetaData;
    }

    /**
     * 计算预报数据的AQI上限值、下限值、准确值、首要污染物、AQI等级
     *
     * @param forData
     */
    private void CalculateAirIndex(PollutantForecastData forData) {
        // AQI下限值
        HashMap<String, BigDecimal> lowPollutantMonValue = new HashMap<String, BigDecimal>();
        lowPollutantMonValue = new HashMap<String, BigDecimal>();
        lowPollutantMonValue.put("SO2", forData.getSo2().subtract(forData.getSo2_FloatingValue()));
        lowPollutantMonValue.put("NO2", forData.getNo2().subtract(forData.getNo2_FloatingValue()));
        lowPollutantMonValue.put("O3_8H", forData.getO3_8h().subtract(forData.getO3_8h_FloatingValue()));
        lowPollutantMonValue.put("PM10", forData.getPm10().subtract(forData.getPm10_FloatingValue()));
        lowPollutantMonValue.put("PM2_5", forData.getPm2_5().subtract(forData.getPm2_5_FloatingValue()));
        lowPollutantMonValue.put("CO", forData.getCo().subtract(forData.getCo_FloatingValue()));
        AirBaseIndex lowAqiIndex = AQICalculator.getResultDay(lowPollutantMonValue, EvaluationCriteria.AQI);

        // AQI上限值
        HashMap<String, BigDecimal> highPollutantMonValue = new HashMap<String, BigDecimal>();
        highPollutantMonValue = new HashMap<String, BigDecimal>();
        highPollutantMonValue.put("SO2", forData.getSo2().add(forData.getSo2_FloatingValue()));
        highPollutantMonValue.put("NO2", forData.getNo2().add(forData.getNo2_FloatingValue()));
        highPollutantMonValue.put("O3_8H", forData.getO3_8h().add(forData.getO3_8h_FloatingValue()));
        highPollutantMonValue.put("PM10", forData.getPm10().add(forData.getPm10_FloatingValue()));
        highPollutantMonValue.put("PM2_5", forData.getPm2_5().add(forData.getPm2_5_FloatingValue()));
        highPollutantMonValue.put("CO", forData.getCo().add(forData.getCo_FloatingValue()));
        AirBaseIndex highAqiIndex = AQICalculator.getResultDay(highPollutantMonValue, EvaluationCriteria.AQI);

        if ((highAqiIndex.getAirIndex() - lowAqiIndex.getAirIndex()) > this.AQIHighRange) {
            lowAqiIndex.setAirIndex(highAqiIndex.getAirIndex() - this.AQIHighRange);
        }
        if (highAqiIndex.getAirIndex() - lowAqiIndex.getAirIndex() < this.AQILowRange
                && highAqiIndex.getAirIndex() - this.AQILowRange > 0) {
            lowAqiIndex.setAirIndex(highAqiIndex.getAirIndex() - this.AQILowRange);
        }

        // AQI准确值
        HashMap<String, BigDecimal> PollutantMonValue = new HashMap<String, BigDecimal>();
        PollutantMonValue = new HashMap<String, BigDecimal>();
        PollutantMonValue.put("SO2", forData.getSo2());
        PollutantMonValue.put("NO2", forData.getNo2());
        PollutantMonValue.put("O3_8H", forData.getO3_8h());
        PollutantMonValue.put("PM10", forData.getPm10());
        PollutantMonValue.put("PM2_5", forData.getPm2_5());
        PollutantMonValue.put("CO", forData.getCo());
        AirBaseIndex aqiIndex = AQICalculator.getResultDay(PollutantMonValue, EvaluationCriteria.AQI);

        RangeAirIndex aqiRange = RangeAqiCalculator.getResultDay(lowPollutantMonValue, highPollutantMonValue,
                EvaluationCriteria.AQI);// AQI范围,用于计算首要污染物

        forData.setAqiLow(lowAqiIndex.getAirIndex());
        forData.setAqiHigh(highAqiIndex.getAirIndex());
        forData.setAqi(aqiIndex.getAirIndex());
        forData.setAqiLevel(aqiIndex.getLevel());
        forData.setPrimaryPollutant(aqiRange.getPrimaryPollutant());
    }

    /**
     * 计算预报数据的AQI上限值、下限值、准确值、首要污染物、AQI等级
     * 先计算一个AQI预报值，根据国家标准加减一个变化幅度按照总站标准计算aqi预报范（中度污染及以下加减15，重度污染及以上加减30），形成AQI范围预报；
     *
     * @param forData
     */
    private void CaculateAirIndexAccordingNationStandard(PollutantForecastData forData) {
        // AQI准确值
        HashMap<String, BigDecimal> PollutantMonValue = new HashMap<>();
        PollutantMonValue.put(PollutantParam.SO2.name(), forData.getSo2().compareTo(ONE) > 0 ? forData.getSo2().divide(THOUSAND_BIG_DECIMAL) : forData.getSo2());
        PollutantMonValue.put(PollutantParam.NO2.name(), forData.getNo2().compareTo(ONE) > 0 ? forData.getNo2().divide(THOUSAND_BIG_DECIMAL) : forData.getNo2());
        PollutantMonValue.put(PollutantParam.O3_8H.name(), forData.getO3_8h().compareTo(ONE) > 0 ? forData.getO3_8h().divide(THOUSAND_BIG_DECIMAL) : forData.getO3_8h());
        PollutantMonValue.put(PollutantParam.PM10.name(), forData.getPm10().compareTo(ONE) > 0 ? forData.getPm10().divide(THOUSAND_BIG_DECIMAL) : forData.getPm10());
        PollutantMonValue.put(PollutantParam.PM2_5.name(), forData.getPm2_5().compareTo(ONE) > 0 ? forData.getPm2_5().divide(THOUSAND_BIG_DECIMAL) : forData.getPm2_5());
        PollutantMonValue.put(PollutantParam.CO.name(), forData.getCo());

        AirBaseIndex aqiIndex = AQICalculator.getResultDay(PollutantMonValue, EvaluationCriteria.AQI);
        int[] aqiRange = GetAQIForecastRangeAccordingNationStandard(aqiIndex); // aqi预报范围

        Integer aqi = aqiIndex.getAirIndex(); // aqi预报结果
        String primaryPollutant = aqiIndex.getPrimaryPollutant(); // 首要污染物预报结果
        primaryPollutant = GetForecastPrimaryPollutant(PollutantMonValue, aqi, aqiRange, primaryPollutant);

        forData.setAqiLow(aqiRange[0]);
        forData.setAqiHigh(aqiRange[1]);
        forData.setAqi(aqi);
        forData.setAqiLevel(aqiIndex.getLevel());
        forData.setPrimaryPollutant(primaryPollutant);
    }

    /**
     * 计算首要污染物的预报结果
     *
     * @param PollutantMonValue 六项污染物浓度值
     * @param aqi               aqi预报结果
     * @param aqiRange          aqi预报范围
     * @param primaryPollutant  首要污染物
     * @return 首要污染物
     */
    private String GetForecastPrimaryPollutant(HashMap<String, BigDecimal> PollutantMonValue, int aqi, int[] aqiRange,
                                               String primaryPollutant) {
        String result = primaryPollutant;

        // aqi预报范围上限值大于50，此种情形有首要污染物；
        // 但aqi预报结果《=50，则上面计算出来的首污（primaryPollutant=aqiIndex.PrimaryPollutant）与aqi预报范围实际情况不一致，需重新计算首污
        if (aqiRange[1] > 50 && aqi <= 50) {
            ArrayList<String> newPrimaryPollutant = new ArrayList<String>();

            PollutantMonValue.forEach((k, v) -> {
                IndividualAirBaseIndexOpreate iabi = new IAQI();
                iabi.setCp(v.doubleValue());
                iabi.setEPollutants(getEPollutantsNameDay(k));
                IndividualAirBaseIndex airBaseIndex = iabi.GetIndividualAirBaseIndex();

                int iaqi = airBaseIndex.getIndividualAirIndex();
                if (iaqi == aqi) // 分指数与aqi预报结果相等的污染物就是首要污染物
                {
                    newPrimaryPollutant.add(airBaseIndex.getPollutantChineseName());
                }
            });

            result = String.join(",", newPrimaryPollutant);
        }
        return result;
    }

    private static EPollutants getEPollutantsNameDay(String p) {
        if (p == "SO2")
            return EPollutants.SO2;
        else if (p == "NO2")
            return EPollutants.NO2;
        else if (p == "O3")
            return EPollutants.O3_8H;
        else if (p == "O3_8H")
            return EPollutants.O3_8H;
        else if (p == "PM10")
            return EPollutants.PM10;
        else if (p == "PM2_5")
            return EPollutants.PM2_5;
        else if (p == "CO")
            return EPollutants.CO;
        else
            return EPollutants.O3_8H;
    }

    /**
     * 根据总站标准设置AQI预报范围
     *
     * @param aqiIndex
     * @return
     */
    private static int[] GetAQIForecastRangeAccordingNationStandard(AirBaseIndex aqiIndex) {
        int[] result = new int[2];
        if (aqiIndex == null) {
            return new int[]{-99, -99};
        }
        int aqi = aqiIndex.getAirIndex();
        if (aqi <= 200) {
            int lowAQI = aqi - 15;

            if (lowAQI <= 0) { // 避免预报值出现《=0的情况，至少也要等于1。
                lowAQI = 1;
            }
            result[0] = lowAQI;
            result[1] = aqi + 15;
        } else {
            result[0] = aqi - 30;
            result[1] = aqi + 30;
        }

        return result;
    }

    /**
     * 数据完整性检查
     */
    protected abstract void DataCompletionCheck();

    /**
     * 数据源准备
     *
     * @param pollutantName
     */
    protected abstract void DataPrepare(String pollutantName);

    /**
     * 预报
     */
    protected abstract void Predict();

    /**
     * 预报过程（没有气象预报数据时所用的备用方法）
     */
    protected abstract void BackupPredict();

    /**
     * 预报输出
     *
     * @return
     */
    public final ArrayList<PollutantForecastData> PredictOutPut() {
        if (this.weatherForecastList != null && this.weatherForecastList.size() > 0) {
            Predict();
        } else {// 没有气象预报数据时使用备用方法
            BackupPredict();
        }

        ArrayList<PollutantForecastData> forList = new ArrayList<PollutantForecastData>();
        for (LocalDateTime forTime = this.timePoint, endForTime = this.timePoint.plusDays(
                this.forecastDayCount - 1); forTime.compareTo(endForTime) <= 0; forTime = forTime.plusDays(1)) {

            PollutantForecastData forData = getCalculatePollutant(forTime);
            if (this.isAccordingNationStandard) {
                CaculateAirIndexAccordingNationStandard(forData);
            } else {
                CalculateAirIndex(forData);
            }

            forList.add(forData);
        }
        return forList;
    }

    public String[] getPollutantNames() {
        return pollutantNames;
    }

    public void setPollutantNames(String[] pollutantNames) {
        this.pollutantNames = pollutantNames;
    }

    public ArrayList<PollutantForecastMetaData> getForMetaDataList() {
        return forMetaDataList;
    }

    public void setForMetaDataList(ArrayList<PollutantForecastMetaData> forMetaDataList) {
        this.forMetaDataList = forMetaDataList;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public int getSampleCount() {
        return sampleCount;
    }

    public void setSampleCount(int sampleCount) {
        this.sampleCount = sampleCount;
    }

    public int getFactorCount() {
        return factorCount;
    }

    public void setFactorCount(int factorCount) {
        this.factorCount = factorCount;
    }

    public int getForecastModelId() {
        return forecastModelId;
    }

    public void setForecastModelId(int forecastModelId) {
        this.forecastModelId = forecastModelId;
    }

    public LocalDateTime getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(LocalDateTime timePoint) {
        this.timePoint = timePoint;
    }

    public int getForecastDayCount() {
        return forecastDayCount;
    }

    public void setForecastDayCount(int forecastDayCount) {
        this.forecastDayCount = forecastDayCount;
    }

    public List<ForecastPollutantRangeDto> getPollutantRangeList() {
        return pollutantRangeList;
    }

    public void setPollutantRangeList(List<ForecastPollutantRangeDto> pollutantRangeList) {
        this.pollutantRangeList = pollutantRangeList;
    }

    public Integer getAQIHighRange() {
        return AQIHighRange;
    }

    public void setAQIHighRange(Integer aQIHighRange) {
        AQIHighRange = aQIHighRange;
    }

    public Integer getAQILowRange() {
        return AQILowRange;
    }

    public void setAQILowRange(Integer aQILowRange) {
        AQILowRange = aQILowRange;
    }

    public LocalDateTime getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(LocalDateTime beginTime) {
        this.beginTime = beginTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Boolean getIsAccordingNationStandard() {
        return isAccordingNationStandard;
    }

    public void setIsAccordingNationStandard(Boolean isAccordingNationStandard) {
        this.isAccordingNationStandard = isAccordingNationStandard;
    }

    public List<PollutantMonitorData> getPollutantMonitorList() {
        return pollutantMonitorList;
    }

    public void setPollutantMonitorList(List<PollutantMonitorData> pollutantMonitorList) {
        this.pollutantMonitorList = pollutantMonitorList;
    }

    public List<WeatherMonitorData> getWeatherMonitorList() {
        return weatherMonitorList;
    }

    public void setWeatherMonitorList(List<WeatherMonitorData> weatherMonitorList) {
        this.weatherMonitorList = weatherMonitorList;
    }

    public List<WeatherForecastData> getWeatherForecastList() {
        return weatherForecastList;
    }

    public void setWeatherForecastList(List<WeatherForecastData> weatherForecastList) {
        this.weatherForecastList = weatherForecastList;
    }

    public double[][] getTestInput() {
        return testInput;
    }

    public void setTestInput(double[][] testInput) {
        this.testInput = testInput;
    }

    public double[][] getTestOutput() {
        return testOutput;
    }

    public void setTestOutput(double[][] testOutput) {
        this.testOutput = testOutput;
    }
}