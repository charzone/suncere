package com.suncere.statistic.model.cnemcassembly.aircriteria.model;

/** 
 空气质量指标
*/
public class AirTarget
{
	private String privateLevel;
	public final String getLevel()
	{
		return privateLevel;
	}
	private void setLevel(String value)
	{
		privateLevel = value;
	}
	private String privateChineseLevel;
	public final String getChineseLevel()
	{
		return privateChineseLevel;
	}
	private void setChineseLevel(String value)
	{
		privateChineseLevel = value;
	}
	private String privateType;
	public final String getType()
	{
		return privateType;
	}
	private void setType(String value)
	{
		privateType = value;
	}
	private String privateColor;
	public final String getColor()
	{
		return privateColor;
	}
	private void setColor(String value)
	{
		privateColor = value;
	}
	private String privateUnheathful;
	public final String getUnheathful()
	{
		return privateUnheathful;
	}
	private void setUnheathful(String value)
	{
		privateUnheathful = value;
	}
	private String privateMeasure;
	public final String getMeasure()
	{
		return privateMeasure;
	}
	private void setMeasure(String value)
	{
		privateMeasure = value;
	}
	private String privateAImage;
	public final String getAImage()
	{
		return privateAImage;
	}
	private void setAImage(String value)
	{
		privateAImage = value;
	}
	private String privateInColor;
	public final String getInColor()
	{
		return privateInColor;
	}
	private void setInColor(String value)
	{
		privateInColor = value;
	}

	public AirTarget(int aqi)
	{
		if (aqi >= 0 && aqi <= 50)
		{
			setLevel("I");
			setChineseLevel("一级");
			setType("优");
			setColor("#58e65b");
			setInColor("#C9FFCA");
			setUnheathful("空气质量令人满意，基本无空气污染");
			setMeasure("各类人群可正常活动");
			setAImage("../Content/images/aqi1.png");
		}

		if (aqi >= 51 && aqi <= 100)
		{
			setLevel("II");
			setChineseLevel("二级");
			setType("良");
			setColor("#FFFF4C");
			setInColor("#FFFFDE");
			setUnheathful("空气质量可接受，但某些污染物可能对极少数异常敏感人群健康有较弱影响");
			setMeasure("极少数异常敏感人群应减少户外活动");
			setAImage("../Content/images/aqi2.png");
		}

		if (aqi >= 101 && aqi <= 150)
		{
			setLevel("III");
			setChineseLevel("三级");
			setType("轻度污染");
			setColor("#F4902C");
			setInColor("#F4E4D4");
			setUnheathful("易感人群症状有轻度加剧，健康人群出现刺激症状");
			setMeasure("儿童、老年人及心脏病、呼吸系统疾病患者应减少长时间、高强度的户外锻炼");
			setAImage("../Content/images/aqi3.png");
		}

		if (aqi >= 151 && aqi <= 200)
		{
			setLevel("IV");
			setChineseLevel("四级");
			setType("中度污染");
			setColor("#FF494C");
			setInColor("#FFD6D7");
			setUnheathful("进一步加剧易感人群症状，可能对健康人群心脏、呼吸系统有影响");
			setMeasure("儿童、老年人及心脏病、呼吸系统疾病患者应减少长时间、高强度的户外锻炼，一般人群适量减少户外活动");
			setAImage("../Content/images/aqi4.png");
		}

		if (aqi >= 201 && aqi <= 300)
		{
			setLevel("V");
			setChineseLevel("五级");
			setType("重度污染");
			setColor("#B82D70");
			setInColor("#FAD5E7");
			setUnheathful("心脏病和肺病患者症状显著加剧，运动耐力降低，健康人群普遍出现症状");
			setMeasure("老年人及心脏病、肺病患者应停留在室内，停止户外活动，一般人群减少户外活动");
			setAImage("../Content/images/aqi5.png");
		}

		if (aqi >= 301 && aqi <= 1000)
		{
			setLevel("VI");
			setChineseLevel("六级");
			setType("严重污染");
			setColor("#A5133A");
			setInColor("#E9B2C1");
			setUnheathful("健康人运动耐受力降低，有明显强烈症状，提前出现某些疾病");
			setMeasure("老年人和病人应当留在室内，避免体力消耗，一般人群应避免户外活动");
			setAImage("../Content/images/aqi6.png");
		}
	}
}