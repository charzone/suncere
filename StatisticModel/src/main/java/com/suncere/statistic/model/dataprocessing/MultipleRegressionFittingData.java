package com.suncere.statistic.model.dataprocessing;

public class MultipleRegressionFittingData {
	/**
	 * 回归系数，共m+1个，前m个对应m个自变量，第m+1个为截距
	 */
	private double[] a;
	/**
	 * 偏差平方和
	 */
	private double q;
	/**
	 * 平均标准偏差
	 */
	private double s;
	/**
	 * 复相关系数
	 */
	private double r;
	/**
	 * 回归平方和
	 */
	private double u;
	/**
	 * m个自变量的偏相关系数
	 */
	private double[] v;
	public double[] getA() {
		return a;
	}
	public void setA(double[] a) {
		this.a = a;
	}
	public double getQ() {
		return q;
	}
	public void setQ(double q) {
		this.q = q;
	}
	public double getS() {
		return s;
	}
	public void setS(double s) {
		this.s = s;
	}
	public double getR() {
		return r;
	}
	public void setR(double r) {
		this.r = r;
	}
	public double getU() {
		return u;
	}
	public void setU(double u) {
		this.u = u;
	}
	public double[] getV() {
		return v;
	}
	public void setV(double[] v) {
		this.v = v;
	}
}