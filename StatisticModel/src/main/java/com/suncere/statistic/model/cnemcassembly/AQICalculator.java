package com.suncere.statistic.model.cnemcassembly;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.suncere.statistic.model.cnemcassembly.aircriteria.AirBaseIndexOpreate;
import com.suncere.statistic.model.cnemcassembly.aircriteria.IAPI;
import com.suncere.statistic.model.cnemcassembly.aircriteria.IAQI;
import com.suncere.statistic.model.cnemcassembly.aircriteria.IndividualAirBaseIndexOpreate;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.AirBaseIndex;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.IndividualAirBaseIndex;
import com.suncere.statistic.serivce.enums.EPollutants;
import com.suncere.statistic.serivce.enums.EvaluationCriteria;

public class AQICalculator {
//	/// #region 计算小时的AQI
//	public static AirBaseIndex getResultHour(HashMap<String, BigDecimal> PollutantMonValue,
//			EvaluationCriteria Mark) {
//		try {
//			ArrayList<IndividualAirBaseIndex> iaqis = new ArrayList<IndividualAirBaseIndex>();
//			for (Map.Entry<String, BigDecimal> item : PollutantMonValue.entrySet()) {
//				IndividualAirBaseIndexOpreate iabi;
//				if (Mark == EvaluationCriteria.AQI) {
//					iabi = new IAQI();
//				} else {
//					iabi = new IAPI();
//				}
//				iabi.setCp(Double.parseDouble(item.getValue().toString()));
//				iabi.setEPollutants(getEPollutantsNameHour(item.getKey()));
//				iaqis.add(iabi.GetIndividualAirBaseIndex());
//			}
//			// 空气质量指数计算测试
//			AirBaseIndexOpreate<IndividualAirBaseIndex> aqi = new AirBaseIndexOpreate<IndividualAirBaseIndex>(iaqis,
//					Mark);
//			return aqi.GetAirIndex();
//		} catch (RuntimeException e) {
//			// LogHelper.Instance().WriteLog("计算AQI小时值时，程序出错", e);
//			return new AirBaseIndex();
//		}
//	}

	/// #region 计算天的AQI
	public static AirBaseIndex getResultDay(HashMap<String, BigDecimal> PollutantMonValue,
			EvaluationCriteria Mark) {
		try {
			ArrayList<IndividualAirBaseIndex> iaqis = new ArrayList<IndividualAirBaseIndex>();
			for (Map.Entry<String, BigDecimal> item : PollutantMonValue.entrySet()) {
				IndividualAirBaseIndexOpreate iabi;
				if (Mark == EvaluationCriteria.AQI) {
					iabi = new IAQI();
				} else {
					iabi = new IAPI();
				}
				iabi.setCp(Double.parseDouble(item.getValue().toString()));
				iabi.setEPollutants(getEPollutantsNameDay(item.getKey()));
				iaqis.add(iabi.GetIndividualAirBaseIndex());
			}
			// 空气质量指数计算测试
			AirBaseIndexOpreate<IndividualAirBaseIndex> aqi = new AirBaseIndexOpreate<IndividualAirBaseIndex>(iaqis, Mark);
			return aqi.GetAirIndex();
		} catch (RuntimeException e) {
			// LogHelper.Instance().WriteLog("计算AQI天值时，程序出错", e);
			return new AirBaseIndex();
		}
	}

	/// #region 通过污染物名称获取污染物类型（日）
	/**
	 * 通过污染物名称获取污染物类型（日）
	 * 
	 * @param p
	 * @return
	 */
	private static EPollutants getEPollutantsNameDay(String p) {
		if (p.equals("SO2")) {
			return EPollutants.SO2;
		} else if (p.equals("NO2")) {
			return EPollutants.NO2;
		} else if (p.equals("O3")) {
			return EPollutants.O3_8H;
		} else if (p.equals("O3_8H")) {
			return EPollutants.O3_8H;
		} else if (p.equals("PM10")) {
			return EPollutants.PM10;
		} else if (p.equals("PM2_5")) {
			return EPollutants.PM2_5;
		} else if (p.equals("CO")) {
			return EPollutants.CO;
		} else {
			return EPollutants.O3_8H;
		}
	}

//	/// #region 通过污染物名称获取污染物类型（小时）
//	/**
//	 * 通过污染物名称获取污染物类型（小时）
//	 * 
//	 * @param p
//	 * @return
//	 */
//	private static EPollutants getEPollutantsNameHour(String p) {
//		if (p.equals("SO2")) {
//			return EPollutants.SO2_1h;
//		} else if (p.equals("NO2")) {
//			return EPollutants.NO2_1h;
//		} else if (p.equals("O3")) {
//			return EPollutants.O3;
//		} else if (p.equals("O3_8H")) {
//			return EPollutants.O3;
//		} else if (p.equals("PM10")) {
//			return EPollutants.PM10;
//		} else if (p.equals("PM2_5")) {
//			return EPollutants.PM2_5;
//		} else if (p.equals("CO")) {
//			return EPollutants.CO_1h;
//		} else {
//			return EPollutants.O3_8h;
//		}
//	}
}