package com.suncere.statistic.model.cnemcassembly.aircriteria;

import com.suncere.statistic.model.cnemcassembly.aircriteria.model.AirIndex;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.DataSource;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.IndividualAirBaseIndex;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.PollutantConLimits;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.PollutantInfo;
import com.suncere.statistic.serivce.enums.EPollutants;
import com.suncere.statistic.serivce.enums.EvaluationCriteria;

//
// *date:2014-1-8 14:09
// *zhou
// *修改说明：
// *  (1) 所有污染浓度超过最高限值按照500IAQI计算
// *  (2)二氧化硫（SO2）1小时平均浓度值高于800微克/立方米的，不再进行其空气质量分指数计算，二氧化硫（SO2）空气质量分指数按24小时平均浓度计算的分指数报告。
// *  (3)臭氧（O3）8小时平均浓度值高于800微克/立方米的，不再进行其空气质量分指数计算，臭氧（O3）空气质量分指数按1小时平均浓度计算的分指数报告。
// * Cp 污染物浓度(单位：毫克/立方米)
// * 2015-08-05
// * 取消IndividualAirBaseIndex _index; readonly属性，配置 为可写,为计算反向计算做准备
// 
public abstract class IndividualAirBaseIndexOpreate {

	private PollutantConLimits pollutantConLimits;
	public IndividualAirBaseIndex index; // 修改为可读写
	public DataSource dataSource;
	/**
	 * 污染物名称
	 */
	private String pollutantName;
	/**
	 * 污染物浓度(单位：毫克/立方米)
	 * 
	 */
	private double cp;
	/**
	 * 指定污染物
	 */
	private EPollutants EPollutants;

	private EvaluationCriteria ec;

	protected IndividualAirBaseIndexOpreate() {
	}

	protected IndividualAirBaseIndexOpreate(EvaluationCriteria ec) {
		this.ec = ec;
		index = new IndividualAirBaseIndex();
		dataSource = new DataSource();
	}

	protected abstract void SetPollutantName();

	/**
	 * 通过污染物浓度 获取污染物浓度限值标准（原来的）
	 */
	protected void GetPollutantConLimits() {
		// 获取污染物信息
		if (ec == EvaluationCriteria.AQI && !getEPollutants().equals(EPollutants.CO)
				&& !EPollutants.CO_1H.equals(getEPollutants())) {
			setCp(getCp() * 1000);
		}
		PollutantInfo pollutantInfo = dataSource.getPollutantInfoList().stream()
				.filter(i -> getPollutantName().trim().equals(i.getName())).findFirst().orElseGet(null);
		if (pollutantInfo != null) {
			index.setPollutantChineseName(pollutantInfo.getChineseName());
			pollutantConLimits = new PollutantConLimits();
			try {
				pollutantConLimits = getCp() == 0
						? dataSource.getPollutantConLimitsList().stream()
								.filter(i -> getPollutantName().trim().equals(i.getPollutantCode())
										&& i.getBpLo() == getCp() && i.getEc() == this.ec.getValue())
								.findFirst().orElseGet(null)
						: dataSource.getPollutantConLimitsList().stream()
								.filter(i -> getPollutantName().trim().equals(i.getPollutantCode())
										&& i.getBpHi() >= getCp() && i.getBpLo() < getCp()
										&& i.getEc() == this.ec.getValue())
								.findFirst().orElseGet(null);
			} catch (RuntimeException e) {
				// throw new Exception("无法找到与指定污染物项目浓度相对应的污染物浓度限值标准");
				pollutantConLimits = null;
			}
		}
	}

	/**
	 * 通过IAQI去获取污染物浓度限值标准（wuzs新增的20150805）
	 * 
	 */
	protected void GetPollutantConLimitsFormIaqi() {

		PollutantInfo pollutantInfo = dataSource.getPollutantInfoList().stream()
				.filter(i -> getPollutantName().trim().equals(i.getName())).findFirst().orElseGet(null);
		if (pollutantInfo != null) {
			index.setPollutantChineseName(pollutantInfo.getChineseName());
			pollutantConLimits = new PollutantConLimits();
			try {
				// 如果IAQI大500或小于1，返回为null
				if (index.getIndividualAirIndex() > 500 || index.getIndividualAirIndex() < 1) {
					pollutantConLimits = null;
				} else {
					pollutantConLimits = dataSource.getPollutantConLimitsList().stream()
							.filter(i -> getPollutantName().trim().equals(i.getPollutantCode())
									&& i.getIaiHi() >= index.getIndividualAirIndex()
									&& i.getIaiLo() < index.getIndividualAirIndex() && i.getEc() == this.ec.getValue())
							.findFirst().orElseGet(null);
				}
			} catch (RuntimeException e) {
				// throw new Exception("无法找到与指定污染物项目浓度相对应的污染物浓度限值标准");
				pollutantConLimits = null;
			}
		}
	}

	/**
	 * 计算空气质量分指数
	 * 
	 * @return
	 */
	protected void CalculateAirIndex() {
		GetPollutantConLimits();
		if (pollutantConLimits == null) {
			if (getCp() < 0 || ((getEPollutants() == EPollutants.SO2_1H || getEPollutants() == EPollutants.O3_8H)
					&& getCp() > 800)) { // 二氧化硫（SO2）1小时平均浓度值或者臭氧（O3）8小时平均浓度值高于800微克/立方米的不再进行其空气质量分指数计算
				index.setIndividualAirIndex(-99);
				index.setLevel("—");
				index.setChineseLevel("—");
			} else { // 爆表的节奏
				index.setIndividualAirIndex(500);
				index.setLevel(dataSource.getAirIndexList().stream()
						.filter(i -> i.getAiHi() == 1000 && i.getEc() == this.ec.getValue()).findFirst().get()
						.getLevel());
				index.setChineseLevel(dataSource.getAirIndexList().stream()
						.filter(i -> i.getAiHi() == 1000 && i.getEc() == this.ec.getValue()).findFirst().get()
						.getChineseLevel());
			}
			return;
		}
		double ceil = Math.ceil((pollutantConLimits.getIaiHi() - pollutantConLimits.getIaiLo()) / (pollutantConLimits.getBpHi() - pollutantConLimits.getBpLo())
				* (this.cp - pollutantConLimits.getBpLo()) + pollutantConLimits.getIaiLo());
				index.setIndividualAirIndex(new Double(ceil).intValue());
		int aqi = index.getIndividualAirIndex();
		AirIndex air = dataSource.getAirIndexList().stream()
				.filter(i -> i.getAiHi() >= aqi && i.getAiLo() <= aqi && i.getEc() == this.ec.getValue()).findFirst()
				.orElse(null);
		index.setLevel(air.getLevel());
		index.setChineseLevel(air.getChineseLevel());
	}

	/**
	 * 通过已知IAQI反向计算污染物浓度 输出的要是毫克单位 ☆ 举例说明：
	 * //如AQI为30~60，首要污染物为PM2.5，则默认PM2.5的IAQI范围为30~60，则反推PM2.5浓度如下： //（1）查上表知，
	 * //IAQI=30时的IAQIlow 与IAQIhigh分别为0和50，对应的Clow 和Chigh分别为0和35ug/m3；
	 * //IAQI=60时的IAQIlow 与IAQIhigh分别为50和100，对应的Clow 和Chigh分别为35和75ug/m3；
	 * //（2）因此，IAQI=30时，PM2.5浓度为：
	 * //C=Clow+（Chigh-Clow）/（IAQIhigh-IAQIlow）*（IAQI-IAQIlow） //
	 * =0+（35-0）/（50-0）*（30-0）=0.7*30=21ug/m3 //（3）同理，IAQI=60时，PM2.5浓度为：
	 * //C=Clow+（Chigh-Clow）/（IAQIhigh-IAQIlow）*（IAQI-IAQIlow） //
	 * =35+（75-35）/（100-50）*（60-50）=35+0.8*10=43ug/m3 //故反推到的PM2.5浓度范围为21~43ug/m3.
	 */

	/**
	 * @return
	 */
	protected double CalculatorCp() {
		// 确定输入的IAQI所在的级别
		GetPollutantConLimitsFormIaqi();
		if (pollutantConLimits == null) {
			return -99;
		}
		// 如果IAQI大500或小于1，返回为-99
		if (index.getIndividualAirIndex() > 500 || index.getIndividualAirIndex() < 1) {
			return -99;
		}
		// 反向计算浓度
		double result = pollutantConLimits.getBpLo() + (pollutantConLimits.getBpHi() - pollutantConLimits.getBpLo())
				/ (pollutantConLimits.getIaiHi() - pollutantConLimits.getIaiLo())
				* (index.getIndividualAirIndex() - pollutantConLimits.getIaiLo());

		if (ec == EvaluationCriteria.AQI && getEPollutants() != EPollutants.CO
				&& getEPollutants() != EPollutants.CO_1H) {
			result = result / 1000;
		}
		return result;
	}

	public double GetCp() {
		SetPollutantName();
		setCp(CalculatorCp());
		return getCp();
	}

	public IndividualAirBaseIndex GetIndividualAirBaseIndex() {
		SetPollutantName();
		CalculateAirIndex();
		return index;
	}

	public PollutantConLimits getPollutantConLimits() {
		return pollutantConLimits;
	}

	public void setPollutantConLimits(PollutantConLimits pollutantConLimits) {
		this.pollutantConLimits = pollutantConLimits;
	}

	public IndividualAirBaseIndex getIndex() {
		return index;
	}

	public void setIndex(IndividualAirBaseIndex index) {
		this.index = index;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		dataSource = dataSource;
	}

	public String getPollutantName() {
		return pollutantName;
	}

	public void setPollutantName(String pollutantName) {
		this.pollutantName = pollutantName;
	}

	public double getCp() {
		return cp;
	}

	public void setCp(double cp) {
		this.cp = cp;
	}

	public EPollutants getEPollutants() {
		return EPollutants;
	}

	public void setEPollutants(EPollutants ePollutants) {
		EPollutants = ePollutants;
	}

	public EvaluationCriteria getEc() {
		return ec;
	}

	public void setEc(EvaluationCriteria ec) {
		this.ec = ec;
	}
}