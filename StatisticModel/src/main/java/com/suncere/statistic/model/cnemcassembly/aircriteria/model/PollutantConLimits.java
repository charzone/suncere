package com.suncere.statistic.model.cnemcassembly.aircriteria.model;

/** 
 污染物浓度限值
 
*/
public class PollutantConLimits
{
	private int id;
	/** 
	 污染物代号
	 
	 */
	private String pollutantCode;
	/** 
	 与污染物相近的污染物浓度限制的低位值
	 
	 */
	private double bpLo;

	/** 
	 与污染物相近的污染物浓度限制的高位值
	 
	 */
	private double bpHi;
	/** 
	 与BP_Lo对应的空气质量分指数
	 */
	private int iaiLo;

	/** 
	 与BP_Hi对应的空气质量分指数
	 */
	private int iaiHi;

	/** 
	 对应指标（1.AQI;2.API）
	 */
	private int ec;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPollutantCode() {
		return pollutantCode;
	}

	public void setPollutantCode(String pollutantCode) {
		this.pollutantCode = pollutantCode;
	}

	public double getBpLo() {
		return bpLo;
	}

	public void setBpLo(double bpLo) {
		this.bpLo = bpLo;
	}

	public double getBpHi() {
		return bpHi;
	}

	public void setBpHi(double bpHi) {
		this.bpHi = bpHi;
	}

	public int getIaiLo() {
		return iaiLo;
	}

	public void setIaiLo(int iaiLo) {
		this.iaiLo = iaiLo;
	}

	public int getIaiHi() {
		return iaiHi;
	}

	public void setIaiHi(int iaiHi) {
		this.iaiHi = iaiHi;
	}

	public int getEc() {
		return ec;
	}

	public void setEc(int ec) {
		this.ec = ec;
	}
}