package com.suncere.statistic.model.cnemcassembly.aircriteria.model;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2019年12月31日
 */
public class AirBaseIndex extends AirIndex{
	/**
	 * aqi预报结果
	 */
	private int airIndex;
	/**
	 * 首要污染物预报结果
	 */
	private String primaryPollutant;
	/** 
	 超标污染物
	*/
	private String nonAttainmentPollutant;
	
	public int getAirIndex() {
		return airIndex;
	}
	public void setAirIndex(int airIndex) {
		this.airIndex = airIndex;
	}
	public String getPrimaryPollutant() {
		return primaryPollutant;
	}
	public void setPrimaryPollutant(String primaryPollutant) {
		this.primaryPollutant = primaryPollutant;
	}
	public String getNonAttainmentPollutant() {
		return nonAttainmentPollutant;
	}
	public void setNonAttainmentPollutant(String nonAttainmentPollutant) {
		this.nonAttainmentPollutant = nonAttainmentPollutant;
	}
}
