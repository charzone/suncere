package com.suncere.statistic.model.cnemcassembly;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.suncere.statistic.model.cnemcassembly.aircriteria.AirBaseIndexOpreate;
import com.suncere.statistic.model.cnemcassembly.aircriteria.IAQI;
import com.suncere.statistic.model.cnemcassembly.aircriteria.IndividualAirBaseIndexOpreate;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.AirBaseIndex;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.AirTarget;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.IndividualAirBaseIndex;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.RangeAirIndex;
import com.suncere.statistic.serivce.enums.EPollutants;
import com.suncere.statistic.serivce.enums.EvaluationCriteria;

/**
 * 范围AQI计算器（供预报一个AQI范围使用）
 * 
 */
public class RangeAqiCalculator {
	/**
	 * 通过两个AQIZ组去计算范围AQI
	 * 
	 * @param lowIndex  低
	 * @param highIndex 高
	 * @return
	 */
	private static RangeAirIndex CalRangeAirIndex(AirBaseIndex lowIndex, AirBaseIndex highIndex) {
		RangeAirIndex tempVar = new RangeAirIndex();
		tempVar.setAqiHigh(highIndex.getAirIndex());
		tempVar.setAqiLow(lowIndex.getAirIndex());
		tempVar.setMeasure(highIndex.getMeasure());
		tempVar.setUnheathful(highIndex.getUnheathful());
		tempVar.setPrimaryPollutant(highIndex.getPrimaryPollutant());
		RangeAirIndex result = tempVar;

		if (!highIndex.getType().equals(lowIndex.getType())) // 如果不是优，那么就应该有首要污染物了
		{
			result.setAqiType(lowIndex.getType() + "~" + highIndex.getType());
		}
		if (!highIndex.getChineseLevel().equals(lowIndex.getChineseLevel())) {
			result.setChineseLevel(lowIndex.getChineseLevel() + "~" + highIndex.getChineseLevel());
		}
		return result;

	}

	/**
	 * 计算浓度去计算AQI
	 * 
	 * @param PollutantMonValue
	 * @param isDay
	 * @return
	 */
	private static AirBaseIndex CalAirIndex(HashMap<String, BigDecimal> PollutantMonValue, boolean isDay) {
		ArrayList<IndividualAirBaseIndex> iaqis = new ArrayList<IndividualAirBaseIndex>();
		for (Map.Entry<String, BigDecimal> item : PollutantMonValue.entrySet()) {
			IndividualAirBaseIndexOpreate iabi;
			iabi = new IAQI();
			iabi.setCp(Double.parseDouble(item.getValue().toString()));
			iabi.setEPollutants(isDay ? getEPollutantsNameDay(item.getKey()) : getEPollutantsNameHour(item.getKey()));
			iaqis.add(iabi.GetIndividualAirBaseIndex());
		}
		// 空气质量指数计算测试
		AirBaseIndexOpreate<IndividualAirBaseIndex> aqi = new AirBaseIndexOpreate<IndividualAirBaseIndex>(iaqis,
				EvaluationCriteria.AQI);
		return aqi.GetAirIndex();
	}

	/// #region 计算小时的AQI
	public static RangeAirIndex getResultHour(HashMap<String, BigDecimal> PollutantMonValue_Lower,
			HashMap<String, BigDecimal> PollutantMonValue_Higher, EvaluationCriteria Mark) {
		try {
			AirBaseIndex lowerAqi = CalAirIndex(PollutantMonValue_Lower, false);
			AirBaseIndex heighAqi = CalAirIndex(PollutantMonValue_Higher, false);
			return CalRangeAirIndex(lowerAqi, heighAqi);
		} catch (RuntimeException e) {
			return new RangeAirIndex();
		}
	}

	/// #region 计算天的AQI
	public static RangeAirIndex getResultDay(HashMap<String, BigDecimal> PollutantMonValue_Lower,
			HashMap<String, BigDecimal> PollutantMonValue_Higher, EvaluationCriteria Mark) {
		try {
			AirBaseIndex lowerAqi = CalAirIndex(PollutantMonValue_Lower, true);
			AirBaseIndex heighAqi = CalAirIndex(PollutantMonValue_Higher, true);
			return CalRangeAirIndex(lowerAqi, heighAqi);
		} catch (RuntimeException e) {
			return new RangeAirIndex();
		}
	}

	/// #region 通过污染物名称获取污染物类型（日）
	/**
	 * 通过污染物名称获取污染物类型（日）
	 * 
	 * @param p
	 * @return
	 */
	private static EPollutants getEPollutantsNameDay(String p) {
		if (p.equals("SO2")) {
			return EPollutants.SO2;
		} else if (p.equals("NO2")) {
			return EPollutants.NO2;
		} else if (p.equals("O3")) {
			return EPollutants.O3_8H;
		} else if (p.equals("O3_8H")) {
			return EPollutants.O3_8H;
		} else if (p.equals("PM10")) {
			return EPollutants.PM10;
		} else if (p.equals("PM2_5")) {
			return EPollutants.PM2_5;
		} else if (p.equals("CO")) {
			return EPollutants.CO;
		} else {
			return EPollutants.O3_8H;
		}
	}

	/// #region 通过污染物名称获取污染物类型（小时）
	/**
	 * 通过污染物名称获取污染物类型（小时）
	 * 
	 * @param p
	 * @return
	 */
	private static EPollutants getEPollutantsNameHour(String p) {
		if (p.equals("SO2")) {
			return EPollutants.SO2_1H;
		} else if (p.equals("NO2")) {
			return EPollutants.NO2_1H;
		} else if (p.equals("O3")) {
			return EPollutants.O3_1H;
		} else if (p.equals("O3_8H")) {
			return EPollutants.O3_8H;
		} else if (p.equals("PM10")) {
			return EPollutants.PM10;
		} else if (p.equals("PM2_5")) {
			return EPollutants.PM2_5;
		} else if (p.equals("CO")) {
			return EPollutants.CO_1H;
		} else {
			return EPollutants.O3_8H;
		}
	}

	/**
	 * 根据AQI范围获取空气质量类型
	 * 
	 * @param aqiLow  AQI下限
	 * @param aqiHigh AQI上限
	 * @return 空气质量类型
	 */
	public static String GetTypeByAQIRange(Integer aqiLow, Integer aqiHigh) {
		String type = "";
		AirTarget airIndexLow = new AirTarget(aqiLow);
		AirTarget airIndexHigh = new AirTarget(aqiHigh);
		if (airIndexLow.getType().equals(airIndexHigh.getType())) {
			type = airIndexLow.getType();
		} else {
			type = airIndexLow.getType() + "~" + airIndexHigh.getType();
		}
		return type;
	}

	/**
	 * 根据AQI范围获取空气质量中文等级
	 * 
	 * @param aqiLow  AQI下限
	 * @param aqiHigh AQI上限
	 * @return 空气质量中文等级
	 */
	public static String GetChineseLevelByAQIRange(Integer aqiLow, Integer aqiHigh) {
		String chineseLevel = "";
		AirTarget airIndexLow = new AirTarget(aqiLow);
		AirTarget airIndexHigh = new AirTarget(aqiHigh);
		if (airIndexLow.getChineseLevel().equals(airIndexHigh.getChineseLevel())) {
			chineseLevel = airIndexLow.getChineseLevel();
		} else {
			chineseLevel = airIndexLow.getChineseLevel() + "~" + airIndexHigh.getChineseLevel();
		}
		return chineseLevel;
	}

	/**
	 * 根据AQI值获取空气质量类型
	 * 
	 * @param aqi
	 * @return
	 */
	public static String GetTypeByAQI(Integer aqi) {
		String type = "";
		AirTarget airIndex = new AirTarget(aqi);
		type = airIndex.getType();
		return type;
	}
}