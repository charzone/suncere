package com.suncere.statistic.model.cnemcassembly.aircriteria.model;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2019年12月31日
 */
public class RangeAirIndex {
	private String aqiType;
    private String primaryPollutant;
    private int aqiLow;
    private int aqiHigh;
    private String chineseLevel;
    private String unheathful;
    private String measure;
	public String getAqiType() {
		return aqiType;
	}
	public void setAqiType(String aqiType) {
		this.aqiType = aqiType;
	}
	public String getPrimaryPollutant() {
		return primaryPollutant;
	}
	public void setPrimaryPollutant(String primaryPollutant) {
		this.primaryPollutant = primaryPollutant;
	}
	public int getAqiLow() {
		return aqiLow;
	}
	public void setAqiLow(int aqiLow) {
		this.aqiLow = aqiLow;
	}
	public int getAqiHigh() {
		return aqiHigh;
	}
	public void setAqiHigh(int aqiHigh) {
		this.aqiHigh = aqiHigh;
	}
	public String getChineseLevel() {
		return chineseLevel;
	}
	public void setChineseLevel(String chineseLevel) {
		this.chineseLevel = chineseLevel;
	}
	public String getUnheathful() {
		return unheathful;
	}
	public void setUnheathful(String unheathful) {
		this.unheathful = unheathful;
	}
	public String getMeasure() {
		return measure;
	}
	public void setMeasure(String measure) {
		this.measure = measure;
	}
}
