package com.suncere.statistic.model.cnemcassembly.aircriteria.model;

public class PollutantInfo
{
	/** 
	 主键值
	 
	*/
	private int privateID;
	public final int getID()
	{
		return privateID;
	}
	public final void setID(int value)
	{
		privateID = value;
	}

	/** 
	 污染物代号
	 
	*/
	private String privateCode;
	public final String getCode()
	{
		return privateCode;
	}
	public final void setCode(String value)
	{
		privateCode = value;
	}

	/** 
	 污染物名称
	 
	*/
	private String privateName;
	public final String getName()
	{
		return privateName;
	}
	public final void setName(String value)
	{
		privateName = value;
	}

	/** 
	 污染物中文名称
	 
	*/
	private String privateChineseName;
	public final String getChineseName()
	{
		return privateChineseName;
	}
	public final void setChineseName(String value)
	{
		privateChineseName = value;
	}
}