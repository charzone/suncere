package com.suncere.statistic.model.dataprocessing;

public class AforgeNeuronExtend {
	/**
	 * 合并多个一维数组至二维数组（转置合并）
	 * 
	 * @param input
	 * @return
	 */
	public static double[][] Combine(double[]... input) {
		int column = input.length;
		int row = input[0].length;

		double[][] output = new double[row][];

		for (int r = 0; r < row; r++) {
			double[] a = new double[column];
			for (int c = 0; c < column; c++) {
				a[c] = input[c][r];
			}
			output[r] = a;
		}

		return output;
	}

	/**
	 * 得到矩阵每列的最大最小值
	 * 
	 */
	public static double[][] Range(double[][] input) {
		int row = input.length;
		int column = input[0].length;
		double[][] range = new double[2][];
		range[0] = new double[column];
		range[1] = new double[column];
		for (int c = 0; c < column; c++) {
			range[0][c] = input[0][c];
			range[1][c] = input[0][c];
			for (int r = 1; r < row; r++) {
				range[0][c] = Math.min(range[0][c], input[r][c]);
				range[1][c] = Math.max(range[1][c], input[r][c]);
			}
		}

		return range;
	}

	/**
	 * 将矩阵按行归一化，得到一个新矩阵（极值已知)
	 * 
	 * @param input 要归一化的矩阵
	 * @param m     极值矩阵
	 * @param range 要归一化的正负范围
	 * @return 原矩阵经过归一化得到的新矩阵
	 */
	public static double[][] Normalization(double[][] input, double[][] m, double range) {
		int row = input.length;
		int column = input[0].length;

		double[][] nolmalized = new double[row][];
		for (int r = 0; r < row; r++) {
			nolmalized[r] = new double[column];
		}

		for (int c = 0; c < column; c++) {
			for (int r = 0; r < row; r++) {
				if (m[0][c] == m[1][c]) {
					nolmalized[r][c] = 1;
				} else {
					nolmalized[r][c] = (input[r][c] - m[0][c]) / (m[1][c] - m[0][c]) * 2 * range - range;
				}
			}
		}

		return nolmalized;
	}

	/**
	 * 将矩阵按行反归一化，得到一个新矩阵
	 * 
	 * @param input 要反归一化的矩阵
	 * @param m     极值矩阵
	 * @param range 要归一化的正负范围
	 * @return 反归一化得到新矩阵
	 */
	public static double[][] Inverse_Normalization(double[][] input, double[][] m, double range) {
		int row = input.length;
		int column = input[0].length;

		double[][] inverse_nolmalized = new double[row][];
		for (int r = 0; r < row; r++) {
			inverse_nolmalized[r] = new double[column];
		}

		for (int c = 0; c < column; c++) {
			for (int r = 0; r < row; r++) {
				if (m[0][c] == m[1][c]) {
					inverse_nolmalized[r][c] = m[0][c];
				} else {
					inverse_nolmalized[r][c] = (input[r][c] + range) / 2 / range * (m[1][c] - m[0][c]) + m[0][c];
				}
			}
		}

		return inverse_nolmalized;
	}

	/**
	 * 将向量归一化，得到一个新向量（极值已知)
	 * 
	 * @param input 要归一化的向量阵
	 * @param m     极值矩阵
	 * @param range 要归一化的正负范围
	 * @return 原矩阵经过归一化得到的新向量
	 */
	public static double[] Normalization(double[] input, double[][] m, double range) {
		int column = input.length;
		double[] nolmalized = new double[column];
		for (int c = 0; c < column; c++) {
			if (m[0][c] == m[1][c]) {
				nolmalized[c] = 1;
			} else {
				nolmalized[c] = (input[c] - m[0][c]) / (m[1][c] - m[0][c]) * 2 * range - range;
			}
		}
		return nolmalized;
	}

	/**
	 * 将向量反归一化，得到一个新向量
	 * 
	 * @param input 要反归一化的向量
	 * @param m     极值矩阵
	 * @param range 要归一化的正负范围
	 * @return 反归一化得到新向量
	 */
	public static double[] Inverse_Normalization(double[] input, double[][] m, double range) {
		int column = input.length;

		double[] inverse_nolmalized = new double[column];

		for (int c = 0; c < column; c++) {
			if (m[0][c] == m[1][c]) {
				inverse_nolmalized[c] = m[0][c];
			} else {
				inverse_nolmalized[c] = (input[c] + range) / 2 / range * (m[1][c] - m[0][c]) + m[0][c];
			}
		}

		return inverse_nolmalized;
	}
}