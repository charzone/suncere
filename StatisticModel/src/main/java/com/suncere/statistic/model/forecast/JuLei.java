package com.suncere.statistic.model.forecast;

import com.suncere.statistic.entity.EntityModel.PollutantForecastMetaData;
import com.suncere.statistic.entity.EntityModel.PollutantMonitorData;
import com.suncere.statistic.entity.EntityModel.WeatherForecastData;
import com.suncere.statistic.entity.EntityModel.WeatherMonitorData;
import com.suncere.statistic.model.BaseStatisticModel;
import com.suncere.statistic.model.dataprocessing.Cluster;
import com.suncere.statistic.model.dataprocessing.Group;
import com.suncere.statistic.model.dataprocessing.MultipleRegressionFittingData;
import com.suncere.statistic.model.dataprocessing.MultipleRegressionOperate;
import com.suncere.statistic.serivce.enums.PollutantParam;
import com.suncere.statistic.utils.MathUtil;
import org.deeplearning4j.eval.RegressionEvaluation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * 聚类回归模型 Creator:Denggq CreateTime:2015/11/5
 */
public class JuLei extends BaseStatisticModel {
	/// #region 公共属性

	/**
	 * 样本在聚类过程中的分类数目
	 * 
	 */
	private int classifyNumber;

	// 模型参数系数（按上述给定顺序）
	private BigDecimal xPollutant_Coefficient = null;
	private BigDecimal airTemperature_Coefficient = null;
	// decimal high_AirTemperature_Coefficient = (decimal)mrPollutant.a[1]);
	private BigDecimal WindSpeed_Coefficient = null;
	private BigDecimal humidity_Coefficient = null;
	private BigDecimal Intercept_Coefficient = null;
	// 模型性能参数
	private BigDecimal Q = null;
	
	/// #region 构造函数
	public JuLei() {
		super();
	}

	public JuLei(String code, String cityCode,LocalDateTime timePoint) {
		super(code, cityCode,timePoint);
		this.sampleCount = 40;
		this.factorCount = 4;
		this.forecastDayCount = 7;

		this.forecastModelId = 5;

		this.classifyNumber = 3;
		this.beginTime = timePoint.plusDays(-this.sampleCount - 8);// 样本数据是开始时间为样本数+8天
		this.endTime = timePoint.plusDays(-1);
	}

	/**
	 * 聚类回归模型构造函数
	 * 
	 * @param code             编码（城市、区域、站点）
	 * @param sampleCount      模型样本数
	 * @param factorCount      模型自变量个数
	 * @param timePoint        日期
	 * @param forecastDayCount 预报天数
	 */
	public JuLei(String code,String cityCode,int sampleCount, int factorCount, LocalDateTime timePoint, int forecastDayCount) {
		super(code, cityCode,sampleCount, factorCount, timePoint, forecastDayCount);
		this.forecastModelId = 5;

		this.classifyNumber = 3;
		this.beginTime = timePoint.plusDays(-this.sampleCount - 8);// 样本数据是开始时间为样本数+8天
		this.endTime = timePoint.plusDays(-1);
	}

	/**
	 * 因变量
	 */
	private double[] yPollutant;
	/**
	 * 昨日的污染物的浓度值
	 */
	private BigDecimal yesterdayPollutant = new BigDecimal(0);
	/**
	 * 样本数据列表
	 */
	private ArrayList<HashMap<String, BigDecimal>> dataList;
	/**
	 * 参数数据的取值范围
	 */
	private HashMap<String, BigDecimal>[] rangeList;
	/**
	 * 样本的分类
	 */
	private ArrayList<Group> groupList_0;

	/**
	 * 数据完整性检查
	 */
	@Override
	protected void DataCompletionCheck() {
		if (this.pollutantMonitorList == null || this.pollutantMonitorList.isEmpty()) {
			throw new RuntimeException("缺失污染物监测数据");
		}
		if (this.weatherMonitorList == null || this.weatherMonitorList.isEmpty()) {
			throw new RuntimeException("缺失气象监测数据");
		}

		// 按样本时间倒序排序,后面会根据索引下标i+1获取前一天样本
		this.pollutantMonitorList = pollutantMonitorList.stream()
				.sorted(Comparator.comparing(PollutantMonitorData::getTimePoint).reversed())
				.collect(Collectors.toList());
		this.weatherMonitorList = weatherMonitorList.stream()
				.sorted(Comparator.comparing(WeatherMonitorData::getTimePoint).reversed()).collect(Collectors.toList());
		if (this.weatherForecastList != null) {
			this.weatherForecastList = weatherForecastList.stream()
					.sorted(Comparator.comparing(WeatherForecastData::getForTime)).collect(Collectors.toList());
		}
	}

	/**
	 * 数据准备
	 * 
	 * @param pollutantName
	 */
	@Override
	protected void DataPrepare(String pollutantName) {
		/// #region 数据预处理
		yPollutant = new double[this.sampleCount];
		double[] xPollutant = new double[this.sampleCount];
		double[] windSpeed = new double[this.sampleCount];
		double[] airTemperature = new double[this.sampleCount];
		double[] humidity = new double[this.sampleCount];

		for (int i = 0; i < this.sampleCount; i++) {
			if (pollutantName.equals(PollutantParam.SO2.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getSo2().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getSo2().doubleValue();
			} else if (pollutantName.equals(PollutantParam.NO2.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getNo2().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getNo2().doubleValue();
			} else if (pollutantName.equals(PollutantParam.CO.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getCo().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getCo().doubleValue();
			} else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getPm2_5().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm2_5().doubleValue();
			} else if (pollutantName.equals(PollutantParam.PM10.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getPm10().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm10().doubleValue();
			} else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getO3_8h().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getO3_8h().doubleValue();
			} else {
				yPollutant[i] = 0;
				xPollutant[i] = 0;
			}

			windSpeed[i] = this.weatherMonitorList.get(i + 1).getWindSpeed().doubleValue();
			airTemperature[i] = this.weatherMonitorList.get(i + 1).getAirTemperature().doubleValue();
			humidity[i] = this.weatherMonitorList.get(i + 1).getHumidity().doubleValue();
		}

		MultipleRegressionOperate.PreTreatment(yPollutant, this.sampleCount);
		MultipleRegressionOperate.PreTreatment(xPollutant, this.sampleCount);
		MultipleRegressionOperate.PreTreatment(windSpeed, this.sampleCount);
		MultipleRegressionOperate.PreTreatment(airTemperature, this.sampleCount);
		MultipleRegressionOperate.PreTreatment(humidity, this.sampleCount);

		/// #region 组织样本数据列表用于聚类
		dataList = new ArrayList<HashMap<String, BigDecimal>>();
		for (int i = 0; i < this.sampleCount; i++) {
			HashMap<String, BigDecimal> data = new HashMap<String, BigDecimal>();
			// 聚类的参数可通过在Cluster类的Distance方法里修改clusterFactorList变量更改
			data.put("airTemperature", BigDecimal.valueOf(airTemperature[i]));
			// data.Add("high_AirTemperature", monMetDayDatas[i].High_AirTemp.Value);
			data.put("windSpeed", BigDecimal.valueOf(windSpeed[i]));
			data.put("humidity", BigDecimal.valueOf(humidity[i]));
			if (pollutantName.equals(PollutantParam.SO2.name())) {
				data.put("yPollutant", BigDecimal.valueOf(yPollutant[i]));
				data.put("xPollutant", BigDecimal.valueOf(xPollutant[i]));
			} else if (pollutantName.equals(PollutantParam.NO2.name())) {
				data.put("yPollutant", BigDecimal.valueOf(yPollutant[i]));
				data.put("xPollutant", BigDecimal.valueOf(xPollutant[i]));
			} else if (pollutantName.equals(PollutantParam.CO.name())) {
				data.put("yPollutant", BigDecimal.valueOf(yPollutant[i]));
				data.put("xPollutant", BigDecimal.valueOf(xPollutant[i]));
			} else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
				data.put("yPollutant", BigDecimal.valueOf(yPollutant[i]));
				data.put("xPollutant", BigDecimal.valueOf(xPollutant[i]));
			} else if (pollutantName.equals(PollutantParam.PM10.name())) {
				data.put("yPollutant", BigDecimal.valueOf(yPollutant[i]));
				data.put("xPollutant", BigDecimal.valueOf(xPollutant[i]));
			} else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
				data.put("yPollutant", BigDecimal.valueOf(yPollutant[i]));
				data.put("xPollutant", BigDecimal.valueOf(xPollutant[i]));
			} else {
				data.put("yPollutant", BigDecimal.valueOf(0));
				data.put("xPollutant", BigDecimal.valueOf(0));
			}
			dataList.add(data);
		}

		yesterdayPollutant = BigDecimal.valueOf(yPollutant[0]); // 赋值，昨日的该种污染物的浓度

		/// #region 聚类过程
		rangeList = Cluster.Range(dataList); // 归一化过程－求出参数数据的取值范围

		ArrayList<HashMap<String, BigDecimal>> dataList_0 = Cluster.Normalization(dataList, rangeList, BigDecimal.valueOf(1)); // 归一化过程－参数数据的取值范围转化为[-1,1]
		groupList_0 = Cluster.HierarchicalCluster(dataList_0, this.classifyNumber); // 样本的分类 还没反归一化
	}
	
	private boolean fit(double[][] input, double[] output) {
		// 模型拟合 自变量x按 前一天监测参数、气温、风向、风速 顺序拟合
		MultipleRegressionFittingData mrPollutant = MultipleRegressionOperate.MultipleRegressionFitting(
				input, output,
				getFactorCount(), input[0].length);
		if (!Double.isNaN(mrPollutant.getR())) {
			// 模型参数系数（按上述给定顺序）
			xPollutant_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[0]);
			airTemperature_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[1]);
			// decimal high_AirTemperature_Coefficient = (decimal)mrPollutant.a[1]);
			WindSpeed_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[2]);
			humidity_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[3]);
			Intercept_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[4]);
			// 模型性能参数
			Q = BigDecimal.valueOf(mrPollutant.getQ());
//			BigDecimal S = BigDecimal.valueOf(mrPollutant.getS());
//			BigDecimal R = BigDecimal.valueOf(mrPollutant.getR());
//			BigDecimal U = BigDecimal.valueOf(mrPollutant.getU());

			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 输入自变量，利用模型进行预测
	 * @param xPollutant
	 * @param airTemperature
	 * @param windSpeed
	 * @param humidity
	 * @return
	 */
	private BigDecimal modelPredict(BigDecimal xPollutant, BigDecimal airTemperature, BigDecimal windSpeed, BigDecimal humidity) {
		return xPollutant_Coefficient.multiply(xPollutant)
		.add(airTemperature_Coefficient.multiply(airTemperature))
		.add(WindSpeed_Coefficient.multiply(windSpeed))
		.add(humidity_Coefficient.multiply(humidity))
		.add(Intercept_Coefficient);
	}
	
	/**
	 * 模型评估
	 */
	private void eval(double[] xPollutant, double[] airTemperature, double[] windSpeed,
			double[] humidity, double[] yPollutant) {
		double[] lableArray = new double[xPollutant.length];
		for(int i=0; i<xPollutant.length; i++) {
			lableArray[i] = modelPredict(BigDecimal.valueOf(xPollutant[i]), BigDecimal.valueOf(airTemperature[i]), BigDecimal.valueOf(windSpeed[i]), BigDecimal.valueOf(humidity[i])).doubleValue();
		}
        INDArray labels = Nd4j.create(lableArray);
        INDArray predictions = Nd4j.create(yPollutant);
		RegressionEvaluation eval = new RegressionEvaluation();
		eval.eval(labels, predictions);
//	    System.out.println("评估模型，RSquared:"+eval.averageRSquared());
	}

	/**
	 * 预报过程
	 * 
	 */
	@Override
	protected void Predict() {
		DataCompletionCheck();

		for (String pollutantName : pollutantNames) {
			DataPrepare(pollutantName);

			/// #region 计算预报数据

			ArrayList<PollutantForecastMetaData> result = new ArrayList<PollutantForecastMetaData>();
			// 预报日的各输入参数
			HashMap<String, BigDecimal> predictInput = new HashMap<String, BigDecimal>();
			predictInput.put("airTemperature", BigDecimal.valueOf(0));
			// predictInput.Add("high_AirTemperature", 0);
			predictInput.put("windSpeed", BigDecimal.valueOf(0));
//			predictInput.put("windDirection", BigDecimal.valueOf(0));
			predictInput.put("humidity", BigDecimal.valueOf(0));
			predictInput.put("xPollutant", BigDecimal.valueOf(0));

			int dayindex = 0; // 代表预报日的天数，先预报第一天，故dayindex=0，每预报完一天dayindex+1
			for (LocalDateTime forTime = this.timePoint, endForTime = this.timePoint.plusDays(
					this.forecastDayCount - 1); forTime.compareTo(endForTime) <= 0; forTime = forTime.plusDays(1)) // 7天预报数据，从当前天到后6天
			{
				PollutantForecastMetaData fd = new PollutantForecastMetaData();
				fd.setTimePoint(getTimePoint());
				fd.setForTime(forTime);
				fd.setPolllutantName(pollutantName);

				// 组织预报日的各输入参数，构成一条样本，样本结构与上述的数据列表相同
				predictInput.put("airTemperature", getWeatherForecastList().get(dayindex).getAirTemp());
				// predictInput["high_AirTemperature"] =
				// forMetDayDatas[dayindex].High_AirTemp.Value;
				predictInput.put("windSpeed", getWeatherForecastList().get(dayindex).getWindSpeed());
//				predictInput.put("windDirection", getWeatherForecastList().get(dayindex).getWindDirection());
				predictInput.put("humidity", getWeatherForecastList().get(dayindex).getRelativeHumidity());
				if (dayindex == 0) {
					predictInput.put("xPollutant", yesterdayPollutant);
				}
				predictInput.put("yPollutant", yesterdayPollutant); // 该值任取，使样本结构统一

				/// #region 该样本（预报日）的分类过程

				HashMap<String, BigDecimal> predictInput_0 = Cluster.Normalization(predictInput, rangeList, BigDecimal.valueOf(1)); // 归一化

				int judgeGroupId = Cluster.Judge(predictInput_0, groupList_0); // 判断该样本属于哪个分类

				Group judgeGroup_0 = Group.clone(groupList_0.get(judgeGroupId));
				// 防止这个分类的样本数过小
				if (judgeGroup_0.sampleCount < 10) {
					for (int i = 0; i < this.classifyNumber; i++) {
						if (judgeGroupId != i) {
							judgeGroup_0.MergeGroup(groupList_0.get(i));
						}
						if (judgeGroup_0.sampleCount >= 10) {
							break;
						}
					}
				}

				// 该类的样本反归一化
				Group judgeGroup = new Group();
				judgeGroup.data = Cluster.Inverse_Normalization(judgeGroup_0.data, rangeList, BigDecimal.valueOf(1));
				judgeGroup.CalculateGroupCenter();
				judgeGroup.sampleCount = judgeGroup_0.sampleCount;

				// 模型的输入数据
				double[] yPollutant_New = new double[judgeGroup.sampleCount]; // 因变量
				double[] xPollutant_New = new double[judgeGroup.sampleCount]; // 前一天污染物浓度

				double[] windSpeed_New = new double[judgeGroup.sampleCount]; // 日均风速
				double[] airTemperature_New = new double[judgeGroup.sampleCount]; // 日均温度
				// double[] high_AirTemperature_New = new double[judgeGroup.sampleCount];
				// //日最高温度
				double[] humidity_New = new double[judgeGroup.sampleCount]; // 日均相对湿度

				// 初始数据数据填充（该样本所属的那个分类）
				for (int i = 0; i < judgeGroup.sampleCount; i++) {
					yPollutant_New[i] = judgeGroup.data.get(i).get("yPollutant").doubleValue();
					xPollutant_New[i] = judgeGroup.data.get(i).get("xPollutant").doubleValue();
					windSpeed_New[i] = judgeGroup.data.get(i).get("windSpeed").doubleValue();
					airTemperature_New[i] = judgeGroup.data.get(i).get("airTemperature").doubleValue();
					// high_AirTemperature_New[i] =
					// (double)judgeGroup.data[i]["high_AirTemperature"];
					humidity_New[i] = judgeGroup.data.get(i).get("humidity").doubleValue();
				}

				/// #region 预报数据计算

				// 模型拟合 自变量x按 前一天监测参数、气温、风向、风速 顺序拟合
				if (fit(new double[][] { xPollutant_New, airTemperature_New, windSpeed_New, humidity_New }, yPollutant_New)) {
					eval(xPollutant_New, airTemperature_New, windSpeed_New, humidity_New, yPollutant_New);
					
					BigDecimal fdValue = modelPredict(predictInput.get("xPollutant"), predictInput.get("airTemperature"), predictInput.get("windSpeed"), predictInput.get("humidity"));
					// fd.Value = Localization.Localize(fd.Value, pollutantName, forTime); //
					// 对预报数值进行本地化
					fd.setValue(MathUtil.roundBank(fdValue.doubleValue(), 3));
					BigDecimal floatingValue = BigDecimal.valueOf(Math.sqrt((1 / (this.sampleCount - this.factorCount - 1))) * Q.doubleValue());
					fd.setFloatingValue(MathUtil.roundBank(floatingValue.doubleValue(), 3));

					result.add(fd);
				} else {
					break;
				}

				dayindex++;
				predictInput.put("xPollutant", fd.getValue()); // 把前一天的预报值赋值给下一个预报日的输入参数中的xPollutant
			}

			forMetaDataList.addAll(result);
		}
	}

	/**
	 * 预报过程（没有气象预报数据时所用的备用方法）
	 * 
	 */
	@Override
	protected void BackupPredict() {
		for (String pollutantName : pollutantNames) {
			ArrayList<PollutantForecastMetaData> result = new ArrayList<PollutantForecastMetaData>();

			/// #region 预报日的各输入参数

			HashMap<String, BigDecimal> predictInput = new HashMap<String, BigDecimal>();
			predictInput.put("airTemperature", getWeatherMonitorList().get(0).getAirTemperature());
			predictInput.put("windDirection", getWeatherMonitorList().get(0).getWindDirection());
			predictInput.put("windSpeed", getWeatherMonitorList().get(0).getWindSpeed());
			predictInput.put("humidity", getWeatherMonitorList().get(0).getHumidity());
			predictInput.put("rainfall", getWeatherMonitorList().get(0).getRainfall());
			predictInput.put("pressure", getWeatherMonitorList().get(0).getPressure());
			
			if (pollutantName.equals(PollutantParam.SO2.name())) {
				predictInput.put("yPollutant", BigDecimal.valueOf(0));
				predictInput.put("xPollutant", getPollutantMonitorList().get(0).getSo2());
			}
			else if (pollutantName.equals(PollutantParam.NO2.name())) {
				predictInput.put("yPollutant", BigDecimal.valueOf(0));
				predictInput.put("xPollutant", getPollutantMonitorList().get(0).getNo2());
			}
			else if (pollutantName.equals(PollutantParam.CO.name())) {
				predictInput.put("yPollutant", BigDecimal.valueOf(0));
				predictInput.put("xPollutant", getPollutantMonitorList().get(0).getCo());
			}
			else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
				predictInput.put("yPollutant", BigDecimal.valueOf(0));
				predictInput.put("xPollutant", getPollutantMonitorList().get(0).getPm2_5());
			}
			else if (pollutantName.equals(PollutantParam.PM10.name())) {
				predictInput.put("yPollutant", BigDecimal.valueOf(0));
				predictInput.put("xPollutant", getPollutantMonitorList().get(0).getPm10());
			}
			else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
				predictInput.put("yPollutant", BigDecimal.valueOf(0));
				predictInput.put("xPollutant", getPollutantMonitorList().get(0).getO3_8h());
			} else {
				predictInput.put("yPollutant", BigDecimal.valueOf(0));
				predictInput.put("xPollutant", BigDecimal.valueOf(0));
			}

			/// #region 预报数据计算

			// 计算白天的预报数据
			int dayindex = 1;
			for (LocalDateTime forTime = this.timePoint, endForTime = this.timePoint.plusDays(this.forecastDayCount - 1); forTime
					.compareTo(endForTime) <= 0; forTime = forTime.plusDays(1)) // 7天预报数据，从当前天到后6天
			{
				PollutantForecastMetaData fd = new PollutantForecastMetaData();
				fd.setTimePoint(getTimePoint());
				fd.setForTime(forTime);
				fd.setPolllutantName(pollutantName);

				/// #region 整合数据列表用于聚类

				dataList = new ArrayList<HashMap<String, BigDecimal>>();
				for (int i = 0; i < this.sampleCount; i++) {
					HashMap<String, BigDecimal> data = new HashMap<String, BigDecimal>();
					data.put("airTemperature", getWeatherMonitorList().get(i + dayindex).getAirTemperature());
					data.put("windDirection", getWeatherMonitorList().get(i + dayindex).getWindDirection());
					data.put("windSpeed", getWeatherMonitorList().get(i + dayindex).getWindSpeed());
					data.put("humidity", getWeatherMonitorList().get(i + dayindex).getHumidity());
					data.put("rainfall", getWeatherMonitorList().get(i + dayindex).getRainfall());
					data.put("pressure", getWeatherMonitorList().get(i + dayindex).getPressure());
					if (pollutantName.equals(PollutantParam.SO2.name())) {
						data.put("yPollutant", getPollutantMonitorList().get(i).getSo2());
						data.put("xPollutant", getPollutantMonitorList().get(i + 1).getSo2());
					}
					else if (pollutantName.equals(PollutantParam.NO2.name())) {
						data.put("yPollutant", getPollutantMonitorList().get(i).getNo2());
						data.put("xPollutant", getPollutantMonitorList().get(i + 1).getNo2());
					}
					else if (pollutantName.equals(PollutantParam.CO.name())) {
						data.put("yPollutant", getPollutantMonitorList().get(i).getCo());
						data.put("xPollutant", getPollutantMonitorList().get(i + 1).getCo());
					}
					else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
						data.put("yPollutant", getPollutantMonitorList().get(i).getPm2_5());
						data.put("xPollutant", getPollutantMonitorList().get(i + 1).getPm2_5());
					}
					else if (pollutantName.equals(PollutantParam.PM10.name())) {
						data.put("yPollutant", getPollutantMonitorList().get(i).getPm10());
						data.put("xPollutant", getPollutantMonitorList().get(i + 1).getPm10());
					}
					else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
						data.put("yPollutant", getPollutantMonitorList().get(i).getO3_8h());
						data.put("xPollutant", getPollutantMonitorList().get(i + 1).getO3_8h());
					} else {
						data.put("yPollutant", BigDecimal.valueOf(0));
						data.put("xPollutant", BigDecimal.valueOf(0));
					}
					dataList.add(data);
				}

				/// #region 聚类、判断
				rangeList = Cluster.Range(dataList); // 归一化过程－求出参数数据的取值范围

				ArrayList<HashMap<String, BigDecimal>> dataList_0 = Cluster.Normalization(dataList, rangeList, BigDecimal.valueOf(1)); // 归一化过程－参数数据的取值范围转化为[-1,1]
				groupList_0 = Cluster.HierarchicalCluster(dataList_0, this.classifyNumber); // 样本的分类 还没反归一化

				HashMap<String, BigDecimal> predictInput_0 = Cluster.Normalization(predictInput, rangeList, BigDecimal.valueOf(1)); // 归一化

				int judgeGroupId = Cluster.Judge(predictInput_0, groupList_0); // 判断该样本属于哪个分类

				Group judgeGroup_0 = Group.clone(groupList_0.get(judgeGroupId));

				// 防止这个分类的样本数过小
				if (judgeGroup_0.sampleCount < 10) {
					for (int i = 0; i < this.classifyNumber; i++) {
						if (judgeGroupId != i) {
							judgeGroup_0.MergeGroup(groupList_0.get(i));
						}
						if (judgeGroup_0.sampleCount >= 10) {
							break;
						}
					}
				}

				// 该类的样本反归一化
				Group judgeGroup = new Group();
				judgeGroup.data = Cluster.Inverse_Normalization(judgeGroup_0.data, rangeList, BigDecimal.valueOf(1));
				judgeGroup.CalculateGroupCenter();
				judgeGroup.sampleCount = judgeGroup_0.sampleCount;

				// 预报用的数据
				yPollutant = new double[judgeGroup.sampleCount];
				double[] xPollutant = new double[judgeGroup.sampleCount];
				double[] windSpeed = new double[judgeGroup.sampleCount];
				double[] airTemperature = new double[judgeGroup.sampleCount];
				double[] humidity = new double[judgeGroup.sampleCount];

				// 初始数据数据填充
				for (int i = 0; i < judgeGroup.sampleCount; i++) {
					yPollutant[i] = judgeGroup.data.get(i).get("yPollutant").doubleValue();
					xPollutant[i] = judgeGroup.data.get(i).get("xPollutant").doubleValue();
					windSpeed[i] = judgeGroup.data.get(i).get("windSpeed").doubleValue();
					airTemperature[i] = judgeGroup.data.get(i).get("airTemperature").doubleValue();
					humidity[i] = judgeGroup.data.get(i).get("humidity").doubleValue();
				}

				// 预处理
				MultipleRegressionOperate.PreTreatment(yPollutant, judgeGroup.sampleCount);
				MultipleRegressionOperate.PreTreatment(xPollutant, judgeGroup.sampleCount);
				MultipleRegressionOperate.PreTreatment(windSpeed, judgeGroup.sampleCount);
				MultipleRegressionOperate.PreTreatment(airTemperature, judgeGroup.sampleCount);
				MultipleRegressionOperate.PreTreatment(humidity, judgeGroup.sampleCount);

				/// #region 模型拟合

				// 定义参数系数
				BigDecimal xPollutant_Coefficient = new BigDecimal(0);
				BigDecimal airTemperature_Coefficient = new BigDecimal(0);
				// decimal high_AirTemperature_Coefficient = 0;
				BigDecimal windSpeed_Coefficient = new BigDecimal(0);
				BigDecimal humidity_Coefficient = new BigDecimal(0);
				BigDecimal Intercept_Coefficient = new BigDecimal(0);
				BigDecimal Q = new BigDecimal(0);

				// 模型拟合 自变量x按 前一天监测参数、气温、风向、风速 顺序拟合
				MultipleRegressionFittingData mrPollutant = MultipleRegressionOperate.MultipleRegressionFitting(
						new double[][] { xPollutant, airTemperature, windSpeed, humidity }, yPollutant,
						getFactorCount(), judgeGroup.sampleCount);
				if (!Double.isNaN(mrPollutant.getR())) {
					// 模型参数系数（按上述给定顺序）
					xPollutant_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[0]);
					airTemperature_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[1]);
					// high_AirTemperature_Coefficient = (decimal)mrPollutant.a[1]);
					windSpeed_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[2]);
					humidity_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[3]);
					Intercept_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[4]);
					// 模型性能参数
					Q = BigDecimal.valueOf(mrPollutant.getQ());
//					BigDecimal S = BigDecimal.valueOf(mrPollutant.getS());
//					BigDecimal R = BigDecimal.valueOf(mrPollutant.getR());
//					BigDecimal U = BigDecimal.valueOf(mrPollutant.getU());

					BigDecimal fdValue = xPollutant_Coefficient.multiply(predictInput.get("xPollutant"))
							.add(airTemperature_Coefficient.multiply(predictInput.get("airTemperature")))
							.add(windSpeed_Coefficient.multiply(predictInput.get("windSpeed")))
							.add(humidity_Coefficient.multiply(predictInput.get("humidity")))
							.add(Intercept_Coefficient);
					// fd.Value = Localization.Localize(fd.Value, pollutantName, forTime); //
					// 对预报数值进行本地化
					fd.setValue(MathUtil.roundBank(fdValue.doubleValue(), 3));
					BigDecimal floatingValue = BigDecimal.valueOf(Math.sqrt((1 / (this.sampleCount - this.factorCount - 1))) * Q.doubleValue());
					fd.setFloatingValue(MathUtil.roundBank(floatingValue.doubleValue(), 3));

					result.add(fd);
				} else {
					break;
				}

				dayindex++;
				predictInput.put("xPollutant", fd.getValue()); // 把前一天的预报值赋值给下一个预报日的输入参数中的xPollutant
			}

			forMetaDataList.addAll(result);
		}
	}
}