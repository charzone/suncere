package com.suncere.statistic.model.cnemcassembly.aircriteria;

import com.suncere.statistic.serivce.enums.EvaluationCriteria;

/** 
 污染物P的空气质量分指数
 
*/
public class IAQI extends IndividualAirBaseIndexOpreate
{
	public IAQI()
	{
		super(EvaluationCriteria.AQI);
	}

	@Override
	protected void SetPollutantName()
	{
		switch (getEPollutants())
		{
			case SO2_1H:
				setPollutantName("SO2-1h");
				break;
			case SO2:
				setPollutantName("SO2");
				break;
			case NO2_1H:
				setPollutantName("NO2-1h");
				break;
			case NO2:
				setPollutantName("NO2");
				break;
			case PM10:
				setPollutantName("PM10");
				break;
			case CO_1H:
				setPollutantName("CO-1h");
				break;
			case CO:
				setPollutantName("CO");
				break;
			case O3_1H:
				setPollutantName("O3-1h");
				break;
			case O3_8H:
				setPollutantName("O3-8h");
				break;
			case PM2_5:
				setPollutantName("PM2.5");
				break;
			default:
				setPollutantName("");
				break;
		}
	}
}