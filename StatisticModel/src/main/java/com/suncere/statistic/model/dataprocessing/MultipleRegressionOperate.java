package com.suncere.statistic.model.dataprocessing;

import java.util.ArrayList;
import java.util.List;

import com.suncere.statistic.utils.MathUtil;

public class MultipleRegressionOperate {
	// 此值表示空缺数据
	private static double nullValue = -99;

	/**
	 * 单一样本数据预处理
	 * 
	 * @param array 样本数组
	 * @param n     样本元素数
	 * @return
	 */
	public static boolean PreTreatment(double[] array, int n) {
		// 异常值处理
		if (!AbnormalDeal(array, n)) {
			return false;
		}

		// 缺数回补处理
		if (!CompletionDeal(array, n)) {
			return false;
		}

		return true;
	}

	/**
	 * 数据回补处理
	 * 
	 * @param array 样本数组
	 * @param n     样本元素数
	 * @return
	 */
	public static boolean CoverTreatment(double[] array, int n) {
		// 缺数回补处理
		if (!CompletionDeal(array, n)) {
			return false;
		}

		return true;
	}

	/**
	 * 多元线性回归分析拟合计算
	 * 
	 * @param x 每一列存放m个自变量的观察值
	 * @param y 存放因变量y的n个观察值
	 * @param m 自变量的个数
	 * @param n 观察数据的组数
	 * @return
	 */
	public static MultipleRegressionFittingData MultipleRegressionFitting(double[][] x, double[] y, int m, int n) {
		MultipleRegressionFittingData result = new MultipleRegressionFittingData();
		double[] a = new double[m + 1]; // 回归系数a0,...,am, am为截距
		double[] v = new double[m]; // m个自变量的偏相关系数
		double[] dt = new double[4]; // dt[0]偏差平方和q,dt[1] 平均标准偏差s dt[2]返回复相关系数r dt[3]返回回归平方和u

		// 检验传入数据的合法性
		if (x.length != m || m == 0) {
			return null;
		}
		for (int i = 0; i < m; i++) {
			if (x[i].length != n || n == 0) {
				return null;
			}
		}
		MultiRegression.MulReg(x, y, m, n, a, dt, v);
		result.setA(a);
		result.setV(v);
		result.setQ(dt[0]);
		result.setS(dt[1]);
		result.setR(dt[2]);
		result.setU(dt[3]);
		return result;
	}

	/**
	 * 建立m元线性回归方程，对预报项目进行求解，获取预报值
	 * 
	 * @param a 回归系数，用于建立m元线性回归方程，m+1个，前m个按顺序对应各个自变量，第m+1个为截距
	 * @param x m个自变量的确定值，用于代入方程，求解预报值
	 * @param m 自变量个数
	 * @return
	 */
	public static double PredictionByMultipleRegression(double[] a, double[] x, int m) {
		// 合法性验证
		if (a.length != m + 1 || x.length != m + 1) {
			return 0;
		}

		double y = 0;
		for (int i = 0; i < m; i++) {
			y += a[i] * x[i];
		}
		y += a[m]; // 加上截距
		return y;
	}

	/// #region 数据预处理的内部方法,包括异常值处理,缺数补全
	/**
	 * 异常值处理，以[u-3s,u+3s]作为正常值区间,此区间外为异常值
	 * 
	 * @param data 原数组
	 * @param n    数组长度
	 * @return
	 */
	private static boolean AbnormalDeal(double[] data, int n) {
		// 异常值处理，以[u-3s,u+3s]作为正常值区间,此区间外为异常值
		double[] arr;
		double u = 0.0; // 均值
		double s = 0; // 标准差
		double lower = 0; // 正常值下限
		double upper = 0; // 正常值上限

		try {
			List<Double> dataList = new ArrayList<>();
			for (double d : data) {
				if (d != nullValue) {// 排除空值组成新数组
					dataList.add(d);
				}
			}
			arr = new double[dataList.size()];
			for (int i = 0; i < dataList.size(); i++) {
				arr[i] = dataList.get(i);
			}

			double sum = 0;
			for (double d : arr) {
				sum += d;
			}
			u = sum / arr.length; // 计算均值

			s = MathUtil.StandardDeviation(arr); // 计算标准差
			lower = u - 3 * s; // 正常值下限
			upper = u + 3 * s; // 正常值上限
			for (int i = 0; i < n; i++) // 对原数据进行异常值检查
			{
				if (data[i] != nullValue) {
					if (data[i] < lower || data[i] > upper) {
						data[i] = nullValue; // 元素不是空值，也不是正常值，判定为异常值，去掉。
					}
				}
			}
		} catch (RuntimeException e) {
			return false;
		}
		return true;
	}

	/**
	 * 缺数补全处理，靠近预报时间点空缺优先补充，空值所处区间两端的平均值
	 * 
	 * @param data 原数组
	 * @param n    数组长度
	 * @return
	 */
	private static boolean CompletionDeal(double[] data, int n) {
		double[] arr;
		double u = 0; // 均值

		try {
			// 先用已有数据计算均值
			List<Double> dataList = new ArrayList<>();
			for (double d : data) {
				if (d != nullValue) {// 排除空值组成新数组
					dataList.add(d);
				}
			}
			arr = new double[dataList.size()];
			for (int i = 0; i < dataList.size(); i++) {
				arr[i] = dataList.get(i);
			}

			double sum = 0;
			for (double d : arr) {
				sum += d;
			}
			u = arr.length == 0 ? 0 : sum / arr.length; // 计算均值

			// 如果数组第一个元素或最后一个元素缺数，补回均值
			if (data[0] == nullValue) {
				data[0] = u;
			}
			if (data[n - 1] == nullValue) {
				data[n - 1] = u;
			}

			// 对整个数组进行空缺数据回补，从后至前先补靠近预报时间点的数据，按缺数区间两端均值进行回补
			for (int i = n - 1; i >= 0; i--) {
				if (data[i] == nullValue) {
					double newValue = NullValueRegionAverage(data, i);
					data[i] = MathUtil.roundBank(newValue, 3).doubleValue();
				}
			}
		} catch (RuntimeException e) {
			return false;
		}
		return true;

	}

	/**
	 * 计算空值所处区间两端的平均值
	 * 
	 * @param data  原数组
	 * @param index 当前空值所处下标
	 * @return
	 */
	private static double NullValueRegionAverage(double[] data, int index) {
		double avg = 0;
		int startIndex = index;
		int endIndex = index;
		for (int i = index; i >= 0; i--) {
			if (data[i] != nullValue) {
				startIndex = i;
				break;
			}
		}
		for (int i = index, l = data.length; i < l; i++) {
			if (data[i] != nullValue) {
				endIndex = i;
				break;
			}
		}
		avg = (data[startIndex] + data[endIndex]) / 2.0;
		return avg;
	}
}