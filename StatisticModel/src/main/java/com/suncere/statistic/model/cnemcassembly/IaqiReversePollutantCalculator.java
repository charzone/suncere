package com.suncere.statistic.model.cnemcassembly;

import com.suncere.statistic.model.cnemcassembly.aircriteria.IAQI;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.IndividualAirBaseIndex;
import com.suncere.statistic.serivce.enums.EPollutants;

/**
 * 根据Iaqi 反推污染物浓度 计算器
 * 
 */
public class IaqiReversePollutantCalculator {
	/**
	 * 根据单个Iaqi 反推污染物浓度
	 * 
	 * @param iaqindex   iaqi值
	 * @param ePollutant 污染物类型
	 * @return
	 */
	public static double GetPollutantFromIaqi(int iaqindex, EPollutants ePollutant) {
		// 构造iaqi
		IAQI tempVar = new IAQI();

		IndividualAirBaseIndex baseIndex = new IndividualAirBaseIndex();
		baseIndex.setIndividualAirIndex(iaqindex);
		tempVar.index = baseIndex;
		tempVar.setEPollutants(ePollutant);
		IAQI iaqi = tempVar;
		double result = iaqi.GetCp();
		return result;
	}

	/**
	 * 根据Iaqi范围 反推污染物浓度范围
	 * 
	 * @param indexLow   iaqi值
	 * @param indexHigh   iaqi值
	 * @param ePollutant 污染物类型
	 * @return
	 */
	public static double[] GetPollutantRange(int indexLow, int indexHigh, EPollutants ePollutant) {
		// 构造iaqi
		IAQI tempVar = new IAQI();
		IndividualAirBaseIndex baseIndex = new IndividualAirBaseIndex();
		baseIndex.setIndividualAirIndex(indexLow);
		tempVar.index = baseIndex;
		tempVar.setEPollutants(ePollutant);
		IAQI iaqiLow = tempVar;
		IAQI tempVar2 = new IAQI();

		baseIndex = new IndividualAirBaseIndex();
		baseIndex.setIndividualAirIndex(indexHigh);
		tempVar2.index = baseIndex;
		tempVar2.setEPollutants(ePollutant);
		IAQI iaqiHigh = tempVar2;
		double[] result = new double[] { iaqiLow.GetCp(), iaqiHigh.GetCp() };
		return result;
	}

	/// #region 通过污染物名称获取污染物类型（日）
	/**
	 * 通过污染物名称获取污染物类型（日）
	 * 
	 * @param p
	 * @return
	 */
	public static EPollutants getEPollutantsNameDay(String p) {
		if (p.equals("二氧化硫")) {
			return EPollutants.SO2;
		} else if (p.equals("二氧化氮")) {
			return EPollutants.NO2;
		} else if (p.equals("O3")) {
			return EPollutants.O3_8H;
		} else if (p.equals("臭氧8小时")) {
			return EPollutants.O3_8H;
		} else if (p.equals("颗粒物(PM10)")) {
			return EPollutants.PM10;
		} else if (p.equals("细颗粒物(PM2.5)")) {
			return EPollutants.PM2_5;
		} else if (p.equals("一氧化碳")) {
			return EPollutants.CO;
		} else {
			return EPollutants.O3_8H;
		}
	}
}