package com.suncere.statistic.model.cnemcassembly.aircriteria.model;

public class IndividualAirBaseIndex {
	/**
	 * 分指数
	 */
	public int individualAirIndex;
	public String pollutantChineseName;
	public String level;
	public String chineseLevel;

	public int getIndividualAirIndex() {
		return individualAirIndex;
	}

	public void setIndividualAirIndex(int individualAirIndex) {
		this.individualAirIndex = individualAirIndex;
	}

	public String getPollutantChineseName() {
		return pollutantChineseName;
	}

	public void setPollutantChineseName(String pollutantChineseName) {
		this.pollutantChineseName = pollutantChineseName;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getChineseLevel() {
		return chineseLevel;
	}

	public void setChineseLevel(String chineseLevel) {
		this.chineseLevel = chineseLevel;
	}
}