package com.suncere.statistic.model.cnemcassembly.aircriteria;

import com.suncere.statistic.model.cnemcassembly.aircriteria.model.AirBaseIndex;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.AirIndex;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.DataSource;
import com.suncere.statistic.model.cnemcassembly.aircriteria.model.IndividualAirBaseIndex;
import com.suncere.statistic.serivce.enums.EvaluationCriteria;
import com.suncere.statistic.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 空气质量指数
 * 
 */
public class AirBaseIndexOpreate<T extends IndividualAirBaseIndex> {
	private ArrayList<T> iaqis;
	private EvaluationCriteria ec;

	public AirBaseIndexOpreate() {
	}

	public DataSource dataSource;

	public AirBaseIndexOpreate(ArrayList<T> iaqis, EvaluationCriteria ec) {
		this.iaqis = iaqis;
		this.ec = ec;
		dataSource = new DataSource();
	}

	public final AirBaseIndex GetAirIndex() {
		AirBaseIndex abi = new AirBaseIndex();
		if (iaqis.stream().filter(x -> x.getIndividualAirIndex() > -99).count() == 0) {
			abi.setAirIndex(-99);
			abi.setColor("—");
			abi.setLevel("—");
			abi.setChineseLevel("—");
			abi.setMeasure("—");
			abi.setUnheathful("—");
			abi.setType("—");
			abi.setPrimaryPollutant("—");
			abi.setNonAttainmentPollutant("—");
			return abi;
		}

		int aqi = iaqis.get(0).getIndividualAirIndex();
		for (T item : iaqis) {
			if (item.getIndividualAirIndex() >= aqi) {
				aqi = item.getIndividualAirIndex();
			}
		}

		// 获取对应指标信息记录
		try {
			final int finalAqi = aqi;
			AirIndex air = dataSource.getAirIndexList().stream()
					.filter(i -> i.getAiHi() >= finalAqi && i.getAiLo() <= finalAqi && i.getEc() == this.ec.getValue())
					.findFirst().orElseGet(null);

			abi.setAirIndex(aqi);
			abi.setAiHi(air.getAiHi());
			abi.setAiLo(air.getAiLo());
			abi.setColor(air.getColor());
			abi.setLevel(air.getLevel());
			abi.setChineseLevel(air.getChineseLevel());
			abi.setMeasure(air.getMeasure());
			abi.setUnheathful(air.getUnheathful());
			abi.setType(air.getType());
			abi.setPrimaryPollutant("—");
			abi.setNonAttainmentPollutant("—");
			if (aqi <= 50) {
				return abi;
			}
			// 空气质量指数大于50时，空气质量分指数最大的污染物为首要污染物。
			// 若空气质量分指数最大的污染物为两项或两项以上时，并列为首要污染物
			abi.setPrimaryPollutant("");
			abi.setNonAttainmentPollutant("");
			List<T> temp = iaqis.stream().filter(x -> x.getIndividualAirIndex() == finalAqi).collect(Collectors.toList());
			for (T item : temp) {
				abi.setPrimaryPollutant(abi.getPrimaryPollutant() + item.getPollutantChineseName() + ",");
				// 空气质量分指数大于100的污染物为超标污染物
				if (item.getIndividualAirIndex() > 100) {
					abi.setNonAttainmentPollutant(
							abi.getNonAttainmentPollutant() + item.getPollutantChineseName() + ",");
				}
			}
			if (StringUtils.isEmpty(abi.getPrimaryPollutant())) {
				abi.setPrimaryPollutant("—");
			} else {
				abi.setPrimaryPollutant(abi.getPrimaryPollutant().substring(0, abi.getPrimaryPollutant().length() - 1));
			}

			if (StringUtils.isEmpty(abi.getNonAttainmentPollutant())) {
				abi.setNonAttainmentPollutant("—");
			} else {
				abi.setNonAttainmentPollutant(
						abi.getNonAttainmentPollutant().substring(0, abi.getNonAttainmentPollutant().length() - 1));
			}
			return abi;
		} catch (Exception e) {
			throw new RuntimeException("无法找到与指定污染物项目分指数相对应的空气质量指数标准");
		}

	}
}