package com.suncere.statistic.model.cnemcassembly.aircriteria.model;

import java.util.ArrayList;

public class DataSource {
	/**
	 * 空气质量指标集合
	 * 
	 */
	private ArrayList<AirIndex> privateAirIndexList;

	public final ArrayList<AirIndex> getAirIndexList() {
		return privateAirIndexList;
	}

	private void setAirIndexList(ArrayList<AirIndex> value) {
		privateAirIndexList = value;
	}

	/**
	 * 污染物浓度限值集合
	 * 
	 */
	private ArrayList<PollutantConLimits> privatePollutantConLimitsList;

	public final ArrayList<PollutantConLimits> getPollutantConLimitsList() {
		return privatePollutantConLimitsList;
	}

	private void setPollutantConLimitsList(ArrayList<PollutantConLimits> value) {
		privatePollutantConLimitsList = value;
	}

	/**
	 * 污染物信息集合
	 * 
	 */
	private ArrayList<PollutantInfo> privatePollutantInfoList;

	public final ArrayList<PollutantInfo> getPollutantInfoList() {
		return privatePollutantInfoList;
	}

	private void setPollutantInfoList(ArrayList<PollutantInfo> value) {
		privatePollutantInfoList = value;
	}

	public DataSource() {
		InitializeData();
	}

	private void InitializeData() {
		setAirIndexList(new ArrayList<AirIndex>());
		setPollutantConLimitsList(new ArrayList<PollutantConLimits>());
		setPollutantInfoList(new ArrayList<PollutantInfo>());

		/// #region 空气质量指标
		AirIndex tempVar = new AirIndex();
		tempVar.setAiLo(0);
		tempVar.setAiHi(50);
		tempVar.setLevel("I");
		tempVar.setChineseLevel("一级");
		tempVar.setType("优");
		tempVar.setColor("Green");
		tempVar.setUnheathful("空气质量令人满意，基本无空气污染");
		tempVar.setMeasure("各类人群可正常活动");
		tempVar.setEc(1);
		AirIndex airIndex1 = tempVar;
		getAirIndexList().add(airIndex1);

		AirIndex tempVar2 = new AirIndex();
		tempVar2.setAiLo(51);
		tempVar2.setAiHi(100);
		tempVar2.setLevel("II");
		tempVar2.setChineseLevel("二级");
		tempVar2.setType("良");
		tempVar2.setColor("Yellow");
		tempVar2.setUnheathful("空气质量可接受，但某些污染物可能对极少数异常敏感人群健康有较弱影响");
		tempVar2.setMeasure("极少数异常敏感人群应减少户外活动");
		tempVar2.setEc(1);
		AirIndex airIndex2 = tempVar2;
		getAirIndexList().add(airIndex2);

		AirIndex tempVar3 = new AirIndex();
		tempVar3.setAiLo(101);
		tempVar3.setAiHi(150);
		tempVar3.setLevel("III");
		tempVar3.setChineseLevel("三级");
		tempVar3.setType("轻度污染");
		tempVar3.setColor("Orange");
		tempVar3.setUnheathful("易感人群症状有轻度加剧，健康人群出现刺激症状");
		tempVar3.setMeasure("儿童、老年人及心脏病、呼吸系统疾病患者应减少长时间、高强度的户外锻炼");
		tempVar3.setEc(1);
		AirIndex airIndex3 = tempVar3;
		getAirIndexList().add(airIndex3);

		AirIndex tempVar4 = new AirIndex();
		tempVar4.setAiLo(151);
		tempVar4.setAiHi(200);
		tempVar4.setLevel("IV");
		tempVar4.setChineseLevel("四级");
		tempVar4.setType("中度污染");
		tempVar4.setColor("Red");
		tempVar4.setUnheathful("进一步加剧易感人群症状，可能对健康人群心脏、呼吸系统有影响");
		tempVar4.setMeasure("儿童、老年人及心脏病、呼吸系统疾病患者应减少长时间、高强度的户外锻炼，一般人群适量减少户外活动");
		tempVar4.setEc(1);
		AirIndex airIndex4 = tempVar4;
		getAirIndexList().add(airIndex4);

		AirIndex tempVar5 = new AirIndex();
		tempVar5.setAiLo(201);
		tempVar5.setAiHi(300);
		tempVar5.setLevel("V");
		tempVar5.setChineseLevel("五级");
		tempVar5.setType("重度污染");
		tempVar5.setColor("Purple");
		tempVar5.setUnheathful("心脏病和肺病患者症状显著加剧，运动耐力降低，健康人群普遍出现症状");
		tempVar5.setMeasure("老年人及心脏病、肺病患者应停留在室内，停止户外活动，一般人群减少户外活动");
		tempVar5.setEc(1);
		AirIndex airIndex5 = tempVar5;
		getAirIndexList().add(airIndex5);

		AirIndex tempVar6 = new AirIndex();
		tempVar6.setAiLo(301);
		tempVar6.setAiHi(1000);
		tempVar6.setLevel("VI");
		tempVar6.setChineseLevel("六级");
		tempVar6.setType("严重污染");
		tempVar6.setColor("Maroon");
		tempVar6.setUnheathful("健康人运动耐受力降低，有明显强烈症状，提前出现某些疾病");
		tempVar6.setMeasure("老年人和病人应当留在室内，避免体力消耗，一般人群应避免户外活动");
		tempVar6.setEc(1);
		AirIndex airIndex6 = tempVar6;
		getAirIndexList().add(airIndex6);

		AirIndex tempVar7 = new AirIndex();
		tempVar7.setAiLo(0);
		tempVar7.setAiHi(50);
		tempVar7.setLevel("I");
		tempVar7.setChineseLevel("一级");
		tempVar7.setType("优");
		tempVar7.setColor("Green");
		tempVar7.setUnheathful("可正常活动");
		tempVar7.setMeasure("");
		tempVar7.setEc(2);
		AirIndex airIndex7 = tempVar7;
		getAirIndexList().add(airIndex7);

		AirIndex tempVar8 = new AirIndex();
		tempVar8.setAiLo(51);
		tempVar8.setAiHi(100);
		tempVar8.setLevel("II");
		tempVar8.setChineseLevel("二级");
		tempVar8.setType("良");
		tempVar8.setColor("Yellow");
		tempVar8.setUnheathful("可正常活动");
		tempVar8.setMeasure("");
		tempVar8.setEc(2);
		AirIndex airIndex8 = tempVar8;
		getAirIndexList().add(airIndex8);

		AirIndex tempVar9 = new AirIndex();
		tempVar9.setAiLo(101);
		tempVar9.setAiHi(150);
		tempVar9.setLevel("III");
		tempVar9.setChineseLevel("三级");
		tempVar9.setType("轻微污染");
		tempVar9.setColor("Orange");
		tempVar9.setUnheathful("易感人群症状有轻度加剧，健康人群出现刺激症状");
		tempVar9.setMeasure("心脏病和呼吸系统疾病患者应减少体力消耗和户外活动");
		tempVar9.setEc(2);
		AirIndex airIndex9 = tempVar9;
		getAirIndexList().add(airIndex9);

		AirIndex tempVar10 = new AirIndex();
		tempVar10.setAiLo(151);
		tempVar10.setAiHi(200);
		tempVar10.setLevel("IV");
		tempVar10.setChineseLevel("四级");
		tempVar10.setType("轻度污染");
		tempVar10.setColor("Red");
		tempVar10.setUnheathful("易感人群症状有轻度加剧，健康人群出现刺激症状");
		tempVar10.setMeasure("心脏病和呼吸系统疾病患者应减少体力消耗和户外活动");
		tempVar10.setEc(2);
		AirIndex airIndex10 = tempVar10;
		getAirIndexList().add(airIndex10);

		AirIndex tempVar11 = new AirIndex();
		tempVar11.setAiLo(201);
		tempVar11.setAiHi(250);
		tempVar11.setLevel("V");
		tempVar11.setChineseLevel("五级");
		tempVar11.setType("中度污染");
		tempVar11.setColor("Purple");
		tempVar11.setUnheathful("心脏病和肺病患者症状显著加剧，运动耐受力降低，健康人群中普遍出现症状");
		tempVar11.setMeasure("老年人和心脏病、肺病患者应在停留在室内，并减少体力活动");
		tempVar11.setEc(2);
		AirIndex airIndex11 = tempVar11;
		getAirIndexList().add(airIndex11);

		AirIndex tempVar12 = new AirIndex();
		tempVar12.setAiLo(251);
		tempVar12.setAiHi(300);
		tempVar12.setLevel("VI");
		tempVar12.setChineseLevel("六级");
		tempVar12.setType("中度重污染");
		tempVar12.setColor("Maroon");
		tempVar12.setUnheathful("心脏病和肺病患者症状显著加剧，运动耐受力降低，健康人群中普遍出现症状");
		tempVar12.setMeasure("老年人和心脏病、肺病患者应在停留在室内，并减少体力活动");
		tempVar12.setEc(2);
		AirIndex airIndex12 = tempVar12;
		getAirIndexList().add(airIndex12);

		AirIndex tempVar13 = new AirIndex();
		tempVar13.setAiLo(301);
		tempVar13.setAiHi(1000);
		tempVar13.setLevel("VII");
		tempVar13.setChineseLevel("七级");
		tempVar13.setType("重污染");
		tempVar13.setColor("Maroon ");
		tempVar13.setUnheathful("健康人运动耐受力降低，有明显强烈症状，提前出现某些疾病");
		tempVar13.setMeasure("老年人和病人应当留在室内，避免体力消耗，一般人群应避免户外活动");
		tempVar13.setEc(2);
		AirIndex airIndex13 = tempVar13;
		getAirIndexList().add(airIndex13);

		/// #region 污染物浓度限值
		PollutantConLimits tempVar14 = new PollutantConLimits();
		tempVar14.setPollutantCode("SO2");
		tempVar14.setBpLo(0);
		tempVar14.setBpHi(50);
		tempVar14.setIaiLo(0);
		tempVar14.setIaiHi(50);
		tempVar14.setEc(1);
		PollutantConLimits pollutantConLimits1 = tempVar14;
		getPollutantConLimitsList().add(pollutantConLimits1);

		PollutantConLimits tempVar15 = new PollutantConLimits();
		tempVar15.setPollutantCode("SO2");
		tempVar15.setBpLo(50);
		tempVar15.setBpHi(150);
		tempVar15.setIaiLo(50);
		tempVar15.setIaiHi(100);
		tempVar15.setEc(1);
		PollutantConLimits pollutantConLimits2 = tempVar15;
		getPollutantConLimitsList().add(pollutantConLimits2);

		PollutantConLimits tempVar16 = new PollutantConLimits();
		tempVar16.setPollutantCode("SO2");
		tempVar16.setBpLo(150);
		tempVar16.setBpHi(475);
		tempVar16.setIaiLo(100);
		tempVar16.setIaiHi(150);
		tempVar16.setEc(1);
		PollutantConLimits pollutantConLimits3 = tempVar16;
		getPollutantConLimitsList().add(pollutantConLimits3);

		PollutantConLimits tempVar17 = new PollutantConLimits();
		tempVar17.setPollutantCode("SO2");
		tempVar17.setBpLo(475);
		tempVar17.setBpHi(800);
		tempVar17.setIaiLo(150);
		tempVar17.setIaiHi(200);
		tempVar17.setEc(1);
		PollutantConLimits pollutantConLimits4 = tempVar17;
		getPollutantConLimitsList().add(pollutantConLimits4);

		PollutantConLimits tempVar18 = new PollutantConLimits();
		tempVar18.setPollutantCode("SO2");
		tempVar18.setBpLo(800);
		tempVar18.setBpHi(1600);
		tempVar18.setIaiLo(200);
		tempVar18.setIaiHi(300);
		tempVar18.setEc(1);
		PollutantConLimits pollutantConLimits5 = tempVar18;
		getPollutantConLimitsList().add(pollutantConLimits5);

		PollutantConLimits tempVar19 = new PollutantConLimits();
		tempVar19.setPollutantCode("SO2");
		tempVar19.setBpLo(1600);
		tempVar19.setBpHi(2100);
		tempVar19.setIaiLo(300);
		tempVar19.setIaiHi(400);
		tempVar19.setEc(1);
		PollutantConLimits pollutantConLimits6 = tempVar19;
		getPollutantConLimitsList().add(pollutantConLimits6);

		PollutantConLimits tempVar20 = new PollutantConLimits();
		tempVar20.setPollutantCode("SO2");
		tempVar20.setBpLo(2100);
		tempVar20.setBpHi(2620);
		tempVar20.setIaiLo(400);
		tempVar20.setIaiHi(500);
		tempVar20.setEc(1);
		PollutantConLimits pollutantConLimits7 = tempVar20;
		getPollutantConLimitsList().add(pollutantConLimits7);
		
		/// #region AQI SO2-1h
		PollutantConLimits tempVar21 = new PollutantConLimits();
		tempVar21.setPollutantCode("SO2-1h");
		tempVar21.setBpLo(0);
		tempVar21.setBpHi(150);
		tempVar21.setIaiLo(0);
		tempVar21.setIaiHi(50);
		tempVar21.setEc(1);
		PollutantConLimits pollutantConLimits73 = tempVar21;
		getPollutantConLimitsList().add(pollutantConLimits73);

		PollutantConLimits tempVar22 = new PollutantConLimits();
		tempVar22.setPollutantCode("SO2-1h");
		tempVar22.setBpLo(150);
		tempVar22.setBpHi(500);
		tempVar22.setIaiLo(50);
		tempVar22.setIaiHi(100);
		tempVar22.setEc(1);
		PollutantConLimits pollutantConLimits74 = tempVar22;
		getPollutantConLimitsList().add(pollutantConLimits74);

		PollutantConLimits tempVar23 = new PollutantConLimits();
		tempVar23.setPollutantCode("SO2-1h");
		tempVar23.setBpLo(500);
		tempVar23.setBpHi(650);
		tempVar23.setIaiLo(100);
		tempVar23.setIaiHi(150);
		tempVar23.setEc(1);
		PollutantConLimits pollutantConLimits75 = tempVar23;
		getPollutantConLimitsList().add(pollutantConLimits75);

		PollutantConLimits tempVar24 = new PollutantConLimits();
		tempVar24.setPollutantCode("SO2-1h");
		tempVar24.setBpLo(650);
		tempVar24.setBpHi(800);
		tempVar24.setIaiLo(150);
		tempVar24.setIaiHi(200);
		tempVar24.setEc(1);
		PollutantConLimits pollutantConLimits76 = tempVar24;
		getPollutantConLimitsList().add(pollutantConLimits76);

		/// #region AQI NO2-24h
		PollutantConLimits tempVar25 = new PollutantConLimits();
		tempVar25.setPollutantCode("NO2");
		tempVar25.setBpLo(0);
		tempVar25.setBpHi(40);
		tempVar25.setIaiLo(0);
		tempVar25.setIaiHi(50);
		tempVar25.setEc(1);
		PollutantConLimits pollutantConLimits8 = tempVar25;
		getPollutantConLimitsList().add(pollutantConLimits8);

		PollutantConLimits tempVar26 = new PollutantConLimits();
		tempVar26.setPollutantCode("NO2");
		tempVar26.setBpLo(40);
		tempVar26.setBpHi(80);
		tempVar26.setIaiLo(50);
		tempVar26.setIaiHi(100);
		tempVar26.setEc(1);
		PollutantConLimits pollutantConLimits9 = tempVar26;
		getPollutantConLimitsList().add(pollutantConLimits9);

		PollutantConLimits tempVar27 = new PollutantConLimits();
		tempVar27.setPollutantCode("NO2");
		tempVar27.setBpLo(80);
		tempVar27.setBpHi(180);
		tempVar27.setIaiLo(100);
		tempVar27.setIaiHi(150);
		tempVar27.setEc(1);
		PollutantConLimits pollutantConLimits10 = tempVar27;
		getPollutantConLimitsList().add(pollutantConLimits10);

		PollutantConLimits tempVar28 = new PollutantConLimits();
		tempVar28.setPollutantCode("NO2");
		tempVar28.setBpLo(180);
		tempVar28.setBpHi(280);
		tempVar28.setIaiLo(150);
		tempVar28.setIaiHi(200);
		tempVar28.setEc(1);
		PollutantConLimits pollutantConLimits11 = tempVar28;
		getPollutantConLimitsList().add(pollutantConLimits11);

		PollutantConLimits tempVar29 = new PollutantConLimits();
		tempVar29.setPollutantCode("NO2");
		tempVar29.setBpLo(280);
		tempVar29.setBpHi(565);
		tempVar29.setIaiLo(200);
		tempVar29.setIaiHi(300);
		tempVar29.setEc(1);
		PollutantConLimits pollutantConLimits12 = tempVar29;
		getPollutantConLimitsList().add(pollutantConLimits12);

		PollutantConLimits tempVar30 = new PollutantConLimits();
		tempVar30.setPollutantCode("NO2");
		tempVar30.setBpLo(565);
		tempVar30.setBpHi(750);
		tempVar30.setIaiLo(300);
		tempVar30.setIaiHi(400);
		tempVar30.setEc(1);
		PollutantConLimits pollutantConLimits13 = tempVar30;
		getPollutantConLimitsList().add(pollutantConLimits13);

		PollutantConLimits tempVar31 = new PollutantConLimits();
		tempVar31.setPollutantCode("NO2");
		tempVar31.setBpLo(750);
		tempVar31.setBpHi(940);
		tempVar31.setIaiLo(400);
		tempVar31.setIaiHi(500);
		tempVar31.setEc(1);
		PollutantConLimits pollutantConLimits14 = tempVar31;
		getPollutantConLimitsList().add(pollutantConLimits14);

		/// #region AQI NO2-1h
		PollutantConLimits tempVar32 = new PollutantConLimits();
		tempVar32.setPollutantCode("NO2-1h");
		tempVar32.setBpLo(0);
		tempVar32.setBpHi(100);
		tempVar32.setIaiLo(0);
		tempVar32.setIaiHi(50);
		tempVar32.setEc(1);
		PollutantConLimits pollutantConLimits77 = tempVar32;
		getPollutantConLimitsList().add(pollutantConLimits77);

		PollutantConLimits tempVar33 = new PollutantConLimits();
		tempVar33.setPollutantCode("NO2-1h");
		tempVar33.setBpLo(100);
		tempVar33.setBpHi(200);
		tempVar33.setIaiLo(50);
		tempVar33.setIaiHi(100);
		tempVar33.setEc(1);
		PollutantConLimits pollutantConLimits78 = tempVar33;
		getPollutantConLimitsList().add(pollutantConLimits78);

		PollutantConLimits tempVar34 = new PollutantConLimits();
		tempVar34.setPollutantCode("NO2-1h");
		tempVar34.setBpLo(200);
		tempVar34.setBpHi(700);
		tempVar34.setIaiLo(100);
		tempVar34.setIaiHi(150);
		tempVar34.setEc(1);
		PollutantConLimits pollutantConLimits79 = tempVar34;
		getPollutantConLimitsList().add(pollutantConLimits79);

		PollutantConLimits tempVar35 = new PollutantConLimits();
		tempVar35.setPollutantCode("NO2-1h");
		tempVar35.setBpLo(700);
		tempVar35.setBpHi(1200);
		tempVar35.setIaiLo(150);
		tempVar35.setIaiHi(200);
		tempVar35.setEc(1);
		PollutantConLimits pollutantConLimits80 = tempVar35;
		getPollutantConLimitsList().add(pollutantConLimits80);

		PollutantConLimits tempVar36 = new PollutantConLimits();
		tempVar36.setPollutantCode("NO2-1h");
		tempVar36.setBpLo(1200);
		tempVar36.setBpHi(2340);
		tempVar36.setIaiLo(200);
		tempVar36.setIaiHi(300);
		tempVar36.setEc(1);
		PollutantConLimits pollutantConLimits81 = tempVar36;
		getPollutantConLimitsList().add(pollutantConLimits81);

		PollutantConLimits tempVar37 = new PollutantConLimits();
		tempVar37.setPollutantCode("NO2-1h");
		tempVar37.setBpLo(2340);
		tempVar37.setBpHi(3090);
		tempVar37.setIaiLo(300);
		tempVar37.setIaiHi(400);
		tempVar37.setEc(1);
		PollutantConLimits pollutantConLimits82 = tempVar37;
		getPollutantConLimitsList().add(pollutantConLimits82);

		PollutantConLimits tempVar38 = new PollutantConLimits();
		tempVar38.setPollutantCode("NO2-1h");
		tempVar38.setBpLo(3090);
		tempVar38.setBpHi(3840);
		tempVar38.setIaiLo(400);
		tempVar38.setIaiHi(500);
		tempVar38.setEc(1);
		PollutantConLimits pollutantConLimits83 = tempVar38;
		getPollutantConLimitsList().add(pollutantConLimits83);

		/// #region AQI CO-24h
		PollutantConLimits tempVar39 = new PollutantConLimits();
		tempVar39.setPollutantCode("CO");
		tempVar39.setBpLo(0);
		tempVar39.setBpHi(2);
		tempVar39.setIaiLo(0);
		tempVar39.setIaiHi(50);
		tempVar39.setEc(1);
		PollutantConLimits pollutantConLimits22 = tempVar39;
		getPollutantConLimitsList().add(pollutantConLimits22);

		PollutantConLimits tempVar40 = new PollutantConLimits();
		tempVar40.setPollutantCode("CO");
		tempVar40.setBpLo(2);
		tempVar40.setBpHi(4);
		tempVar40.setIaiLo(50);
		tempVar40.setIaiHi(100);
		tempVar40.setEc(1);
		PollutantConLimits pollutantConLimits23 = tempVar40;
		getPollutantConLimitsList().add(pollutantConLimits23);

		PollutantConLimits tempVar41 = new PollutantConLimits();
		tempVar41.setPollutantCode("CO");
		tempVar41.setBpLo(4);
		tempVar41.setBpHi(14);
		tempVar41.setIaiLo(100);
		tempVar41.setIaiHi(150);
		tempVar41.setEc(1);
		PollutantConLimits pollutantConLimits24 = tempVar41;
		getPollutantConLimitsList().add(pollutantConLimits24);

		PollutantConLimits tempVar42 = new PollutantConLimits();
		tempVar42.setPollutantCode("CO");
		tempVar42.setBpLo(14);
		tempVar42.setBpHi(24);
		tempVar42.setIaiLo(150);
		tempVar42.setIaiHi(200);
		tempVar42.setEc(1);
		PollutantConLimits pollutantConLimits25 = tempVar42;
		getPollutantConLimitsList().add(pollutantConLimits25);

		PollutantConLimits tempVar43 = new PollutantConLimits();
		tempVar43.setPollutantCode("CO");
		tempVar43.setBpLo(24);
		tempVar43.setBpHi(36);
		tempVar43.setIaiLo(200);
		tempVar43.setIaiHi(300);
		tempVar43.setEc(1);
		PollutantConLimits pollutantConLimits26 = tempVar43;
		getPollutantConLimitsList().add(pollutantConLimits26);

		PollutantConLimits tempVar44 = new PollutantConLimits();
		tempVar44.setPollutantCode("CO");
		tempVar44.setBpLo(36);
		tempVar44.setBpHi(48);
		tempVar44.setIaiLo(300);
		tempVar44.setIaiHi(400);
		tempVar44.setEc(1);
		PollutantConLimits pollutantConLimits27 = tempVar44;
		getPollutantConLimitsList().add(pollutantConLimits27);

		PollutantConLimits tempVar45 = new PollutantConLimits();
		tempVar45.setPollutantCode("CO");
		tempVar45.setBpLo(48);
		tempVar45.setBpHi(60);
		tempVar45.setIaiLo(400);
		tempVar45.setIaiHi(500);
		tempVar45.setEc(1);
		PollutantConLimits pollutantConLimits28 = tempVar45;
		getPollutantConLimitsList().add(pollutantConLimits28);

		/// #region AQI CO-1h
		PollutantConLimits tempVar46 = new PollutantConLimits();
		tempVar46.setPollutantCode("CO-1h");
		tempVar46.setBpLo(0);
		tempVar46.setBpHi(5);
		tempVar46.setIaiLo(0);
		tempVar46.setIaiHi(50);
		tempVar46.setEc(1);
		PollutantConLimits pollutantConLimits84 = tempVar46;
		getPollutantConLimitsList().add(pollutantConLimits84);

		PollutantConLimits tempVar47 = new PollutantConLimits();
		tempVar47.setPollutantCode("CO-1h");
		tempVar47.setBpLo(5);
		tempVar47.setBpHi(10);
		tempVar47.setIaiLo(50);
		tempVar47.setIaiHi(100);
		tempVar47.setEc(1);
		PollutantConLimits pollutantConLimits85 = tempVar47;
		getPollutantConLimitsList().add(pollutantConLimits85);

		PollutantConLimits tempVar48 = new PollutantConLimits();
		tempVar48.setPollutantCode("CO-1h");
		tempVar48.setBpLo(10);
		tempVar48.setBpHi(35);
		tempVar48.setIaiLo(100);
		tempVar48.setIaiHi(150);
		tempVar48.setEc(1);
		PollutantConLimits pollutantConLimits86 = tempVar48;
		getPollutantConLimitsList().add(pollutantConLimits86);

		PollutantConLimits tempVar49 = new PollutantConLimits();
		tempVar49.setPollutantCode("CO-1h");
		tempVar49.setBpLo(35);
		tempVar49.setBpHi(60);
		tempVar49.setIaiLo(150);
		tempVar49.setIaiHi(200);
		tempVar49.setEc(1);
		PollutantConLimits pollutantConLimits87 = tempVar49;
		getPollutantConLimitsList().add(pollutantConLimits87);

		PollutantConLimits tempVar50 = new PollutantConLimits();
		tempVar50.setPollutantCode("CO-1h");
		tempVar50.setBpLo(60);
		tempVar50.setBpHi(90);
		tempVar50.setIaiLo(200);
		tempVar50.setIaiHi(300);
		tempVar50.setEc(1);
		PollutantConLimits pollutantConLimits88 = tempVar50;
		getPollutantConLimitsList().add(pollutantConLimits88);

		PollutantConLimits tempVar51 = new PollutantConLimits();
		tempVar51.setPollutantCode("CO-1h");
		tempVar51.setBpLo(90);
		tempVar51.setBpHi(120);
		tempVar51.setIaiLo(300);
		tempVar51.setIaiHi(400);
		tempVar51.setEc(1);
		PollutantConLimits pollutantConLimits89 = tempVar51;
		getPollutantConLimitsList().add(pollutantConLimits89);

		PollutantConLimits tempVar52 = new PollutantConLimits();
		tempVar52.setPollutantCode("CO-1h");
		tempVar52.setBpLo(120);
		tempVar52.setBpHi(150);
		tempVar52.setIaiLo(400);
		tempVar52.setIaiHi(500);
		tempVar52.setEc(1);
		PollutantConLimits pollutantConLimits90 = tempVar52;
		getPollutantConLimitsList().add(pollutantConLimits90);

		/// #region AQI O3-1h
		PollutantConLimits tempVar53 = new PollutantConLimits();
		tempVar53.setPollutantCode("O3-1h");
		tempVar53.setBpLo(0);
		tempVar53.setBpHi(160);
		tempVar53.setIaiLo(0);
		tempVar53.setIaiHi(50);
		tempVar53.setEc(1);
		PollutantConLimits pollutantConLimits29 = tempVar53;
		getPollutantConLimitsList().add(pollutantConLimits29);

		PollutantConLimits tempVar54 = new PollutantConLimits();
		tempVar54.setPollutantCode("O3-1h");
		tempVar54.setBpLo(160);
		tempVar54.setBpHi(200);
		tempVar54.setIaiLo(50);
		tempVar54.setIaiHi(100);
		tempVar54.setEc(1);
		PollutantConLimits pollutantConLimits30 = tempVar54;
		getPollutantConLimitsList().add(pollutantConLimits30);

		PollutantConLimits tempVar55 = new PollutantConLimits();
		tempVar55.setPollutantCode("O3-1h");
		tempVar55.setBpLo(200);
		tempVar55.setBpHi(300);
		tempVar55.setIaiLo(100);
		tempVar55.setIaiHi(150);
		tempVar55.setEc(1);
		PollutantConLimits pollutantConLimits31 = tempVar55;
		getPollutantConLimitsList().add(pollutantConLimits31);

		PollutantConLimits tempVar56 = new PollutantConLimits();
		tempVar56.setPollutantCode("O3-1h");
		tempVar56.setBpLo(300);
		tempVar56.setBpHi(400);
		tempVar56.setIaiLo(150);
		tempVar56.setIaiHi(200);
		tempVar56.setEc(1);
		PollutantConLimits pollutantConLimits32 = tempVar56;
		getPollutantConLimitsList().add(pollutantConLimits32);

		PollutantConLimits tempVar57 = new PollutantConLimits();
		tempVar57.setPollutantCode("O3-1h");
		tempVar57.setBpLo(400);
		tempVar57.setBpHi(800);
		tempVar57.setIaiLo(200);
		tempVar57.setIaiHi(300);
		tempVar57.setEc(1);
		PollutantConLimits pollutantConLimits33 = tempVar57;
		getPollutantConLimitsList().add(pollutantConLimits33);

		PollutantConLimits tempVar58 = new PollutantConLimits();
		tempVar58.setPollutantCode("O3-1h");
		tempVar58.setBpLo(800);
		tempVar58.setBpHi(1000);
		tempVar58.setIaiLo(300);
		tempVar58.setIaiHi(400);
		tempVar58.setEc(1);
		PollutantConLimits pollutantConLimits34 = tempVar58;
		getPollutantConLimitsList().add(pollutantConLimits34);

		PollutantConLimits tempVar59 = new PollutantConLimits();
		tempVar59.setPollutantCode("O3-1h");
		tempVar59.setBpLo(1000);
		tempVar59.setBpHi(1200);
		tempVar59.setIaiLo(400);
		tempVar59.setIaiHi(500);
		tempVar59.setEc(1);
		PollutantConLimits pollutantConLimits35 = tempVar59;
		getPollutantConLimitsList().add(pollutantConLimits35);

		/// #region AQI O3-8h
		PollutantConLimits tempVar60 = new PollutantConLimits();
		tempVar60.setPollutantCode("O3-8h");
		tempVar60.setBpLo(0);
		tempVar60.setBpHi(100);
		tempVar60.setIaiLo(0);
		tempVar60.setIaiHi(50);
		tempVar60.setEc(1);
		PollutantConLimits pollutantConLimits36 = tempVar60;
		getPollutantConLimitsList().add(pollutantConLimits36);

		PollutantConLimits tempVar61 = new PollutantConLimits();
		tempVar61.setPollutantCode("O3-8h");
		tempVar61.setBpLo(100);
		tempVar61.setBpHi(160);
		tempVar61.setIaiLo(50);
		tempVar61.setIaiHi(100);
		tempVar61.setEc(1);
		PollutantConLimits pollutantConLimits37 = tempVar61;
		getPollutantConLimitsList().add(pollutantConLimits37);

		PollutantConLimits tempVar62 = new PollutantConLimits();
		tempVar62.setPollutantCode("O3-8h");
		tempVar62.setBpLo(160);
		tempVar62.setBpHi(215);
		tempVar62.setIaiLo(100);
		tempVar62.setIaiHi(150);
		tempVar62.setEc(1);
		PollutantConLimits pollutantConLimits38 = tempVar62;
		getPollutantConLimitsList().add(pollutantConLimits38);

		PollutantConLimits tempVar63 = new PollutantConLimits();
		tempVar63.setPollutantCode("O3-8h");
		tempVar63.setBpLo(215);
		tempVar63.setBpHi(265);
		tempVar63.setIaiLo(150);
		tempVar63.setIaiHi(200);
		tempVar63.setEc(1);
		PollutantConLimits pollutantConLimits39 = tempVar63;
		getPollutantConLimitsList().add(pollutantConLimits39);

		PollutantConLimits tempVar64 = new PollutantConLimits();
		tempVar64.setPollutantCode("O3-8h");
		tempVar64.setBpLo(265);
		tempVar64.setBpHi(800);
		tempVar64.setIaiLo(200);
		tempVar64.setIaiHi(300);
		tempVar64.setEc(1);
		PollutantConLimits pollutantConLimits40 = tempVar64;
		getPollutantConLimitsList().add(pollutantConLimits40);

		/// #region AQI PM10
		PollutantConLimits tempVar65 = new PollutantConLimits();
		tempVar65.setPollutantCode("PM10");
		tempVar65.setBpLo(0);
		tempVar65.setBpHi(50);
		tempVar65.setIaiLo(0);
		tempVar65.setIaiHi(50);
		tempVar65.setEc(1);
		PollutantConLimits pollutantConLimits15 = tempVar65;
		getPollutantConLimitsList().add(pollutantConLimits15);

		PollutantConLimits tempVar66 = new PollutantConLimits();
		tempVar66.setPollutantCode("PM10");
		tempVar66.setBpLo(50);
		tempVar66.setBpHi(150);
		tempVar66.setIaiLo(50);
		tempVar66.setIaiHi(100);
		tempVar66.setEc(1);
		PollutantConLimits pollutantConLimits16 = tempVar66;
		getPollutantConLimitsList().add(pollutantConLimits16);

		PollutantConLimits tempVar67 = new PollutantConLimits();
		tempVar67.setPollutantCode("PM10");
		tempVar67.setBpLo(150);
		tempVar67.setBpHi(250);
		tempVar67.setIaiLo(100);
		tempVar67.setIaiHi(150);
		tempVar67.setEc(1);
		PollutantConLimits pollutantConLimits17 = tempVar67;
		getPollutantConLimitsList().add(pollutantConLimits17);

		PollutantConLimits tempVar68 = new PollutantConLimits();
		tempVar68.setPollutantCode("PM10");
		tempVar68.setBpLo(250);
		tempVar68.setBpHi(350);
		tempVar68.setIaiLo(150);
		tempVar68.setIaiHi(200);
		tempVar68.setEc(1);
		PollutantConLimits pollutantConLimits18 = tempVar68;
		getPollutantConLimitsList().add(pollutantConLimits18);

		PollutantConLimits tempVar69 = new PollutantConLimits();
		tempVar69.setPollutantCode("PM10");
		tempVar69.setBpLo(350);
		tempVar69.setBpHi(420);
		tempVar69.setIaiLo(200);
		tempVar69.setIaiHi(300);
		tempVar69.setEc(1);
		PollutantConLimits pollutantConLimits19 = tempVar69;
		getPollutantConLimitsList().add(pollutantConLimits19);

		PollutantConLimits tempVar70 = new PollutantConLimits();
		tempVar70.setPollutantCode("PM10");
		tempVar70.setBpLo(420);
		tempVar70.setBpHi(500);
		tempVar70.setIaiLo(300);
		tempVar70.setIaiHi(400);
		tempVar70.setEc(1);
		PollutantConLimits pollutantConLimits20 = tempVar70;
		getPollutantConLimitsList().add(pollutantConLimits20);

		PollutantConLimits tempVar71 = new PollutantConLimits();
		tempVar71.setPollutantCode("PM10");
		tempVar71.setBpLo(500);
		tempVar71.setBpHi(600);
		tempVar71.setIaiLo(400);
		tempVar71.setIaiHi(500);
		tempVar71.setEc(1);
		PollutantConLimits pollutantConLimits21 = tempVar71;
		getPollutantConLimitsList().add(pollutantConLimits21);

		/// #region AQI PM2.5
		PollutantConLimits tempVar72 = new PollutantConLimits();
		tempVar72.setPollutantCode("PM2.5");
		tempVar72.setBpLo(0);
		tempVar72.setBpHi(35);
		tempVar72.setIaiLo(0);
		tempVar72.setIaiHi(50);
		tempVar72.setEc(1);
		PollutantConLimits pollutantConLimits41 = tempVar72;
		getPollutantConLimitsList().add(pollutantConLimits41);

		PollutantConLimits tempVar73 = new PollutantConLimits();
		tempVar73.setPollutantCode("PM2.5");
		tempVar73.setBpLo(35);
		tempVar73.setBpHi(75);
		tempVar73.setIaiLo(50);
		tempVar73.setIaiHi(100);
		tempVar73.setEc(1);
		PollutantConLimits pollutantConLimits42 = tempVar73;
		getPollutantConLimitsList().add(pollutantConLimits42);

		PollutantConLimits tempVar74 = new PollutantConLimits();
		tempVar74.setPollutantCode("PM2.5");
		tempVar74.setBpLo(75);
		tempVar74.setBpHi(115);
		tempVar74.setIaiLo(100);
		tempVar74.setIaiHi(150);
		tempVar74.setEc(1);
		PollutantConLimits pollutantConLimits43 = tempVar74;
		getPollutantConLimitsList().add(pollutantConLimits43);

		PollutantConLimits tempVar75 = new PollutantConLimits();
		tempVar75.setPollutantCode("PM2.5");
		tempVar75.setBpLo(115);
		tempVar75.setBpHi(150);
		tempVar75.setIaiLo(150);
		tempVar75.setIaiHi(200);
		tempVar75.setEc(1);
		PollutantConLimits pollutantConLimits44 = tempVar75;
		getPollutantConLimitsList().add(pollutantConLimits44);

		PollutantConLimits tempVar76 = new PollutantConLimits();
		tempVar76.setPollutantCode("PM2.5");
		tempVar76.setBpLo(150);
		tempVar76.setBpHi(250);
		tempVar76.setIaiLo(200);
		tempVar76.setIaiHi(300);
		tempVar76.setEc(1);
		PollutantConLimits pollutantConLimits45 = tempVar76;
		getPollutantConLimitsList().add(pollutantConLimits45);

		PollutantConLimits tempVar77 = new PollutantConLimits();
		tempVar77.setPollutantCode("PM2.5");
		tempVar77.setBpLo(250);
		tempVar77.setBpHi(350);
		tempVar77.setIaiLo(300);
		tempVar77.setIaiHi(400);
		tempVar77.setEc(1);
		PollutantConLimits pollutantConLimits46 = tempVar77;
		getPollutantConLimitsList().add(pollutantConLimits46);

		PollutantConLimits tempVar78 = new PollutantConLimits();
		tempVar78.setPollutantCode("PM2.5");
		tempVar78.setBpLo(350);
		tempVar78.setBpHi(500);
		tempVar78.setIaiLo(400);
		tempVar78.setIaiHi(500);
		tempVar78.setEc(1);
		PollutantConLimits pollutantConLimits47 = tempVar78;
		getPollutantConLimitsList().add(pollutantConLimits47);

		/// #region API SO2
		PollutantConLimits tempVar79 = new PollutantConLimits();
		tempVar79.setPollutantCode("SO2");
		tempVar79.setBpLo(0);
		tempVar79.setBpHi(0.05);
		tempVar79.setIaiLo(0);
		tempVar79.setIaiHi(50);
		tempVar79.setEc(2);
		PollutantConLimits pollutantConLimits48 = tempVar79;
		getPollutantConLimitsList().add(pollutantConLimits48);

		PollutantConLimits tempVar80 = new PollutantConLimits();
		tempVar80.setPollutantCode("SO2");
		tempVar80.setBpLo(0.05);
		tempVar80.setBpHi(0.15);
		tempVar80.setIaiLo(50);
		tempVar80.setIaiHi(100);
		tempVar80.setEc(2);
		PollutantConLimits pollutantConLimits49 = tempVar80;
		getPollutantConLimitsList().add(pollutantConLimits49);

		PollutantConLimits tempVar81 = new PollutantConLimits();
		tempVar81.setPollutantCode("SO2");
		tempVar81.setBpLo(0.15);
		tempVar81.setBpHi(0.8);
		tempVar81.setIaiLo(100);
		tempVar81.setIaiHi(200);
		tempVar81.setEc(2);
		PollutantConLimits pollutantConLimits50 = tempVar81;
		getPollutantConLimitsList().add(pollutantConLimits50);

		PollutantConLimits tempVar82 = new PollutantConLimits();
		tempVar82.setPollutantCode("SO2");
		tempVar82.setBpLo(0.8);
		tempVar82.setBpHi(1.6);
		tempVar82.setIaiLo(200);
		tempVar82.setIaiHi(300);
		tempVar82.setEc(2);
		PollutantConLimits pollutantConLimits51 = tempVar82;
		getPollutantConLimitsList().add(pollutantConLimits51);

		PollutantConLimits tempVar83 = new PollutantConLimits();
		tempVar83.setPollutantCode("SO2");
		tempVar83.setBpLo(1.6);
		tempVar83.setBpHi(2.1);
		tempVar83.setIaiLo(300);
		tempVar83.setIaiHi(400);
		tempVar83.setEc(2);
		PollutantConLimits pollutantConLimits52 = tempVar83;
		getPollutantConLimitsList().add(pollutantConLimits52);

		PollutantConLimits tempVar84 = new PollutantConLimits();
		tempVar84.setPollutantCode("SO2");
		tempVar84.setBpLo(2.1);
		tempVar84.setBpHi(2.62);
		tempVar84.setIaiLo(400);
		tempVar84.setIaiHi(500);
		tempVar84.setEc(2);
		PollutantConLimits pollutantConLimits53 = tempVar84;
		getPollutantConLimitsList().add(pollutantConLimits53);

		/// #region API NO2
		PollutantConLimits tempVar85 = new PollutantConLimits();
		tempVar85.setPollutantCode("NO2");
		tempVar85.setBpLo(0);
		tempVar85.setBpHi(0.08);
		tempVar85.setIaiLo(0);
		tempVar85.setIaiHi(50);
		tempVar85.setEc(2);
		PollutantConLimits pollutantConLimits54 = tempVar85;
		getPollutantConLimitsList().add(pollutantConLimits54);

		PollutantConLimits tempVar86 = new PollutantConLimits();
		tempVar86.setPollutantCode("NO2");
		tempVar86.setBpLo(0.08);
		tempVar86.setBpHi(0.12);
		tempVar86.setIaiLo(50);
		tempVar86.setIaiHi(100);
		tempVar86.setEc(2);
		PollutantConLimits pollutantConLimits55 = tempVar86;
		getPollutantConLimitsList().add(pollutantConLimits55);

		PollutantConLimits tempVar87 = new PollutantConLimits();
		tempVar87.setPollutantCode("NO2");
		tempVar87.setBpLo(0.12);
		tempVar87.setBpHi(0.28);
		tempVar87.setIaiLo(100);
		tempVar87.setIaiHi(200);
		tempVar87.setEc(2);
		PollutantConLimits pollutantConLimits56 = tempVar87;
		getPollutantConLimitsList().add(pollutantConLimits56);

		PollutantConLimits tempVar88 = new PollutantConLimits();
		tempVar88.setPollutantCode("NO2");
		tempVar88.setBpLo(0.28);
		tempVar88.setBpHi(0.565);
		tempVar88.setIaiLo(200);
		tempVar88.setIaiHi(300);
		tempVar88.setEc(2);
		PollutantConLimits pollutantConLimits57 = tempVar88;
		getPollutantConLimitsList().add(pollutantConLimits57);

		PollutantConLimits tempVar89 = new PollutantConLimits();
		tempVar89.setPollutantCode("NO2");
		tempVar89.setBpLo(0.565);
		tempVar89.setBpHi(0.75);
		tempVar89.setIaiLo(300);
		tempVar89.setIaiHi(400);
		tempVar89.setEc(2);
		PollutantConLimits pollutantConLimits58 = tempVar89;
		getPollutantConLimitsList().add(pollutantConLimits58);

		PollutantConLimits tempVar90 = new PollutantConLimits();
		tempVar90.setPollutantCode("NO2");
		tempVar90.setBpLo(0.75);
		tempVar90.setBpHi(0.94);
		tempVar90.setIaiLo(400);
		tempVar90.setIaiHi(500);
		tempVar90.setEc(2);
		PollutantConLimits pollutantConLimits59 = tempVar90;
		getPollutantConLimitsList().add(pollutantConLimits59);

		/// #region API PM10
		PollutantConLimits tempVar91 = new PollutantConLimits();
		tempVar91.setPollutantCode("PM10");
		tempVar91.setBpLo(0);
		tempVar91.setBpHi(0.05);
		tempVar91.setIaiLo(0);
		tempVar91.setIaiHi(50);
		tempVar91.setEc(2);
		PollutantConLimits pollutantConLimits60 = tempVar91;
		getPollutantConLimitsList().add(pollutantConLimits60);

		PollutantConLimits tempVar92 = new PollutantConLimits();
		tempVar92.setPollutantCode("PM10");
		tempVar92.setBpLo(0.05);
		tempVar92.setBpHi(0.15);
		tempVar92.setIaiLo(50);
		tempVar92.setIaiHi(100);
		tempVar92.setEc(2);
		PollutantConLimits pollutantConLimits61 = tempVar92;
		getPollutantConLimitsList().add(pollutantConLimits61);

		PollutantConLimits tempVar93 = new PollutantConLimits();
		tempVar93.setPollutantCode("PM10");
		tempVar93.setBpLo(0.15);
		tempVar93.setBpHi(0.35);
		tempVar93.setIaiLo(100);
		tempVar93.setIaiHi(200);
		tempVar93.setEc(2);
		PollutantConLimits pollutantConLimits62 = tempVar93;
		getPollutantConLimitsList().add(pollutantConLimits62);

		PollutantConLimits tempVar94 = new PollutantConLimits();
		tempVar94.setPollutantCode("PM10");
		tempVar94.setBpLo(0.35);
		tempVar94.setBpHi(0.42);
		tempVar94.setIaiLo(200);
		tempVar94.setIaiHi(300);
		tempVar94.setEc(2);
		PollutantConLimits pollutantConLimits63 = tempVar94;
		getPollutantConLimitsList().add(pollutantConLimits63);

		PollutantConLimits tempVar95 = new PollutantConLimits();
		tempVar95.setPollutantCode("PM10");
		tempVar95.setBpLo(0.42);
		tempVar95.setBpHi(0.5);
		tempVar95.setIaiLo(300);
		tempVar95.setIaiHi(400);
		tempVar95.setEc(2);
		PollutantConLimits pollutantConLimits64 = tempVar95;
		getPollutantConLimitsList().add(pollutantConLimits64);

		PollutantConLimits tempVar96 = new PollutantConLimits();
		tempVar96.setPollutantCode("PM10");
		tempVar96.setBpLo(0.5);
		tempVar96.setBpHi(0.6);
		tempVar96.setIaiLo(400);
		tempVar96.setIaiHi(500);
		tempVar96.setEc(2);
		PollutantConLimits pollutantConLimits65 = tempVar96;
		getPollutantConLimitsList().add(pollutantConLimits65);

		/// #region API CO
		PollutantConLimits tempVar97 = new PollutantConLimits();
		tempVar97.setPollutantCode("CO");
		tempVar97.setBpLo(0);
		tempVar97.setBpHi(5);
		tempVar97.setIaiLo(0);
		tempVar97.setIaiHi(50);
		tempVar97.setEc(2);
		PollutantConLimits pollutantConLimits66 = tempVar97;
		getPollutantConLimitsList().add(pollutantConLimits66);

		PollutantConLimits tempVar98 = new PollutantConLimits();
		tempVar98.setPollutantCode("CO");
		tempVar98.setBpLo(5);
		tempVar98.setBpHi(10);
		tempVar98.setIaiLo(50);
		tempVar98.setIaiHi(100);
		tempVar98.setEc(2);
		PollutantConLimits pollutantConLimits67 = tempVar98;
		getPollutantConLimitsList().add(pollutantConLimits67);

		PollutantConLimits tempVar99 = new PollutantConLimits();
		tempVar99.setPollutantCode("CO");
		tempVar99.setBpLo(10);
		tempVar99.setBpHi(60);
		tempVar99.setIaiLo(100);
		tempVar99.setIaiHi(200);
		tempVar99.setEc(2);
		PollutantConLimits pollutantConLimits68 = tempVar99;
		getPollutantConLimitsList().add(pollutantConLimits68);

		PollutantConLimits tempVar100 = new PollutantConLimits();
		tempVar100.setPollutantCode("CO");
		tempVar100.setBpLo(60);
		tempVar100.setBpHi(90);
		tempVar100.setIaiLo(200);
		tempVar100.setIaiHi(300);
		tempVar100.setEc(2);
		PollutantConLimits pollutantConLimits69 = tempVar100;
		getPollutantConLimitsList().add(pollutantConLimits69);

		PollutantConLimits tempVar101 = new PollutantConLimits();
		tempVar101.setPollutantCode("CO");
		tempVar101.setBpLo(90);
		tempVar101.setBpHi(120);
		tempVar101.setIaiLo(300);
		tempVar101.setIaiHi(400);
		tempVar101.setEc(2);
		PollutantConLimits pollutantConLimits70 = tempVar101;
		getPollutantConLimitsList().add(pollutantConLimits70);

		PollutantConLimits tempVar102 = new PollutantConLimits();
		tempVar102.setPollutantCode("CO");
		tempVar102.setBpLo(120);
		tempVar102.setBpHi(150);
		tempVar102.setIaiLo(400);
		tempVar102.setIaiHi(500);
		tempVar102.setEc(2);
		PollutantConLimits pollutantConLimits71 = tempVar102;
		getPollutantConLimitsList().add(pollutantConLimits71);

		/// #region API O3
		PollutantConLimits tempVar103 = new PollutantConLimits();
		tempVar103.setPollutantCode("O3");
		tempVar103.setBpLo(0);
		tempVar103.setBpHi(0.12);
		tempVar103.setIaiLo(0);
		tempVar103.setIaiHi(50);
		tempVar103.setEc(2);
		PollutantConLimits pollutantConLimits72 = tempVar103;
		getPollutantConLimitsList().add(pollutantConLimits72);

		PollutantConLimits tempVar104 = new PollutantConLimits();
		tempVar104.setPollutantCode("O3");
		tempVar104.setBpLo(0.12);
		tempVar104.setBpHi(0.2);
		tempVar104.setIaiLo(50);
		tempVar104.setIaiHi(100);
		tempVar104.setEc(2);
		PollutantConLimits pollutantConLimits91 = tempVar104;
		getPollutantConLimitsList().add(pollutantConLimits91);

		PollutantConLimits tempVar105 = new PollutantConLimits();
		tempVar105.setPollutantCode("O3");
		tempVar105.setBpLo(0.2);
		tempVar105.setBpHi(0.4);
		tempVar105.setIaiLo(100);
		tempVar105.setIaiHi(200);
		tempVar105.setEc(2);
		PollutantConLimits pollutantConLimits92 = tempVar105;
		getPollutantConLimitsList().add(pollutantConLimits92);

		PollutantConLimits tempVar106 = new PollutantConLimits();
		tempVar106.setPollutantCode("O3");
		tempVar106.setBpLo(0.4);
		tempVar106.setBpHi(0.8);
		tempVar106.setIaiLo(200);
		tempVar106.setIaiHi(300);
		tempVar106.setEc(2);
		PollutantConLimits pollutantConLimits93 = tempVar106;
		getPollutantConLimitsList().add(pollutantConLimits93);

		PollutantConLimits tempVar107 = new PollutantConLimits();
		tempVar107.setPollutantCode("O3");
		tempVar107.setBpLo(0.8);
		tempVar107.setBpHi(1);
		tempVar107.setIaiLo(300);
		tempVar107.setIaiHi(400);
		tempVar107.setEc(2);
		PollutantConLimits pollutantConLimits94 = tempVar107;
		getPollutantConLimitsList().add(pollutantConLimits94);

		PollutantConLimits tempVar108 = new PollutantConLimits();
		tempVar108.setPollutantCode("O3");
		tempVar108.setBpLo(1);
		tempVar108.setBpHi(1.2);
		tempVar108.setIaiLo(400);
		tempVar108.setIaiHi(500);
		tempVar108.setEc(2);
		PollutantConLimits pollutantConLimits95 = tempVar108;
		getPollutantConLimitsList().add(pollutantConLimits95);

		/// #region 污染物信息
		PollutantInfo tempVar109 = new PollutantInfo();
		tempVar109.setName("SO2");
		tempVar109.setChineseName("二氧化硫");
		PollutantInfo info1 = tempVar109;
		getPollutantInfoList().add(info1);

		PollutantInfo tempVar110 = new PollutantInfo();
		tempVar110.setName("SO2-1h");
		tempVar110.setChineseName("二氧化硫");
		PollutantInfo info10 = tempVar110;
		getPollutantInfoList().add(info10);

		PollutantInfo tempVar111 = new PollutantInfo();
		tempVar111.setName("NO2");
		tempVar111.setChineseName("二氧化氮");
		PollutantInfo info2 = tempVar111;
		getPollutantInfoList().add(info2);

		PollutantInfo tempVar112 = new PollutantInfo();
		tempVar112.setName("NO2-1h");
		tempVar112.setChineseName("二氧化氮");
		PollutantInfo info9 = tempVar112;
		getPollutantInfoList().add(info9);

		PollutantInfo tempVar113 = new PollutantInfo();
		tempVar113.setName("PM10");
		tempVar113.setChineseName("颗粒物(PM10)");
		PollutantInfo info3 = tempVar113;
		getPollutantInfoList().add(info3);

		PollutantInfo tempVar114 = new PollutantInfo();
		tempVar114.setName("CO");
		tempVar114.setChineseName("一氧化碳");
		PollutantInfo info4 = tempVar114;
		getPollutantInfoList().add(info4);

		PollutantInfo tempVar115 = new PollutantInfo();
		tempVar115.setName("CO-1h");
		tempVar115.setChineseName("一氧化碳");
		PollutantInfo info8 = tempVar115;
		getPollutantInfoList().add(info8);

		PollutantInfo tempVar116 = new PollutantInfo();
		tempVar116.setName("O3-1h");
		tempVar116.setChineseName("臭氧1小时");
		PollutantInfo info5 = tempVar116;
		getPollutantInfoList().add(info5);

		PollutantInfo tempVar117 = new PollutantInfo();
		tempVar117.setName("O3-8h");
		tempVar117.setChineseName("臭氧8小时");
		PollutantInfo info6 = tempVar117;
		getPollutantInfoList().add(info6);

		PollutantInfo tempVar118 = new PollutantInfo();
		tempVar118.setName("PM2.5");
		tempVar118.setChineseName("细颗粒物(PM2.5)");
		PollutantInfo info7 = tempVar118;
		getPollutantInfoList().add(info7);
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
		/// #endregion
	}
}