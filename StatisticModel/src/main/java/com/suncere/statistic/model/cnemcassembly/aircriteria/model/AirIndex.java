package com.suncere.statistic.model.cnemcassembly.aircriteria.model;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2019年12月31日
 */
public class AirIndex {

    private int id;
    private int aiLo;
    private int aiHi;
    private String level;
    private String chineseLevel;
    private String type;
    private String color;
    private String unheathful;
    private String measure;
    private int ec;
    private String inColor;
    private String aImage;
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAiLo() {
		return aiLo;
	}
	public void setAiLo(int aiLo) {
		this.aiLo = aiLo;
	}
	public int getAiHi() {
		return aiHi;
	}
	public void setAiHi(int aiHi) {
		this.aiHi = aiHi;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getChineseLevel() {
		return chineseLevel;
	}
	public void setChineseLevel(String chineseLevel) {
		this.chineseLevel = chineseLevel;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getUnheathful() {
		return unheathful;
	}
	public void setUnheathful(String unheathful) {
		this.unheathful = unheathful;
	}
	public String getMeasure() {
		return measure;
	}
	public void setMeasure(String measure) {
		this.measure = measure;
	}
	public int getEc() {
		return ec;
	}
	public void setEc(int ec) {
		this.ec = ec;
	}
	public String getInColor() {
		return inColor;
	}
	public void setInColor(String inColor) {
		this.inColor = inColor;
	}
	public String getaImage() {
		return aImage;
	}
	public void setaImage(String aImage) {
		this.aImage = aImage;
	}
}
