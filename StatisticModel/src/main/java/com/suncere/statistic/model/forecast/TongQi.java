package com.suncere.statistic.model.forecast;

import com.suncere.statistic.entity.EntityModel.PollutantForecastMetaData;
import com.suncere.statistic.entity.EntityModel.PollutantMonitorData;
import com.suncere.statistic.model.BaseStatisticModel;
import com.suncere.statistic.model.dataprocessing.MultipleRegressionFittingData;
import com.suncere.statistic.model.dataprocessing.MultipleRegressionOperate;
import com.suncere.statistic.serivce.enums.PollutantParam;
import com.suncere.statistic.utils.MathUtil;
import com.suncere.statistic.utils.StringExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * 同期回归模型 Creator：denggq CreateTime:2015/11/05
 * 
 */
public class TongQi extends BaseStatisticModel {

	/// #region 构造函数

	public TongQi() {
		super();
	}

	public TongQi(String code,String cityCode, LocalDateTime timePoint) {
		super(code,cityCode,timePoint);
		this.sampleCount = 30;
		this.factorCount = 4;
		this.forecastDayCount = 7;

		this.forecastModelId = 2;
		this.beginTime = timePoint.plusDays(-this.sampleCount);
		this.endTime = timePoint.plusDays(-1);
	}

	/**
	 * 同期回归模型构造函数
	 * 
	 * @param code             编码（城市、区域、站点）
	 * @param sampleCount      模型样本数
	 * @param factorCount      模型自变量个数
	 * @param timePoint        日期
	 * @param forecastDayCount 预报天数
	 */
	public TongQi(String code, String cityCode, int sampleCount, int factorCount, LocalDateTime timePoint, int forecastDayCount) {
		super(code,cityCode,sampleCount, factorCount, timePoint, forecastDayCount);
		this.forecastModelId = 2;
		this.beginTime = timePoint.plusDays(-this.sampleCount);
		this.endTime = timePoint.plusDays(-1);
	}

	/// #region 私有变量
	private double[] yPollutant;

	private double[] x1Pollutant;
	private double[] x2Pollutant;
	private double[] x3Pollutant;
	private double[] x4Pollutant;
	private BigDecimal[] pre4Pollutant;

	/**
	 * 数据完整性检查
	 * 
	 */
	@Override
	protected void DataCompletionCheck() {
		if (getPollutantMonitorList() == null || getPollutantMonitorList().isEmpty()) {

			throw new RuntimeException("缺失污染物监测数据");
		}
		this.pollutantMonitorList = pollutantMonitorList.stream()
				.sorted(Comparator.comparing(PollutantMonitorData::getTimePoint))
				.collect(Collectors.toList());
	}

	/**
	 * 数据准备
	 * 
	 * @param pollutantName 污染物名称
	 */
	@Override
	protected void DataPrepare(String pollutantName) {
		// 模型的输入数据
		yPollutant = new double[this.sampleCount - 4];
		x1Pollutant = new double[this.sampleCount - 4];
		x2Pollutant = new double[this.sampleCount - 4];
		x3Pollutant = new double[this.sampleCount - 4];
		x4Pollutant = new double[this.sampleCount - 4];
		pre4Pollutant = new BigDecimal[4]; // 前4天的污染物数据 t-1、t-2、t-3、t-4

		/// #region 填充初始数据
		for (int i = 0; i < this.sampleCount - 4; i++) {
			if (pollutantName.equals(PollutantParam.SO2.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i + 4).getSo2().doubleValue();
				x1Pollutant[i] = this.pollutantMonitorList.get(i + 3).getSo2().doubleValue();
				x2Pollutant[i] = this.pollutantMonitorList.get(i + 2).getSo2().doubleValue();
				x3Pollutant[i] = this.pollutantMonitorList.get(i + 1).getSo2().doubleValue();
				x4Pollutant[i] = this.pollutantMonitorList.get(i).getSo2().doubleValue();
			}
			else if (pollutantName.equals(PollutantParam.NO2.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i + 4).getNo2().doubleValue();
				x1Pollutant[i] = this.pollutantMonitorList.get(i + 3).getNo2().doubleValue();
				x2Pollutant[i] = this.pollutantMonitorList.get(i + 2).getNo2().doubleValue();
				x3Pollutant[i] = this.pollutantMonitorList.get(i + 1).getNo2().doubleValue();
				x4Pollutant[i] = this.pollutantMonitorList.get(i).getNo2().doubleValue();
			}
			else if (pollutantName.equals(PollutantParam.CO.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i + 4).getCo().doubleValue();
				x1Pollutant[i] = this.pollutantMonitorList.get(i + 3).getCo().doubleValue();
				x2Pollutant[i] = this.pollutantMonitorList.get(i + 2).getCo().doubleValue();
				x3Pollutant[i] = this.pollutantMonitorList.get(i + 1).getCo().doubleValue();
				x4Pollutant[i] = this.pollutantMonitorList.get(i).getCo().doubleValue();
			}
			else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i + 4).getPm2_5().doubleValue();
				x1Pollutant[i] = this.pollutantMonitorList.get(i + 3).getPm2_5().doubleValue();
				x2Pollutant[i] = this.pollutantMonitorList.get(i + 2).getPm2_5().doubleValue();
				x3Pollutant[i] = this.pollutantMonitorList.get(i + 1).getPm2_5().doubleValue();
				x4Pollutant[i] = this.pollutantMonitorList.get(i).getPm2_5().doubleValue();
			}
			else if (pollutantName.equals(PollutantParam.PM10.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i + 4).getPm10().doubleValue();
				x1Pollutant[i] = this.pollutantMonitorList.get(i + 3).getPm10().doubleValue();
				x2Pollutant[i] = this.pollutantMonitorList.get(i + 2).getPm10().doubleValue();
				x3Pollutant[i] = this.pollutantMonitorList.get(i + 1).getPm10().doubleValue();
				x4Pollutant[i] = this.pollutantMonitorList.get(i).getPm10().doubleValue();
			}
			else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i + 4).getO3_8h().doubleValue();
				x1Pollutant[i] = this.pollutantMonitorList.get(i + 3).getO3_8h().doubleValue();
				x2Pollutant[i] = this.pollutantMonitorList.get(i + 2).getO3_8h().doubleValue();
				x3Pollutant[i] = this.pollutantMonitorList.get(i + 1).getO3_8h().doubleValue();
				x4Pollutant[i] = this.pollutantMonitorList.get(i).getO3_8h().doubleValue();
			} else {
				yPollutant[i] = 0;
				x1Pollutant[i] = 0;
				x2Pollutant[i] = 0;
				x3Pollutant[i] = 0;
				x4Pollutant[i] = 0;
			}
		}
		
		/// #region 预处理
		MultipleRegressionOperate.PreTreatment(yPollutant, this.sampleCount - 4);
		MultipleRegressionOperate.PreTreatment(x1Pollutant, this.sampleCount - 4);
		MultipleRegressionOperate.PreTreatment(x2Pollutant, this.sampleCount - 4);
		MultipleRegressionOperate.PreTreatment(x3Pollutant, this.sampleCount - 4);
		MultipleRegressionOperate.PreTreatment(x4Pollutant, this.sampleCount - 4);
	}

	/**
	 * 预报过程
	 * 
	 */
	@Override
	protected void Predict() {
		DataCompletionCheck();
		for (String pollutantName : pollutantNames) {

			DataPrepare(pollutantName);

			/// #region 预报

			BigDecimal last_pollutant = new BigDecimal(0);

			ArrayList<PollutantForecastMetaData> result = new ArrayList<PollutantForecastMetaData>();

			boolean isFirstDay = true;

			for (LocalDateTime forTime = this.timePoint, endForTime = this.timePoint.plusDays(
					this.forecastDayCount - 1); forTime.compareTo(endForTime) <= 0; forTime = forTime.plusDays(1)) // 7天预报数据，从当前天到后6天
			{
				PollutantForecastMetaData fd = new PollutantForecastMetaData();
				fd.setTimePoint(this.timePoint);
				fd.setForTime(forTime);
				fd.setPolllutantName(pollutantName);

				if (isFirstDay) // 第一天使用初始拟合的模型 不需重新组织数据
				{
					isFirstDay = false;
				} else {
					// 重新组织数据
					SetNewArrayInTongQi(yPollutant, x1Pollutant, x2Pollutant, x3Pollutant, x4Pollutant, last_pollutant,
							this.sampleCount - 4);
				}

				// 前4天的污染物数据 t-1、t-2、t-3、t-4
				for (int i = 0; i < pre4Pollutant.length; i++) {
					pre4Pollutant[i] = BigDecimal.valueOf(yPollutant[this.sampleCount - 4 - 1 - i]);
				}

				// 模型拟合 自变量x按 t-1、t-2、t-3、t-4的顺序拟合
				MultipleRegressionFittingData mrPollutant = MultipleRegressionOperate.MultipleRegressionFitting(
						new double[][] { x1Pollutant, x2Pollutant, x3Pollutant, x4Pollutant }, yPollutant,
						this.factorCount, this.sampleCount - 4);

				if (!Double.isNaN(mrPollutant.getR())) {
					BigDecimal Last_Coefficient = MathUtil.roundBank(mrPollutant.getA()[0], 3);
					BigDecimal AirTemperature_Coefficient = MathUtil.roundBank(mrPollutant.getA()[1], 3);
					BigDecimal WindDirection_Coefficient = MathUtil.roundBank(mrPollutant.getA()[2], 3);
					BigDecimal WindSpeed_Coefficient = MathUtil.roundBank(mrPollutant.getA()[3], 3);
					BigDecimal Intercept_Coefficient = MathUtil.roundBank(mrPollutant.getA()[4], 3);
					BigDecimal Q = MathUtil.roundBank(mrPollutant.getQ(), 3);
//					BigDecimal S = MathUtil.roundBank(mrPollutant.getS(), 3);
//					BigDecimal R = MathUtil.roundBank(mrPollutant.getR(), 3);
//					BigDecimal U = MathUtil.roundBank(mrPollutant.getU(), 3);
					BigDecimal StandardDeviation = MathUtil.roundBank(
							Math.sqrt((1 / (this.sampleCount - this.factorCount - 1)) * Q.doubleValue()),
							3);

					fd.setValue(
							Last_Coefficient.multiply(pre4Pollutant[0])
							.add(AirTemperature_Coefficient.multiply(pre4Pollutant[1]))
							.add(WindDirection_Coefficient.multiply(pre4Pollutant[2]))
							.add(WindSpeed_Coefficient.multiply(pre4Pollutant[0]))
							.add(Intercept_Coefficient));// Last_Coefficient * pre4Pollutant[0] + AirTemperature_Coefficient * pre4Pollutant[1] + WindDirection_Coefficient * pre4Pollutant[2] + WindSpeed_Coefficient * pre4Pollutant[0] + Intercept_Coefficient
					fd.setValue(MathUtil.roundBank(fd.getValue().doubleValue(), 3));
					fd.setFloatingValue(StringExtension.DecimalRound(StandardDeviation, 3, BigDecimal.valueOf(0)));
					last_pollutant = fd.getValue();
					result.add(fd);
				} else {
					break;
				}

			}

			forMetaDataList.addAll(result);
		}

	}

	/**
	 * 预报过程（没有气象预报数据时所用的备用方法）
	 * 
	 */
	@Override
	protected void BackupPredict() {
		// 同期回归模型不需要气象预报数据，因此备用方法调用Predict()即可
		Predict();
	}

	private void SetNewArrayInTongQi(double[] y, double[] x1, double[] x2, double[] x3, double[] x4,
			BigDecimal newValue, int sampleCount) {
		double x1NewValue = y[sampleCount - 1];
		double x2NewValue = x1[sampleCount - 1];
		double x3NewValue = x2[sampleCount - 1];
		double x4NewValue = x3[sampleCount - 1];

		for (int i = 0; i < sampleCount - 1; i++) {
			y[i] = y[i + 1];
			x1[i] = x1[i + 1];
			x2[i] = x2[i + 1];
			x3[i] = x3[i + 1];
			x4[i] = x4[i + 1];
		}
		y[sampleCount - 1] = newValue.doubleValue();
		x1[sampleCount - 1] = x1NewValue;
		x2[sampleCount - 1] = x2NewValue;
		x3[sampleCount - 1] = x3NewValue;
		x4[sampleCount - 1] = x4NewValue;

		MultipleRegressionOperate.CoverTreatment(y, sampleCount);
		MultipleRegressionOperate.CoverTreatment(x1, sampleCount);
		MultipleRegressionOperate.CoverTreatment(x2, sampleCount);
		MultipleRegressionOperate.CoverTreatment(x3, sampleCount);
		MultipleRegressionOperate.CoverTreatment(x4, sampleCount);

	}

}