package com.suncere.statistic.model.forecast;

import com.suncere.statistic.entity.EntityModel.PollutantForecastMetaData;
import com.suncere.statistic.entity.EntityModel.PollutantMonitorData;
import com.suncere.statistic.entity.EntityModel.WeatherForecastData;
import com.suncere.statistic.entity.EntityModel.WeatherMonitorData;
import com.suncere.statistic.model.BaseStatisticModel;
import com.suncere.statistic.model.dataprocessing.AforgeNeuronExtend;
import com.suncere.statistic.model.dataprocessing.MultipleRegressionOperate;
import com.suncere.statistic.serivce.enums.PollutantParam;
import com.suncere.statistic.utils.MathUtil;
import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.deeplearning4j.eval.RegressionEvaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Sgd;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * BP神经网络模型 Creator：denggq CreateTime:2015/11/05
 */
public class BPNet extends BaseStatisticModel {
    /**
     * 因变量个数
     */
    private int outputCount;
    /**
     * 学习速率
     */
    private double learningRate;
    /**
     * 动量项系数
     */
    private double momentum;
    /**
     * 激励函数参数
     */
    private double sigmoidAlphaValue;
    /**
     * 隐层神经元数
     */
    private int neuronsInFirstLayer;
    /**
     * 允许误差
     */
    private double allowableError;
    /**
     * 最大训练循环次数
     */
    private int iterationMax;
    /**
     * 数据同化过程的预报值的权值
     */
    private double weightOfForecastValue;
    /**
     * 因变量
     */
    private double[] yPollutant;

    private double[][] range_input;
    private double[][] range_output;
    private double[][] modelInput_0;
    private double[][] modelOutput_0;

    public BPNet() {
        super();
    }

    public BPNet(String code,String cityCode, LocalDateTime timePoint) {
        super(code,cityCode,timePoint);
        this.sampleCount = 40;
        this.factorCount = 6;
        this.forecastDayCount = 7;

        this.outputCount = 1;
        this.learningRate = 0.1;
        this.momentum = 0.0;
        this.sigmoidAlphaValue = 2.0;
        this.neuronsInFirstLayer = 5;
        this.allowableError = 0.5;
        this.iterationMax = 10000;
        this.weightOfForecastValue = 0.5;
        this.forecastModelId = 3;

        this.beginTime = timePoint.plusDays(-this.sampleCount - 8);
        this.endTime = timePoint.plusDays(-1);
    }

    /**
     * BP神经网络模型构造函数
     *
     * @param code             编码（城市、区域、站点）
     * @param sampleCount      模型样本数
     * @param factorCount      模型自变量个数
     * @param timePoint        日期
     * @param forecastDayCount 预报天数
     */
    public BPNet(String code,String cityCode, int sampleCount, int factorCount, LocalDateTime timePoint, int forecastDayCount) {
        super(code,cityCode,sampleCount, factorCount, timePoint, forecastDayCount);

        this.outputCount = 1;
        this.learningRate = 0.1;
        this.momentum = 0.0;
        this.sigmoidAlphaValue = 2.0;
        this.neuronsInFirstLayer = 5;
        this.allowableError = 0.5;
        this.iterationMax = 10000;
        this.weightOfForecastValue = 0.5;
        this.forecastModelId = 3;
        this.beginTime = timePoint.plusDays(-this.sampleCount - 8);//样本数据是开始时间为样本数+8天
        this.endTime = timePoint.plusDays(-1);
    }

    /**
     * 数据完整性检查
     */
    @Override
    protected void DataCompletionCheck() {
        if (this.pollutantMonitorList == null || this.pollutantMonitorList.isEmpty()) {
            throw new RuntimeException("缺失污染物监测数据");
        }
        if (this.weatherMonitorList == null || this.weatherMonitorList.isEmpty()) {
            throw new RuntimeException("缺失气象监测数据");
        }

        //按样本时间倒序排序,后面会根据索引下标i+1获取前一天样本
        this.pollutantMonitorList = pollutantMonitorList.stream()
                .sorted(Comparator.comparing(PollutantMonitorData::getTimePoint).reversed())
                .collect(Collectors.toList());
        this.weatherMonitorList = weatherMonitorList.stream()
                .sorted(Comparator.comparing(WeatherMonitorData::getTimePoint).reversed()).collect(Collectors.toList());
        if (this.weatherForecastList != null) {
            this.weatherForecastList = weatherForecastList.stream()
                    .sorted(Comparator.comparing(WeatherForecastData::getForTime)).collect(Collectors.toList());
        }
    }

    /**
     * 数据准备
     *
     * @param pollutantName 污染物名称（"SO2", "NO2", "NO", "CO", "O3", "PM2_5", "PM10",
     *                      "O3_8H"）
     */
    @Override
    protected void DataPrepare(String pollutantName) {
        // 模型的输入数据
        yPollutant = new double[this.sampleCount]; // 因变量
        double[] xPollutant = new double[this.sampleCount]; // 前一天污染物浓度

        double[] windDirection = new double[this.sampleCount]; // 日均风速
        double[] windSpeed = new double[this.sampleCount]; // 日均风速
        double[] airTemperature = new double[this.sampleCount]; // 日均温度
        double[] humidity = new double[this.sampleCount]; // 日均相对湿度
        double[] rainfall = new double[this.sampleCount]; // 日降雨量

        // 初始数据数据填充
        for (int i = 0; i < this.sampleCount; i++) {
            if (pollutantName.equals(PollutantParam.SO2.name())) {
                yPollutant[i] = this.pollutantMonitorList.get(i).getSo2().doubleValue();
                xPollutant[i] = this.pollutantMonitorList.get(i + 1).getSo2().doubleValue();
            } else if (pollutantName.equals(PollutantParam.NO2.name())) {
                yPollutant[i] = this.pollutantMonitorList.get(i).getNo2().doubleValue();
                xPollutant[i] = this.pollutantMonitorList.get(i + 1).getNo2().doubleValue();
            } else if (pollutantName.equals(PollutantParam.CO.name())) {
                yPollutant[i] = this.pollutantMonitorList.get(i).getCo().doubleValue();
                xPollutant[i] = this.pollutantMonitorList.get(i + 1).getCo().doubleValue();
            } else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
                yPollutant[i] = this.pollutantMonitorList.get(i).getPm2_5().doubleValue();
                xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm2_5().doubleValue();
            } else if (pollutantName.equals(PollutantParam.PM10.name())) {
                yPollutant[i] = this.pollutantMonitorList.get(i).getPm10().doubleValue();
                xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm10().doubleValue();
            } else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
                yPollutant[i] = this.pollutantMonitorList.get(i).getO3_8h().doubleValue();
                xPollutant[i] = this.pollutantMonitorList.get(i + 1).getO3_8h().doubleValue();
            }
//			else if (pollutantName.equals(PollutantParam.NO.name())) {
//				yPollutant[i] = this.pollutantMonitorList.get(i).getNo().doubleValue();
//				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getNo().doubleValue();
//			} else if (pollutantName.equals(PollutantParam.NOx.name())) {
//				yPollutant[i] = this.pollutantMonitorList.get(i).getNox().doubleValue();
//				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getNox().doubleValue();
//			} else if (pollutantName.equals(PollutantParam.O3_1H.name())) {
//				yPollutant[i] = this.pollutantMonitorList.get(i).getO3().doubleValue();
//				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getO3().doubleValue();
//			}
            else {
                yPollutant[i] = 0;
                xPollutant[i] = 0;
            }

            windSpeed[i] = this.weatherMonitorList.get(i).getWindSpeed().doubleValue();
            windDirection[i] = this.weatherMonitorList.get(i).getWindDirection().doubleValue();
            airTemperature[i] = this.weatherMonitorList.get(i).getAirTemperature().doubleValue();
            // high_AirTemperature[i] = (double)monMetDayDatas[i].High_AirTemp;
            humidity[i] = this.weatherMonitorList.get(i).getHumidity().doubleValue();
            rainfall[i] = this.weatherMonitorList.get(i).getRainfall().doubleValue();
        }

        // 预处理
        MultipleRegressionOperate.PreTreatment(yPollutant, this.sampleCount);
        MultipleRegressionOperate.PreTreatment(xPollutant, this.sampleCount);
        MultipleRegressionOperate.PreTreatment(windSpeed, this.sampleCount);
        MultipleRegressionOperate.PreTreatment(windDirection, this.sampleCount);
        MultipleRegressionOperate.PreTreatment(airTemperature, this.sampleCount);
        // MultipleRegressionOperate.PreTreatment(high_AirTemperature, sampleCount);
        MultipleRegressionOperate.PreTreatment(humidity, this.sampleCount);
        MultipleRegressionOperate.PreTreatment(rainfall, this.sampleCount);

        // 填充输入输出参数矩阵（类型转换）
        double[][] modelInput = AforgeNeuronExtend.Combine(xPollutant, airTemperature, windSpeed, windDirection,
                humidity, rainfall); // 模型输入的二维数组
        double[][] modelOutput = AforgeNeuronExtend.Combine(yPollutant);// 模型输出的二维数组

        // 输入输出二维数组的归一化
        range_input = AforgeNeuronExtend.Range(modelInput);
        range_output = AforgeNeuronExtend.Range(modelOutput);
        modelInput_0 = AforgeNeuronExtend.Normalization(modelInput, range_input, 1);
        modelOutput_0 = AforgeNeuronExtend.Normalization(modelOutput, range_output, 0.85);
    }

    /**
     * 预报过程
     */
    @Override
    protected void Predict() {
        DataCompletionCheck();

        for (String pollutantName : pollutantNames) {
            DataPrepare(pollutantName);

            // 模型拟合
            MultiLayerNetwork net = BpModelFit();

            // 预报数据计算
            ArrayList<PollutantForecastMetaData> result = new ArrayList<PollutantForecastMetaData>();

            // 预报日的各输入参数
            double[] predictInput = new double[this.factorCount];

            int dayindex = 0; // 代表预报日的天数，先预报第一天，故dayindex=0，每预报完一天dayindex+1
            for (LocalDateTime forTime = this.timePoint, endForTime = this.timePoint.plusDays(
                    this.forecastDayCount - 1); forTime.compareTo(endForTime) <= 0; forTime = forTime.plusDays(1)) // 7天预报数据，从当前天到后6天
            {
                // 组织预报日的各输入参数（注意参数的顺序）
                predictInput[1] = this.weatherForecastList.get(dayindex).getAirTemp().doubleValue();
                // predictInput[1] = forMetDayDatas[dayindex].High_AirTemp.Value;
                predictInput[2] = this.weatherForecastList.get(dayindex).getWindSpeed().doubleValue();
                predictInput[3] = this.weatherForecastList.get(dayindex).getWindDirection().doubleValue();
                predictInput[4] = this.weatherForecastList.get(dayindex).getRelativeHumidity().doubleValue();
//				predictInput[5] = this.weatherForecastList.get(dayindex).getRainFall().doubleValue();
                if (dayindex == 0) {
                    predictInput[0] = yPollutant[0];
                }

                // 归一化
                double[] predictInput_0 = AforgeNeuronExtend.Normalization(predictInput, range_input, 1);
                // 模拟
                double[] sim = net.output(Nd4j.create(predictInput_0)).toDoubleVector();
                // 反归一化
                double[] predictOutput = AforgeNeuronExtend.Inverse_Normalization(sim, range_output, 0.85);

                PollutantForecastMetaData fd = new PollutantForecastMetaData();
                fd.setTimePoint(this.timePoint);
                fd.setForTime(forTime);
                fd.setPolllutantName(pollutantName);
                fd.setValue(new BigDecimal(predictOutput[0]));
                // fd.Value = Localization.Localize((decimal)predictOutput[0], pollutantName,
                // forTime); // 对预报数值进行本地化
                fd.setValue(MathUtil.roundBank(fd.getValue().doubleValue(), 3));

                // 浮动值设定
                if (pollutantName.equals(PollutantParam.SO2.name())) {
                    fd.setFloatingValue(new BigDecimal(0.002));
                } else if (pollutantName.equals(PollutantParam.NO2.name())) {
                    fd.setFloatingValue(new BigDecimal(0.004));
                } else if (pollutantName.equals(PollutantParam.NO.name())) {
                    fd.setFloatingValue(new BigDecimal(0.001));
                } else if (pollutantName.equals(PollutantParam.NOx.name())) {
                    fd.setFloatingValue(new BigDecimal(0.001));
                } else if (pollutantName.equals(PollutantParam.CO.name())) {
                    fd.setFloatingValue(new BigDecimal(0.05));
                } else if (pollutantName.equals(PollutantParam.O3_1H.name())) {
                    fd.setFloatingValue(new BigDecimal(0.010));
                } else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
                    fd.setFloatingValue(new BigDecimal(0.013));
                } else if (pollutantName.equals(PollutantParam.PM10.name())) {
                    fd.setFloatingValue(new BigDecimal(0.013));
                } else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
                    fd.setFloatingValue(new BigDecimal(0.013));
                } else {
                    fd.setFloatingValue(new BigDecimal(0.013));
                }

                result.add(fd);

                dayindex++;
                predictInput[0] = fd.getValue().doubleValue(); // 把前一天的预报值赋值给下一个预报日的输入参数中的xPollutant

            }
            forMetaDataList.addAll(result);
        }

    }

    private MultiLayerNetwork BpModelFit() {
//		ActivationNetwork network = new ActivationNetwork(new BipolarSigmoidFunction(this.sigmoidAlphaValue),
//				this.factorCount, this.neuronsInFirstLayer, this.outputCount);
//		BackPropagationLearning teacher = new BackPropagationLearning(network);
//		teacher.LearningRate = this.learningRate;
//		teacher.Momentum = this.momentum;
//		
//		double error = 30; // 初始误差
//		int iteration = 1;
//
//		while (error > this.allowableError && iteration < this.iterationMax) {
//			error = teacher.RunEpoch(modelInput_0, modelOutput_0);
//			iteration++;
//		}		

        //训练数据
        INDArray featureNdarray = Nd4j.create(modelInput_0);
        INDArray labelsNdarray = Nd4j.create(modelOutput_0);
        DataSet trainingData = new DataSet(featureNdarray, labelsNdarray);

//        List<DataSet> listDs = trainingData.asList();
//        ListDataSetIterator dataSetIterator = new ListDataSetIterator(listDs,100);

        //Create the network
        MultiLayerNetwork net = new MultiLayerNetwork(new NeuralNetConfiguration.Builder()
                .seed(10)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .weightInit(WeightInit.XAVIER)
                .updater(new Sgd(learningRate))
                .list()
                .layer(0, new DenseLayer.Builder().nIn(this.factorCount).nOut(this.neuronsInFirstLayer)
                        .activation(Activation.LEAKYRELU).build())
                .layer(1, new DenseLayer.Builder().nIn(neuronsInFirstLayer).nOut(neuronsInFirstLayer)
                        .activation(Activation.LEAKYRELU).build())
                .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                        .activation(Activation.IDENTITY)
                        .nIn(this.neuronsInFirstLayer).nOut(this.outputCount).build())
                .pretrain(false).backprop(true).build()
        );
        net.init();
        //net.setListeners(new ScoreIterationListener(1000));

        double error = 30; // 初始误差
        for (int i = 1; error > this.allowableError && i < iterationMax; i++) {
            net.fit(trainingData);
            //System.out.println("score---"+net.score());
        }
        return net;
    }

    /**
     * 评估模型
     *
     * @param net
     */
    public void eval(MultiLayerNetwork net) {
        INDArray featureNdarray = Nd4j.create(modelInput_0);
        INDArray labelsNdarray = Nd4j.create(modelOutput_0);
        DataSet trainingData = new DataSet(featureNdarray, labelsNdarray);

        List<DataSet> listDs = trainingData.asList();
        DataSetIterator testIter = new ListDataSetIterator(listDs, listDs.size());

        while (testIter.hasNext()) {
//            DataSet t = testIter.next();
//            INDArray features = t.getFeatures();
//            INDArray labels = t.getLabels();
//            INDArray predicted = net.output(features,false);
//            eval.eval(labels, predicted);
            RegressionEvaluation regressionEvaluation = net.evaluateRegression(testIter);
//    	    System.out.println("评估模型，RSquared:"+regressionEvaluation.averageRSquared()+",R2:"+regressionEvaluation.averagecorrelationR2());
        }
    }

    /**
     * 预报过程（没有气象预报数据时所用的备用方法）
     */
    @Override
    protected void BackupPredict() {
        for (String pollutantName : pollutantNames) {
            // 模型的输入数据
            yPollutant = new double[this.sampleCount]; // 因变量
            double[] xPollutant = new double[this.sampleCount]; // 前一天污染物浓度

            double[] windDirection = new double[this.sampleCount]; // 日均风速
            double[] windSpeed = new double[this.sampleCount]; // 日均风速
            double[] airTemperature = new double[this.sampleCount]; // 日均温度
            double[] humidity = new double[this.sampleCount]; // 日均相对湿度
            double[] rainfall = new double[this.sampleCount]; // 日降雨量

            // 初始数据数据填充（仅yPollutant,xPollutant）
            for (int i = 0; i < this.sampleCount; i++) {
                if (pollutantName.equals(PollutantParam.SO2.name())) {
                    yPollutant[i] = this.pollutantMonitorList.get(i).getSo2().doubleValue();
                    xPollutant[i] = this.pollutantMonitorList.get(i + 1).getSo2().doubleValue();
                } else if (pollutantName.equals(PollutantParam.NO2.name())) {
                    yPollutant[i] = this.pollutantMonitorList.get(i).getNo2().doubleValue();
                    xPollutant[i] = this.pollutantMonitorList.get(i + 1).getNo2().doubleValue();
                } else if (pollutantName.equals(PollutantParam.CO.name())) {
                    yPollutant[i] = this.pollutantMonitorList.get(i).getCo().doubleValue();
                    xPollutant[i] = this.pollutantMonitorList.get(i + 1).getCo().doubleValue();
                }
//				else if (pollutantName.equals(PollutantParam.NO.name())) {
//					yPollutant[i] = this.pollutantMonitorList.get(i).getNo().doubleValue();
//					xPollutant[i] = this.pollutantMonitorList.get(i + 1).getNo().doubleValue();
//				} else if (pollutantName.equals(PollutantParam.NOx.name())) {
//					yPollutant[i] = this.pollutantMonitorList.get(i).getNox().doubleValue();
//					xPollutant[i] = this.pollutantMonitorList.get(i + 1).getNox().doubleValue();
//				} else if (pollutantName.equals(PollutantParam.O3_1H.name())) {
//					yPollutant[i] = this.pollutantMonitorList.get(i).getO3().doubleValue();
//					xPollutant[i] = this.pollutantMonitorList.get(i + 1).getO3().doubleValue();
//				}
                else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
                    yPollutant[i] = this.pollutantMonitorList.get(i).getPm2_5().doubleValue();
                    xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm2_5().doubleValue();
                } else if (pollutantName.equals(PollutantParam.PM10.name())) {
                    yPollutant[i] = this.pollutantMonitorList.get(i).getPm10().doubleValue();
                    xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm10().doubleValue();
                } else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
                    yPollutant[i] = this.pollutantMonitorList.get(i).getO3_8h().doubleValue();
                    xPollutant[i] = this.pollutantMonitorList.get(i + 1).getO3_8h().doubleValue();
                } else {
                    yPollutant[i] = 0;
                    xPollutant[i] = 0;
                }
            }

            MultipleRegressionOperate.PreTreatment(yPollutant, this.sampleCount);
            MultipleRegressionOperate.PreTreatment(xPollutant, this.sampleCount);

            // 预报数据计算
            ArrayList<PollutantForecastMetaData> result = new ArrayList<PollutantForecastMetaData>();

            // 预报日的各输入参数
            double[] predictInput = new double[getFactorCount()];
            // 组织预报日的各输入参数（注意参数的顺序）
            predictInput[1] = this.weatherMonitorList.get(0).getAirTemperature().doubleValue();
            // predictInput[1] = (double)monMetDayDatas[0].High_AirTemp.Value;
            predictInput[2] = this.weatherMonitorList.get(0).getWindSpeed().doubleValue();
            predictInput[3] = this.weatherMonitorList.get(0).getWindDirection().doubleValue();
            predictInput[4] = this.weatherMonitorList.get(0).getHumidity().doubleValue();
            predictInput[5] = this.weatherMonitorList.get(0).getRainfall().doubleValue();

            int dayindex = 1; // 代表预报日的天数，先预报第一天，故dayindex=1，每预报完一天dayindex+1

            for (LocalDateTime forTime = getTimePoint(), endForTime = this.timePoint.plusDays(
                    getForecastDayCount() - 1); forTime.compareTo(endForTime) <= 0; forTime = forTime.plusDays(1)) // 7天预报数据，从当前天到后6天
            {
                PollutantForecastMetaData fd = new PollutantForecastMetaData();
                fd.setTimePoint(getTimePoint());
                fd.setForTime(forTime);
                fd.setPolllutantName(pollutantName);

                if (dayindex == 1) {
                    predictInput[0] = yPollutant[0];
                }

                for (int i = 0; i < this.sampleCount; i++) {
                    windSpeed[i] = this.weatherMonitorList.get(i + dayindex).getWindSpeed().doubleValue();
                    windDirection[i] = this.weatherMonitorList.get(i + dayindex).getWindDirection().doubleValue();
                    airTemperature[i] = this.weatherMonitorList.get(i + dayindex).getAirTemperature().doubleValue();
                    // high_AirTemperature[i] = (double)monMetDayDatas[i+ dayindex].High_AirTemp;
                    humidity[i] = this.weatherMonitorList.get(i + dayindex).getHumidity().doubleValue();
                    rainfall[i] = this.weatherMonitorList.get(i + dayindex).getRainfall().doubleValue();
                }

                MultipleRegressionOperate.PreTreatment(windSpeed, this.sampleCount);
                MultipleRegressionOperate.PreTreatment(windDirection, this.sampleCount);
                MultipleRegressionOperate.PreTreatment(airTemperature, this.sampleCount);
                // MultipleRegressionOperate.PreTreatment(high_AirTemperature, sampleCount);
                MultipleRegressionOperate.PreTreatment(humidity, this.sampleCount);
                MultipleRegressionOperate.PreTreatment(rainfall, this.sampleCount);

                // 填充输入输出参数矩阵（类型转换）
                double[][] modelInput = AforgeNeuronExtend.Combine(xPollutant, airTemperature, windSpeed, windDirection,
                        humidity, rainfall);// 模型输入的二维数组
                double[][] modelOutput = AforgeNeuronExtend.Combine(yPollutant);// 模型输出的二维数组

                // 输入输出二维数组的归一化
                range_input = AforgeNeuronExtend.Range(modelInput);
                range_output = AforgeNeuronExtend.Range(modelOutput);
                modelInput_0 = AforgeNeuronExtend.Normalization(modelInput, range_input, 1);
                modelOutput_0 = AforgeNeuronExtend.Normalization(modelOutput, range_output, 0.85);

                // 模型拟合
                MultiLayerNetwork net = BpModelFit();
                //评估模型
                //eval(net);

                // 归一化
                double[] predictInput_0 = AforgeNeuronExtend.Normalization(predictInput, range_input, 1);
                // 模拟
                double[] sim = net.output(Nd4j.create(predictInput_0)).toDoubleVector();
                // 反归一化
                double[] predictOutput = AforgeNeuronExtend.Inverse_Normalization(sim, range_output, 0.85);

                // fd.Value = Localization.Localize((decimal)predictOutput[0], pollutantName,
                // forTime); // 对预报数值进行本地化
                fd.setValue(MathUtil.roundBank(predictOutput[0], 3));//保留三位小数点

                // 浮动值设定
                BigDecimal floatingValue = new BigDecimal(0.001);
                if (pollutantName.equals(PollutantParam.SO2.name())) {
                    fd.setFloatingValue(new BigDecimal(0.002));
                } else if (pollutantName.equals(PollutantParam.NO2.name())) {
                    fd.setFloatingValue(new BigDecimal(0.004));
                } else if (pollutantName.equals(PollutantParam.NO.name())) {
                    fd.setFloatingValue(floatingValue);
                } else if (pollutantName.equals(PollutantParam.NOx.name())) {
                    fd.setFloatingValue(floatingValue);
                } else if (pollutantName.equals(PollutantParam.CO.name())) {
                    fd.setFloatingValue(new BigDecimal(0.05));
                } else if (pollutantName.equals(PollutantParam.O3_1H.name())) {
                    fd.setFloatingValue(new BigDecimal(0.010));
                } else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
                    fd.setFloatingValue(new BigDecimal(0.013));
                } else if (pollutantName.equals(PollutantParam.PM10.name())) {
                    fd.setFloatingValue(new BigDecimal(0.013));
                } else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
                    fd.setFloatingValue(new BigDecimal(0.013));
                } else {
                    fd.setFloatingValue(new BigDecimal(0.013));
                }

                result.add(fd);

                dayindex++;
                predictInput[0] = fd.getValue().doubleValue(); // 把前一天的预报值赋值给下一个预报日的输入参数中的xPollutant

            }
            forMetaDataList.addAll(result);
        }
    }

    public int getOutputCount() {
        return outputCount;
    }

    public void setOutputCount(int outputCount) {
        this.outputCount = outputCount;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public double getMomentum() {
        return momentum;
    }

    public void setMomentum(double momentum) {
        this.momentum = momentum;
    }

    public double getSigmoidAlphaValue() {
        return sigmoidAlphaValue;
    }

    public void setSigmoidAlphaValue(double sigmoidAlphaValue) {
        this.sigmoidAlphaValue = sigmoidAlphaValue;
    }

    public int getNeuronsInFirstLayer() {
        return neuronsInFirstLayer;
    }

    public void setNeuronsInFirstLayer(int neuronsInFirstLayer) {
        this.neuronsInFirstLayer = neuronsInFirstLayer;
    }

    public double getAllowableError() {
        return allowableError;
    }

    public void setAllowableError(double allowableError) {
        this.allowableError = allowableError;
    }

    public int getIterationMax() {
        return iterationMax;
    }

    public void setIterationMax(int iterationMax) {
        this.iterationMax = iterationMax;
    }

    public double getWeightOfForecastValue() {
        return weightOfForecastValue;
    }

    public void setWeightOfForecastValue(double weightOfForecastValue) {
        this.weightOfForecastValue = weightOfForecastValue;
    }

    public double[] getyPollutant() {
        return yPollutant;
    }

    public void setyPollutant(double[] yPollutant) {
        this.yPollutant = yPollutant;
    }

    public double[][] getRange_input() {
        return range_input;
    }

    public void setRange_input(double[][] range_input) {
        this.range_input = range_input;
    }

    public double[][] getRange_output() {
        return range_output;
    }

    public void setRange_output(double[][] range_output) {
        this.range_output = range_output;
    }

    public double[][] getModelInput_0() {
        return modelInput_0;
    }

    public void setModelInput_0(double[][] modelInput_0) {
        this.modelInput_0 = modelInput_0;
    }

    public double[][] getModelOutput_0() {
        return modelOutput_0;
    }

    public void setModelOutput_0(double[][] modelOutput_0) {
        this.modelOutput_0 = modelOutput_0;
    }
}