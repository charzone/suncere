package com.suncere.statistic.model;

import com.suncere.statistic.entity.CityMonitorDayPollutant;
import com.suncere.statistic.entity.CityMonitorDayWeather;
import com.suncere.statistic.entity.CityWeatherForeastInfo;
import com.suncere.statistic.entity.EntityModel.*;
import com.suncere.statistic.entity.ForecastPollutantRange;
import com.suncere.statistic.serivce.CityMonitorDayPollutantService;
import com.suncere.statistic.serivce.CityMonitorDayWeatherService;
import com.suncere.statistic.serivce.CityWeatherForeastInfoService;
import com.suncere.statistic.serivce.ForecastPollutantRangeService;
import com.suncere.statistic.serivce.enums.PollutantParam;
import com.suncere.statistic.utils.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class StatisticModelBll {
    @Autowired
    private CityMonitorDayPollutantService cityMonitorDayPollutantService;
    @Autowired
    private CityMonitorDayWeatherService cityMonitorDayWeatherService;
    @Autowired
    private CityWeatherForeastInfoService cityWeatherForeastInfoService;
    @Autowired
    private ForecastPollutantRangeService forecastPollutantRangeService;

    private static BigDecimal defaultValue = new BigDecimal(-99);
    /**
     * 区域站点预测预报专用
     *
     * @param forecastModel
     * @param monAirDayDatas
     * @return
     */
    public ArrayList<PollutantForecastData> Predict(BaseStatisticModel forecastModel, ArrayList<CityMonitorDayPollutant> monAirDayDatas) {
        // 气象监测日均值
        ArrayList<CityMonitorDayWeather> monMetDayDatas = cityMonitorDayWeatherService.GetDayMonDataCompletion(
                forecastModel.getCityCode(), forecastModel.getBeginTime(), forecastModel.getEndTime(), defaultValue);
        // 气象预报
        ArrayList<CityWeatherForeastInfo> forMetDayDatas = cityWeatherForeastInfoService.GetDayForDataCompletion(forecastModel.getCityCode(), forecastModel.getTimePoint(), defaultValue);
        // 污染物预报浓度值范围限制规则
        ArrayList<ForecastPollutantRange> forecastPollutantRangeList = forecastPollutantRangeService.GetList();
        //对除了CO外的污染物的限值 * 1000
        forecastPollutantRangeList.stream().forEach(range -> {
            if (range.getPollutant() != null && !PollutantParam.CO.name().equals(range.getPollutant())) {
                range.setLowerLimit(range.getLowerLimit().multiply(new BigDecimal(1000)));
                range.setUpperLimit(range.getUpperLimit().multiply(new BigDecimal(1000)));
            }
        });
        List<PollutantMonitorData> pollutantDataList;
        List<WeatherMonitorData> weatherDataList;
        List<WeatherForecastData> weatherForDataList;
        List<ForecastPollutantRangeDto> rangeList;
        try {
            pollutantDataList = ObjectUtil.copyList(monAirDayDatas, PollutantMonitorData.class);
            weatherDataList = ObjectUtil.copyList(monMetDayDatas, WeatherMonitorData.class);
            weatherForDataList = ObjectUtil.copyList(forMetDayDatas, WeatherForecastData.class);
            rangeList = ObjectUtil.copyList(forecastPollutantRangeList, ForecastPollutantRangeDto.class);
        } catch (Exception e) {
            throw new RuntimeException("参数不正确", e);
        }

        // 数据源及模型设置
        forecastModel.setIsAccordingNationStandard(true);
        forecastModel.setPollutantMonitorList(pollutantDataList);
        if (forecastModel.getPollutantMonitorList().size() <= forecastModel.getSampleCount()){
            forecastModel.setSampleCount(forecastModel.getPollutantMonitorList().size() -1);
        }
        forecastModel.setWeatherMonitorList(weatherDataList);
        forecastModel.setWeatherForecastList(weatherForDataList);
        forecastModel.setPollutantRangeList(rangeList);
        forecastModel.setAQILowRange(20);
        forecastModel.setAQIHighRange(35);

        ArrayList<PollutantForecastData> forDataList = forecastModel.PredictOutPut();
        return forDataList;
    }


    /**
     * 城市预测预报
     *
     * @param forecastModel
     * @return
     */
    public ArrayList<PollutantForecastData> Predict(BaseStatisticModel forecastModel) {
        //污染物监测日均值  站点 区域也用此实体即可
        // 城市污染物监测日均值
        ArrayList<CityMonitorDayPollutant>  monAirDayDatas = cityMonitorDayPollutantService.GetDayDataCompletion(
                forecastModel.getCode(), forecastModel.getBeginTime(), forecastModel.getEndTime(), defaultValue);
        return Predict(forecastModel, monAirDayDatas);
//        // 气象监测日均值
//        ArrayList<CityMonitorDayWeather> monMetDayDatas = cityMonitorDayWeatherService.GetDayMonDataCompletion(
//                forecastModel.getCityCode(), forecastModel.getBeginTime(), forecastModel.getEndTime(), defaultValue);
//        // 气象预报
//        ArrayList<CityWeatherForeastInfo> forMetDayDatas = cityWeatherForeastInfoService.GetDayForDataCompletion(forecastModel.getCityCode(), forecastModel.getTimePoint(), defaultValue);
//        // 污染物预报浓度值范围限制规则
//        ArrayList<ForecastPollutantRange> forecastPollutantRangeList = forecastPollutantRangeService.GetList();
//        //对除了CO外的污染物的限值 * 1000
//        forecastPollutantRangeList.stream().forEach(range -> {
//            if (range.getPollutant() != null && !PollutantParam.CO.name().equals(range.getPollutant())) {
//                range.setLowerLimit(range.getLowerLimit().multiply(new BigDecimal(1000)));
//                range.setUpperLimit(range.getUpperLimit().multiply(new BigDecimal(1000)));
//            }
//        });
//        List<PollutantMonitorData> pollutantDataList;
//        List<WeatherMonitorData> weatherDataList;
//        List<WeatherForecastData> weatherForDataList;
//        List<ForecastPollutantRangeDto> rangeList;
//        try {
//            pollutantDataList = ObjectUtil.copyList(monAirDayDatas, PollutantMonitorData.class);
//            weatherDataList = ObjectUtil.copyList(monMetDayDatas, WeatherMonitorData.class);
//            weatherForDataList = ObjectUtil.copyList(forMetDayDatas, WeatherForecastData.class);
//            rangeList = ObjectUtil.copyList(forecastPollutantRangeList, ForecastPollutantRangeDto.class);
//        } catch (Exception e) {
//            throw new RuntimeException("参数不正确", e);
//        }
//
//        // 数据源及模型设置
//        forecastModel.setIsAccordingNationStandard(true);
//        forecastModel.setPollutantMonitorList(pollutantDataList);
//        forecastModel.setWeatherMonitorList(weatherDataList);
//        forecastModel.setWeatherForecastList(weatherForDataList);
//        forecastModel.setPollutantRangeList(rangeList);
//        forecastModel.setAQILowRange(20);
//        forecastModel.setAQIHighRange(35);
//
//        ArrayList<PollutantForecastData> forDataList = forecastModel.PredictOutPut();
//        return forDataList;
    }

//	public static ArrayList<PollutantForecastData> BPPredict(Date timePoint, String code) {
//		BPNet bpNet = new BPNet(code, 40, 6, timePoint, 7);
//
//		ArrayList<City_Monitor_Day_Pollutant> monAirDayDatas = City_Monitor_Day_PollutantBll
//				.GetDayDataCompletion(bpNet.getCode(), bpNet.getBeginTime(), bpNet.getEndTime(), defaultValue); // 污染物监测日均值
//		ArrayList<City_Monitor_Day_Weather> monMetDayDatas = City_Monitor_Day_WeatherBll
//				.GetDayMonDataCompletion(bpNet.getCode(), bpNet.getBeginTime(), bpNet.getEndTime(), defaultValue); // 气象监测日均值
//		// var monAirHourDatas_Today =
//		// City_Monitor_Hour_PollutantBll.GetHourDataCompletion(cityCode, baseTime,
//		// defaultValue).ToList(); //第一天已有的小时值污染物监测数据
//		ArrayList<City_WeatherForeastInfo> forMetDayDatas = City_WeatherForeastInfoBll
//				.GetDayForDataCompletion(bpNet.getCode(), bpNet.getTimePoint(), defaultValue); // 气象预报
//		ArrayList<Forecast_Pollutant_Range> forecastPollutantRangeList = Forecast_Pollutant_RangeBll.GetList();
//
//		ArrayList<PollutantMonitorData> pollutantDataList = ReflectionHelper
//				.<PollutantMonitorData, City_Monitor_Day_Pollutant>Copy(monAirDayDatas, null);
//
//		ArrayList<WeatherMonitorData> weatherDataList = ReflectionHelper
//				.<WeatherMonitorData, City_Monitor_Day_Weather>Copy(monMetDayDatas, null);
//
//		ArrayList<WeatherForecastData> weatherForDataList = ReflectionHelper
//				.<WeatherForecastData, City_WeatherForeastInfo>Copy(forMetDayDatas, null);
//
//		ArrayList<ForecastPollutantRange> rangeList = ReflectionHelper
//				.<ForecastPollutantRange, Forecast_Pollutant_Range>Copy(forecastPollutantRangeList, null);
//
//		// 数据源及模型设置
//		bpNet.setPollutantMonitorList(pollutantDataList);
//		bpNet.setWeatherMonitorList(weatherDataList);
//		bpNet.setWeatherForecastList(weatherForDataList);
//		bpNet.setPollutantRangeList(rangeList);
//		bpNet.setAQILowRange(20);
//		bpNet.setAQIHighRange(35);
//
//		ArrayList<PollutantForecastData> forDataList = bpNet.PredictOutPut();
//
//		return forDataList;
//	}
//
//	public static ArrayList<PollutantForecastData> DuoYuanPredict(Date timePoint, String code)
//	{
//		DuoYuan duoYuan = new DuoYuan(code, 30, 4, timePoint, 7);
//
//
//
////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//		var monAirDayDatas = City_Monitor_Day_PollutantBll.GetDayDataCompletion(duoYuan.getCode(), duoYuan.getBeginTime(), duoYuan.getEndTime(), defaultValue).OrderByDescending(o => o.TimePoint).ToList(); //城市污染物日均值
////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//		var monMetDayDatas = City_Monitor_Day_WeatherBll.GetDayMonDataCompletion(duoYuan.getCode(), duoYuan.getBeginTime(), duoYuan.getEndTime(), defaultValue).OrderByDescending(o => o.TimePoint).ToList(); //气象日均值
////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//		var forMetDayDatas = City_WeatherForeastInfoBll.GetDayForDataCompletion(duoYuan.getCode(), duoYuan.getTimePoint(), defaultValue).OrderBy(o => o.ForTime).ToList(); //气象预报
//
//		ArrayList<Forecast_Pollutant_Range> forecastPollutantRangeList = Forecast_Pollutant_RangeBll.GetList();
//
//		ArrayList<PollutantMonitorData> pollutantDataList = ReflectionHelper.<PollutantMonitorData, City_Monitor_Day_Pollutant>Copy(monAirDayDatas, null);
//
//		ArrayList<WeatherMonitorData> weatherDataList = ReflectionHelper.<WeatherMonitorData, City_Monitor_Day_Weather>Copy(monMetDayDatas, null);
//
//		ArrayList<WeatherForecastData> weatherForDataList = ReflectionHelper.<WeatherForecastData, City_WeatherForeastInfo>Copy(forMetDayDatas, null);
//
//		ArrayList<ForecastPollutantRange> rangeList = ReflectionHelper.<ForecastPollutantRange, Forecast_Pollutant_Range>Copy(forecastPollutantRangeList, null);
//
//		//数据源及模型设置
//		duoYuan.setPollutantMonitorList(pollutantDataList);
//		duoYuan.setWeatherMonitorList(weatherDataList);
//		duoYuan.setWeatherForecastList(weatherForDataList);
//		duoYuan.setPollutantRangeList(rangeList);
//		duoYuan.setAQILowRange(20);
//		duoYuan.setAQIHighRange(35);
//
//
//		ArrayList<PollutantForecastData> forDataList = duoYuan.PredictOutPut();
//
//		return forDataList;
//	}
//
//	public static ArrayList<PollutantForecastData> TongQiPredict(Date timePoint, String code) {
//		TongQi tongQi = new TongQi(code, 30, 4, timePoint, 7);
//
//		ArrayList<City_Monitor_Day_Pollutant> monitorDayPollutantList = City_Monitor_Day_PollutantBll
//				.GetDayDataCompletion(code, tongQi.getBeginTime(), tongQi.getEndTime(), defaultValue); // 前30天的污染物监测日均值数据
//
//		ArrayList<Forecast_Pollutant_Range> forecastPollutantRangeList = Forecast_Pollutant_RangeBll.GetList();
//
//		ArrayList<PollutantMonitorData> pollutantDataList = ReflectionHelper
//				.<PollutantMonitorData, City_Monitor_Day_Pollutant>Copy(monitorDayPollutantList, null);
//
//		ArrayList<ForecastPollutantRange> rangeList = ReflectionHelper
//				.<ForecastPollutantRange, Forecast_Pollutant_Range>Copy(forecastPollutantRangeList, null);
//
//		// 数据源及模型设置
//		tongQi.setPollutantMonitorList(pollutantDataList);
//		tongQi.setPollutantRangeList(rangeList);
//		tongQi.setAQILowRange(20);
//		tongQi.setAQIHighRange(35);
//
//		ArrayList<PollutantForecastData> forDataList = tongQi.PredictOutPut();
//
//		return forDataList;
//	}
//
//	public static ArrayList<PollutantForecastData> JuLeiPredict(Date timePoint, String code) {
//
//		JuLei juLei = new JuLei(code, 40, 4, timePoint, 7);
//
//		ArrayList<City_Monitor_Day_Pollutant> monAirDayDatas = City_Monitor_Day_PollutantBll
//				.GetDayDataCompletion(juLei.getCode(), juLei.getBeginTime(), juLei.getEndTime(), defaultValue); // 城市污染物日均值
//		ArrayList<City_Monitor_Day_Weather> monMetDayDatas = City_Monitor_Day_WeatherBll
//				.GetDayMonDataCompletion(juLei.getCode(), juLei.getBeginTime(), juLei.getEndTime(), defaultValue); // 气象日均值
//		ArrayList<City_WeatherForeastInfo> forMetDayDatas = City_WeatherForeastInfoBll
//				.GetDayForDataCompletion(juLei.getCode(), juLei.getTimePoint(), defaultValue); // 气象预报
//
//		ArrayList<Forecast_Pollutant_Range> forecastPollutantRangeList = Forecast_Pollutant_RangeBll.GetList();
//
//		ArrayList<PollutantMonitorData> pollutantDataList = ReflectionHelper
//				.<PollutantMonitorData, City_Monitor_Day_Pollutant>Copy(monAirDayDatas, null);
//
//		ArrayList<WeatherMonitorData> weatherDataList = ReflectionHelper
//				.<WeatherMonitorData, City_Monitor_Day_Weather>Copy(monMetDayDatas, null);
//
//		ArrayList<WeatherForecastData> weatherForDataList = ReflectionHelper
//				.<WeatherForecastData, City_WeatherForeastInfo>Copy(forMetDayDatas, null);
//
//		ArrayList<ForecastPollutantRange> rangeList = ReflectionHelper
//				.<ForecastPollutantRange, Forecast_Pollutant_Range>Copy(forecastPollutantRangeList, null);
//
//		// 数据源及模型设置
//		juLei.setPollutantMonitorList(pollutantDataList);
//		juLei.setWeatherMonitorList(weatherDataList);
//		juLei.setWeatherForecastList(weatherForDataList);
//		juLei.setPollutantRangeList(rangeList);
//		juLei.setAQILowRange(20);
//		juLei.setAQIHighRange(35);
//
//		ArrayList<PollutantForecastData> forDataList = juLei.PredictOutPut();
//
//		return forDataList;
//	}
}