package com.suncere.statistic.model.cnemcassembly.aircriteria;

import com.suncere.statistic.serivce.enums.EvaluationCriteria;

public class IAPI extends IndividualAirBaseIndexOpreate {
	public IAPI() {
		super(EvaluationCriteria.API);
	}

	@Override
	protected void SetPollutantName() {
		switch (getEPollutants()) {
		case SO2:
			setPollutantName("SO2");
			break;
		case NO2:
			setPollutantName("NO2");
			break;
		case PM10:
			setPollutantName("PM10");
			break;
		default:
			setPollutantName("");
			break;
		}
	}
}