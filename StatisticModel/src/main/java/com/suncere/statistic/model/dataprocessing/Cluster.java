package com.suncere.statistic.model.dataprocessing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.suncere.statistic.utils.MathUtil;

/**
 * 聚类
 */
public class Cluster {
	/// #region k均值聚类

	/**
	 * k均值聚类
	 * 
	 * @param sampleList 输入数据集
	 * @param groupCount 要分类的个数
	 * @return 类的集合
	 */
	public static ArrayList<Group> KmeansCluster(ArrayList<HashMap<String, BigDecimal>> sampleList, int groupCount) {
		final int iterations = 100; // 聚类过程循环次数

		ArrayList<Group> groupList = new ArrayList<Group>(); // 类的集合，分类的个数为groupCount

		// 随机选取样本作为聚类中心
		int[] firstCenterNumber = Randomlize(sampleList.size(), groupCount);
		for (int i = 0; i < groupCount; i++) {
			HashMap<String, BigDecimal> center = Group.clone(sampleList.get(firstCenterNumber[i]));
			Group group = new Group(center);
			groupList.add(group);
		}

		// 聚类过程
		for (int iteration = 0; iteration < iterations; iteration++) {
			for (int i = 0; i < sampleList.size(); i++) {
				BigDecimal minDistance = Distance(sampleList.get(i), groupList.get(0).groupCenter);
				Group belongGroup = groupList.get(0);

				for (int j = 1; j < groupList.size(); j++) {
					BigDecimal distance = Distance(sampleList.get(i), groupList.get(j).groupCenter);
					if (distance.compareTo(minDistance) < 0) {
						minDistance = distance;
						belongGroup = groupList.get(j);
					}
				}

				belongGroup.Add(sampleList.get(i));
			}

			for (Group group : groupList) {
				group.CalculateGroupCenter();
				if (iteration != iterations - 1) {
					group.Clear();
				}
			}
		}

		return groupList;
	}


	/// #region 层次聚类

	/**
	 * 层次聚类
	 * 
	 * @param sampleList 输入数据集
	 * @param groupCount 要分类的个数
	 * @return 类的集合
	 */
	public static ArrayList<Group> HierarchicalCluster(ArrayList<HashMap<String, BigDecimal>> sampleList,
			int groupCount) {
		ArrayList<Group> groupList = new ArrayList<Group>(); // 类的集合

		// 每个样本为一类
		for (int i = 0; i < sampleList.size(); i++) {
			Group g = new Group(Group.clone(sampleList.get(i)));
			g.Add(sampleList.get(i));
			groupList.add(g);
		}

		// 每次循环计算距离最小的类并合并，循环次数为groupCount-targetCount，最后合并为targetCount个类。
		for (int i = 0; i < sampleList.size() - groupCount; i++) {
			HashMap<String, BigDecimal> min = MinDistance(groupList);
			BigDecimal minDistance = min.get("minDistance"); // 最小距离
			BigDecimal num1 = min.get("num1"); // 距离最小的两个类的编号

			BigDecimal num2 = min.get("num2"); // 距离最小的两个类的编号

			groupList.get(num1.intValue()).MergeGroup(groupList.get(num2.intValue())); // 将第二个类合并至第一个类
			groupList.remove(num2.intValue()); // 删除第二个类
		}

		return groupList;
	}

	/// #region 欧式距离
	/**
	 * 两个样本的欧式距离
	 * 
	 */
	public static BigDecimal Distance(HashMap<String, BigDecimal> a, HashMap<String, BigDecimal> b) {
		String[] clusterFactorList = { "xPollutant", "windSpeed", "airTemperature", "humidity" };

		double u = 0;
		for (String factor : clusterFactorList) {
			u = u + Math.pow(a.get(factor).subtract(b.get(factor)).doubleValue(), 2);
		}
		return BigDecimal.valueOf(Math.sqrt(u));
	}

	/// #region 判断距离最小的两个类别，以类中心距为标准
	/**
	 * 判断距离最小的两个类别，以类中心距为标准
	 * 
	 * @param groupList 类的集合
	 * @return 距离最小的两个类的距离及编号，封装在Dictionary<string, decimal>中
	 */
	public static HashMap<String, BigDecimal> MinDistance(ArrayList<Group> groupList) {
		if (groupList.size() == 1) {
			throw new RuntimeException("类的个数只有一个");
		}

		HashMap<String, BigDecimal> min = new HashMap<String, BigDecimal>();
		BigDecimal minDistance = new BigDecimal("79228162514264337593543950335"); // 最小距离
		BigDecimal num1 = new BigDecimal(0); // 距离最小的两个类的编号

		BigDecimal num2 = new BigDecimal(0); // 距离最小的两个类的编号

		for (int i = 0; i < groupList.size(); i++) {
			for (int j = i + 1; j < groupList.size(); j++) {
				BigDecimal distance = Distance(groupList.get(i).groupCenter, groupList.get(j).groupCenter);
				if (distance.compareTo(minDistance) < 0) {
					minDistance = distance;
					num1 = new BigDecimal(i);
					num2 = new BigDecimal(j);
				}
			}
		}

		min.put("minDistance", minDistance);
		min.put("num1", num1);
		min.put("num2", num2);

		return min;
	}

	/// #region 判断数据样本的分类
	/**
	 * 返回数据样本属于的分类的编号
	 * 
	 * @param input     输入数据
	 * @param grouplist 类的集合
	 * @return 在类的集合中返回数据所属的类的编号
	 */
	public static int Judge(HashMap<String, BigDecimal> input, ArrayList<Group> grouplist) {
		int id = 0;
		BigDecimal minDistance = Distance(input, grouplist.get(0).groupCenter);
		for (int i = 1; i < grouplist.size(); i++) {
			BigDecimal distance = Distance(input, grouplist.get(i).groupCenter);
			if (distance.compareTo(minDistance) < 0) {
				minDistance = distance;
				id = i;
			}
		}
		return id;
	}

	/// #region 得到矩阵的极值矩阵
	/**
	 * 得到矩阵每列的最大最小值
	 * 
	 * @param input 输入数据集
	 * @return 数据集的极值数组
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap<String, BigDecimal>[] Range(ArrayList<HashMap<String, BigDecimal>> input) {
		
		BigDecimal max = new BigDecimal(0);
		BigDecimal min = new BigDecimal(0);
		int row = input.size();
		HashMap[] m = new HashMap[] {new HashMap(),new HashMap()};

		for (String key : input.get(0).keySet()) {
			min = input.get(0).get(key);
			max = input.get(0).get(key);
			m[0].put(key, min);
			m[1].put(key, max);
			for (int r = 1; r < row; r++) {
				min = MathUtil.min(min, input.get(r).get(key));
				max = MathUtil.max(max, input.get(r).get(key));
				m[0].put(key, min);
				m[1].put(key, max);
			}
		}

		return m;
	}

	/// #region 归一化
	/**
	 * 将矩阵按行归一化，得到一个新矩阵（极值已知)
	 * 
	 * @param input 要归一化的矩阵
	 * @param m     极值矩阵
	 * @param range 要归一化的正负范围
	 * @return 原矩阵经过归一化得到的新矩阵
	 */
	public static ArrayList<HashMap<String, BigDecimal>> Normalization(ArrayList<HashMap<String, BigDecimal>> input,
			HashMap<String, BigDecimal>[] m, BigDecimal range) {
		int row = input.size();
		ArrayList<HashMap<String, BigDecimal>> nolmalizedList = new ArrayList<HashMap<String, BigDecimal>>();

		for (int r = 0; r < row; r++) {
			HashMap<String, BigDecimal> nolmalizedsample = new HashMap<String, BigDecimal>();
			for (String key : input.get(0).keySet()) {
				nolmalizedsample.put(key, BigDecimal.valueOf(0));
				if (m[0].get(key).equals(m[1].get(key))) {
					nolmalizedsample.put(key, BigDecimal.valueOf(1));
				} else {
					nolmalizedsample.put(key, 
							input.get(r).get(key).subtract(m[0].get(key))
							.divide(m[1].get(key).subtract(m[0].get(key)),10, BigDecimal.ROUND_HALF_EVEN)
							.multiply(BigDecimal.valueOf(2))
							.multiply(range)
							.subtract(range)
							);
				}
			}
			nolmalizedList.add(nolmalizedsample);
		}

		return nolmalizedList;
	}

	/// #region 归一化
	/**
	 * 将向量归一化
	 * 
	 * @param input 要归一化的向量
	 * @param m     极值矩阵
	 * @param range 要归一化的正负范围
	 * @return 归一化后的新向量
	 */
	public static HashMap<String, BigDecimal> Normalization(HashMap<String, BigDecimal> input,
			HashMap<String, BigDecimal>[] m, BigDecimal range) {
		HashMap<String, BigDecimal> nolmalizedsample = new HashMap<String, BigDecimal>();

		for (String key : input.keySet()) {
			nolmalizedsample.put(key, BigDecimal.valueOf(0));
			if (m[0].get(key).equals(m[1].get(key))) {
				nolmalizedsample.put(key, BigDecimal.valueOf(1));
			} else {
				nolmalizedsample.put(key, 
						(input.get(key).subtract(m[0].get(key)))
						.divide((m[1].get(key).subtract(m[0].get(key))),10, BigDecimal.ROUND_HALF_EVEN)
						.multiply(BigDecimal.valueOf(2))
						.multiply(range)
						.subtract(range));
			}
		}

		return nolmalizedsample;
	}
	
	/// #region 反归一化
	/**
	 * 将矩阵按行反归一化，得到一个新矩阵
	 * 
	 * @param input 要反归一化的矩阵
	 * @param m     极值矩阵
	 * @param range 要归一化的正负范围
	 * @return 原矩阵经过归一化得到的新矩阵
	 */
	public static ArrayList<HashMap<String, BigDecimal>> Inverse_Normalization(
			ArrayList<HashMap<String, BigDecimal>> input, HashMap<String, BigDecimal>[] m, BigDecimal range) {
		int row = input.size();
		ArrayList<HashMap<String, BigDecimal>> inverse_nolmalized_List = new ArrayList<HashMap<String, BigDecimal>>();

		for (int r = 0; r < row; r++) {
			HashMap<String, BigDecimal> inverse_nolmalized_sample = new HashMap<String, BigDecimal>();
			for (String key : input.get(0).keySet()) {
				inverse_nolmalized_sample.put(key, input.get(r).get(key));
				if (m[0].get(key).equals(m[1].get(key))) {
					inverse_nolmalized_sample.put(key, m[1].get(key));
				} else {
					inverse_nolmalized_sample.put(key, 
							(input.get(r).get(key).add(range))
							.divide(BigDecimal.valueOf(2))
							.divide(range)
							.multiply((m[1].get(key).subtract(m[0].get(key))))
							.add(m[0].get(key))); //(input[r][key] + range) / 2 / range * (m[1][key] - m[0][key]) + m[0][key];
				}
			}
			inverse_nolmalized_List.add(inverse_nolmalized_sample);
		}

		return inverse_nolmalized_List;
	}
	
	/// #region 反归一化
	/**
	 * 将向量反归一化
	 * 
	 * @param input 要反归一化的向量
	 * @param m     极值矩阵
	 * @param range 要归一化的正负范围
	 * @return 反归一化后的新向量
	 */
	public static HashMap<String, BigDecimal> Inverse_Normalization(HashMap<String, BigDecimal> input,
			HashMap<String, BigDecimal>[] m, BigDecimal range) {
		HashMap<String, BigDecimal> inverse_nolmalized_sample = new HashMap<String, BigDecimal>();

		for (String key : input.keySet()) {
			inverse_nolmalized_sample.put(key, m[1].get(key));
			if (!m[0].get(key).equals(m[1].get(key))) {
				inverse_nolmalized_sample.put(key,
						(input.get(key).add(range))
						.divide(BigDecimal.valueOf(2))
						.divide(range)
						.multiply((m[1].get(key).subtract(m[0].get(key))))
						.add(m[0].get(key))); //(input[key] + range) / 2 / range * (m[1][key] - m[0][key]) + m[0][key]
			}
		}

		return inverse_nolmalized_sample;
	}

	/// #region 生成随机数
	/**
	 * 生成不重复的随机数组
	 * 
	 * @param length       随机数的范围是[0,length-1]之间的整数
	 * @param randomNumber 生成随机数的个数
	 * @return 随机数数组
	 */
	public static int[] Randomlize(int length, int randomNumber) {
		int[] index = new int[length];
		for (int i = 0; i < length; i++) {
			index[i] = i;
		}
		Random r = new Random();
		// 用来保存随机生成的不重复的数
		int[] result = new int[randomNumber];
		int site = length; // 设置上限
		int id;
		for (int j = 0; j < randomNumber; j++) {
			id = r.nextInt(site);
			// 在随机位置取出一个数，保存到结果数组
			result[j] = index[id];
			// 最后一个数复制到当前位置
			index[id] = index[site - 1];
			// 位置的上限减少一
			site--;
		}
		return result;
	}

}