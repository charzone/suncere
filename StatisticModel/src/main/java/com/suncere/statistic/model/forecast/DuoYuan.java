package com.suncere.statistic.model.forecast;

import com.suncere.statistic.entity.EntityModel.PollutantForecastMetaData;
import com.suncere.statistic.entity.EntityModel.PollutantMonitorData;
import com.suncere.statistic.entity.EntityModel.WeatherForecastData;
import com.suncere.statistic.entity.EntityModel.WeatherMonitorData;
import com.suncere.statistic.model.BaseStatisticModel;
import com.suncere.statistic.model.dataprocessing.MultipleRegressionFittingData;
import com.suncere.statistic.model.dataprocessing.MultipleRegressionOperate;
import com.suncere.statistic.serivce.enums.PollutantParam;
import com.suncere.statistic.utils.MathUtil;
import org.deeplearning4j.eval.RegressionEvaluation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * 多元回归模型 Creator：denggq CreateTime:2015/11/05
 * 
 */
public class DuoYuan extends BaseStatisticModel {
	/**
	 * 因变量
	 */
	private double[] yPollutant;
	/**
	 * 前一天污染物浓度
	 */
	private double[] xPollutant;
	/**
	 * 日均风速
	 */
	private double[] windSpeed;
	/**
	 * 日均温度
	 */
	private double[] airTemperature;
	/**
	 * 日均相对湿度
	 */
	private double[] humidity;

	
	// 定义模型参数系数
	private BigDecimal xPollutant_Coefficient = null;
	private BigDecimal airTemperature_Coefficient = null;
	// decimal high_AirTemperature_Coefficient = 0;
	private BigDecimal windSpeed_Coefficient = null;
	private BigDecimal humidity_Coefficient = null;
	private BigDecimal Intercept_Coefficient = null;
	private BigDecimal Q = new BigDecimal(0);
	
	public DuoYuan() {
		super();
	}

	public DuoYuan(String code,String cityCode,LocalDateTime timePoint) {
		super(code,cityCode,timePoint);
		this.sampleCount = 30;
		this.factorCount = 4;
		this.forecastDayCount = 7;
		this.forecastModelId = 1;

		this.beginTime = timePoint.plusDays(-this.sampleCount - 8);
		this.endTime = timePoint.plusDays(-1);
	}

	/**
	 * 多元回归模型构造函数
	 * 
	 * @param code             编码（城市、区域、站点）
	 * @param sampleCount      模型样本数
	 * @param factorCount      模型自变量个数
	 * @param timePoint        日期
	 * @param forecastDayCount 预报天数
	 */
	public DuoYuan(String code,String cityCode, int sampleCount, int factorCount, LocalDateTime timePoint, int forecastDayCount) {
		super(code,cityCode,sampleCount, factorCount, timePoint, forecastDayCount);
		this.forecastModelId = 1;

		this.beginTime = timePoint.plusDays(-this.sampleCount - 8);
		this.endTime = timePoint.plusDays(-1);
	}

	/**
	 * 数据完整性检查
	 */
	@Override
	protected void DataCompletionCheck() {
		if (getPollutantMonitorList() == null || getPollutantMonitorList().isEmpty()) {
			throw new RuntimeException("缺失污染物监测数据");
		}
		if (getWeatherMonitorList() == null || getWeatherMonitorList().isEmpty()) {
			throw new RuntimeException("缺失气象监测数据");
		}

		// 按样本时间倒序排序,后面会根据索引下标i+1获取前一天样本
		this.pollutantMonitorList = pollutantMonitorList.stream()
				.sorted(Comparator.comparing(PollutantMonitorData::getTimePoint).reversed())
				.collect(Collectors.toList());
		this.weatherMonitorList = weatherMonitorList.stream()
				.sorted(Comparator.comparing(WeatherMonitorData::getTimePoint).reversed()).collect(Collectors.toList());
		if (this.weatherForecastList != null) {
			this.weatherForecastList = weatherForecastList.stream()
					.sorted(Comparator.comparing(WeatherForecastData::getForTime)).collect(Collectors.toList());
		}
	}

	/**
	 * 数据准备
	 * 
	 * @param pollutantName 污染物名称（"SO2", "NO2", "NO", "CO", "O3", "PM2_5", "PM10",
	 *                      "O3_8H"）
	 */
	@Override
	protected void DataPrepare(String pollutantName) {
		// 模型的输入数据
		yPollutant = new double[this.sampleCount];
		xPollutant = new double[this.sampleCount];
		windSpeed = new double[this.sampleCount];
		airTemperature = new double[this.sampleCount];
		humidity = new double[this.sampleCount];

		// 初始数据数据填充
		for (int i = 0; i < this.sampleCount; i++) {
			if (pollutantName.equals(PollutantParam.SO2.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getSo2().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getSo2().doubleValue();
			} else if (pollutantName.equals(PollutantParam.NO2.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getNo2().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getNo2().doubleValue();
			} else if (pollutantName.equals(PollutantParam.CO.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getCo().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getCo().doubleValue();
			} else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getPm2_5().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm2_5().doubleValue();
			} else if (pollutantName.equals(PollutantParam.PM10.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getPm10().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm10().doubleValue();
			} else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
				yPollutant[i] = this.pollutantMonitorList.get(i).getO3_8h().doubleValue();
				xPollutant[i] = this.pollutantMonitorList.get(i + 1).getO3_8h().doubleValue();
			} else {
				yPollutant[i] = 0;
				xPollutant[i] = 0;
			}

			windSpeed[i] = this.weatherMonitorList.get(i).getWindSpeed().doubleValue();
			airTemperature[i] = this.weatherMonitorList.get(i).getAirTemperature().doubleValue();

			// high_AirTemperature[i] = (double)monMetDayDatas[i].High_AirTemp;
			humidity[i] = this.weatherMonitorList.get(i).getHumidity().doubleValue();
		}

		// 预处理
		MultipleRegressionOperate.PreTreatment(yPollutant, this.sampleCount);
		MultipleRegressionOperate.PreTreatment(xPollutant, this.sampleCount);
		MultipleRegressionOperate.PreTreatment(windSpeed, this.sampleCount);
		MultipleRegressionOperate.PreTreatment(airTemperature, this.sampleCount);
		// MultipleRegressionOperate.PreTreatment(high_AirTemperature, sampleCount);
		MultipleRegressionOperate.PreTreatment(humidity, this.sampleCount);
	}
	/**
	 * 模型拟合，模拟成功则返回true
	 * @return
	 */
	private boolean fit(double[][] input, double[] output) {
		MultipleRegressionFittingData mrPollutant = MultipleRegressionOperate.MultipleRegressionFitting(
				input, output, this.factorCount,
				this.sampleCount);
		if (!Double.isNaN(mrPollutant.getR())) {
			// 模型参数系数（按上述给定顺序）
			xPollutant_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[0]);
			airTemperature_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[1]);
			// high_AirTemperature_Coefficient = (decimal)mrPollutant.a[1];
			windSpeed_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[2]);
			humidity_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[3]);
			Intercept_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[4]);

			// 模型性能参数
			Q = BigDecimal.valueOf(mrPollutant.getQ());
//			BigDecimal S = BigDecimal.valueOf(mrPollutant.getS());
//			BigDecimal R = BigDecimal.valueOf(mrPollutant.getR());
//			BigDecimal U = BigDecimal.valueOf(mrPollutant.getU());
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 输入自变量，利用模型进行预测
	 * @param xPollutant
	 * @param airTemperature
	 * @param windSpeed
	 * @param humidity
	 * @return
	 */
	private BigDecimal modelPredict(BigDecimal xPollutant, BigDecimal airTemperature, BigDecimal windSpeed, BigDecimal humidity) {
		return xPollutant_Coefficient.multiply(xPollutant)
				.add(airTemperature_Coefficient.multiply(airTemperature))
				.add(windSpeed_Coefficient.multiply(windSpeed))
				.add(humidity_Coefficient.multiply(humidity))
				.add(Intercept_Coefficient);
	}
	
	/**
	 * 模型评估
	 */
	private void eval() {
		double[] lableArray = new double[xPollutant.length];
		for(int i=0; i<xPollutant.length; i++) {
			lableArray[i] = modelPredict(BigDecimal.valueOf(xPollutant[i]), BigDecimal.valueOf(airTemperature[i]), BigDecimal.valueOf(windSpeed[i]), BigDecimal.valueOf(humidity[i])).doubleValue();
		}
        INDArray labels = Nd4j.create(lableArray);
        INDArray predictions = Nd4j.create(yPollutant);
		RegressionEvaluation eval = new RegressionEvaluation();
		eval.eval(labels, predictions);
//	    System.out.println("评估模型，RSquared:"+eval.averageRSquared());
	}
	
	/**
	 * 预报过程
	 */
	@Override
	protected void Predict() {
		DataCompletionCheck();

		for (String pollutantName : pollutantNames) {
			DataPrepare(pollutantName);
			/// #region 模型拟合
			if (!this.fit(new double[][] { xPollutant, airTemperature, windSpeed, humidity }, yPollutant)) {
				continue;
			}
			eval();

			/// #region 预报数据计算
			ArrayList<PollutantForecastMetaData> result = new ArrayList<PollutantForecastMetaData>();

			// 预报日的各输入参数
			HashMap<String, BigDecimal> predictInput = new HashMap<String, BigDecimal>();
			predictInput.put("airTemperature", new BigDecimal(0));
			// predictInput.Add("high_AirTemperature", 0);
			predictInput.put("windSpeed", new BigDecimal(0));
			predictInput.put("humidity", new BigDecimal(0));
			predictInput.put("xPollutant", new BigDecimal(0));

			int dayindex = 0; // 代表预报日的天数，先预报第一天，故dayindex=0，每预报完一天dayindex+1

			for (LocalDateTime forTime = this.timePoint, endForTime = this.timePoint.plusDays(
					this.forecastDayCount - 1); forTime.compareTo(endForTime) <= 0; forTime = forTime.plusDays(1)) // 7天预报数据，从当前天到后6天
			{
				// 组织预报日的各输入参数
				predictInput.put("airTemperature", this.weatherForecastList.get(dayindex).getAirTemp());
				// predictInput["high_AirTemperature"] = forMetDayDatas[dayindex].High_AirTemp;
				predictInput.put("windSpeed", this.weatherForecastList.get(dayindex).getWindSpeed());
				predictInput.put("humidity", this.weatherForecastList.get(dayindex).getRelativeHumidity());
				if (dayindex == 0) {
					predictInput.put("xPollutant", BigDecimal.valueOf(yPollutant[0]));
				}

				PollutantForecastMetaData fd = new PollutantForecastMetaData();
				fd.setTimePoint(getTimePoint());
				fd.setForTime(forTime);
				fd.setPolllutantName(pollutantName);
				BigDecimal fdValue = modelPredict(predictInput.get("xPollutant"), predictInput.get("airTemperature"), predictInput.get("windSpeed"), predictInput.get("humidity"));
				// fd.Value = Localization.Localize(fd.Value, pollutantName, forTime); //
				// 对预报数值进行本地化
				fd.setValue(MathUtil.roundBank(fdValue.doubleValue(), 3));
				
				BigDecimal floatingValue = BigDecimal.valueOf(Math.sqrt((1 / (this.sampleCount - this.factorCount - 1))) * Q.doubleValue());
				fd.setFloatingValue(MathUtil.roundBank(floatingValue.doubleValue(), 3));

				result.add(fd);

				dayindex++;
				predictInput.put("xPollutant", fd.getValue()); // 把前一天的预报值赋值给下一个预报日的输入参数中的xPollutant
			}
			forMetaDataList.addAll(result);
		}
	}

	/**
	 * 预报过程（没有气象预报数据时所用的备用方法）
	 * 
	 */
	@Override
	protected void BackupPredict() {
		for (String pollutantName : pollutantNames) {
			// 预报用的数据
			yPollutant = new double[this.sampleCount];
			xPollutant = new double[this.sampleCount]; // 前一天污染物浓度

			windSpeed = new double[this.sampleCount]; // 日均风速
			airTemperature = new double[this.sampleCount]; // 日均温度
			// double[] high_AirTemperature = new double[sampleCount]; //日最高温度
			humidity = new double[this.sampleCount]; // 日均相对湿度

			/// #region 初始数据数据填充（仅yPollutant,xPollutant）
			for (int i = 0; i < this.sampleCount; i++) {
				if (pollutantName.equals(PollutantParam.SO2.name())) {
					yPollutant[i] = this.pollutantMonitorList.get(i).getSo2().doubleValue();
					xPollutant[i] = this.pollutantMonitorList.get(i + 1).getSo2().doubleValue();
				} else if (pollutantName.equals(PollutantParam.NO2.name())) {
					yPollutant[i] = this.pollutantMonitorList.get(i).getNo2().doubleValue();
					xPollutant[i] = this.pollutantMonitorList.get(i + 1).getNo2().doubleValue();
				} else if (pollutantName.equals(PollutantParam.CO.name())) {
					yPollutant[i] = this.pollutantMonitorList.get(i).getCo().doubleValue();
					xPollutant[i] = this.pollutantMonitorList.get(i + 1).getCo().doubleValue();
				}else if (pollutantName.equals(PollutantParam.PM2_5.name())) {
					yPollutant[i] = this.pollutantMonitorList.get(i).getPm2_5().doubleValue();
					xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm2_5().doubleValue();
				} else if (pollutantName.equals(PollutantParam.PM10.name())) {
					yPollutant[i] = this.pollutantMonitorList.get(i).getPm10().doubleValue();
					xPollutant[i] = this.pollutantMonitorList.get(i + 1).getPm10().doubleValue();
				} else if (pollutantName.equals(PollutantParam.O3_8H.name())) {
					yPollutant[i] = this.pollutantMonitorList.get(i).getO3_8h().doubleValue();
					xPollutant[i] = this.pollutantMonitorList.get(i + 1).getO3_8h().doubleValue();
				} else {
					yPollutant[i] = 0;
					xPollutant[i] = 0;
				}
			}

			MultipleRegressionOperate.PreTreatment(yPollutant, this.sampleCount);
			MultipleRegressionOperate.PreTreatment(xPollutant, this.sampleCount);

			/// #region 预报数据计算

			// 预报日的各输入参数
			HashMap<String, BigDecimal> predictInput = new HashMap<String, BigDecimal>();
			predictInput.put("airTemperature", getWeatherMonitorList().get(0).getAirTemperature());
			// predictInput.Add("high_AirTemperature", monMetDayDatas[0].);
			predictInput.put("windSpeed", getWeatherMonitorList().get(0).getWindSpeed());
			predictInput.put("humidity", getWeatherMonitorList().get(0).getHumidity());

			ArrayList<PollutantForecastMetaData> result = new ArrayList<PollutantForecastMetaData>();

			// 计算白天的预报数据
			int dayindex = 1; // 代表预报的是第几天
			for (LocalDateTime forTime = this.timePoint, endForTime = this.timePoint.plusDays(this.forecastDayCount - 1); forTime
					.compareTo(endForTime) <= 0; forTime = forTime.plusDays(1)) // 7天预报数据，从当前天到后6天
			{
				PollutantForecastMetaData fd = new PollutantForecastMetaData();
				fd.setTimePoint(getTimePoint());
				fd.setForTime(forTime);
				fd.setPolllutantName(pollutantName);

				// 第一天使用初始拟合的模型
				if (dayindex == 1) {
					predictInput.put("xPollutant", BigDecimal.valueOf(yPollutant[0]));
				}

				/// #region 初始数据数据填充

				for (int i = 0; i < this.sampleCount; i++) {
					// windDirection[i] = (double)monMetDayDatas[i + index].WindDirection;
					windSpeed[i] = this.weatherMonitorList.get(i + dayindex).getWindSpeed().doubleValue();
					airTemperature[i] = this.weatherMonitorList.get(i + dayindex).getAirTemperature().doubleValue();
					humidity[i] = this.weatherMonitorList.get(i + dayindex).getHumidity().doubleValue();
				}

				/// #region 预处理

				// MultipleRegressionOperate.PreTreatment(windDirection, sampleCount);
				MultipleRegressionOperate.PreTreatment(windSpeed, this.sampleCount);
				MultipleRegressionOperate.PreTreatment(airTemperature, this.sampleCount);
				MultipleRegressionOperate.PreTreatment(humidity, this.sampleCount);

				/// #region 模型拟合
				// 定义参数系数
				BigDecimal xPollutant_Coefficient = null;
				BigDecimal airTemperature_Coefficient = null;
				// decimal high_AirTemperature_Coefficient = 0;
				BigDecimal windSpeed_Coefficient = null;
				BigDecimal humidity_Coefficient = null;
				BigDecimal Intercept_Coefficient = null;
				BigDecimal Q = new BigDecimal(0);
				MultipleRegressionFittingData mrPollutant = MultipleRegressionOperate.MultipleRegressionFitting(
						new double[][] { xPollutant, airTemperature, windSpeed, humidity }, yPollutant,
						getFactorCount(), this.sampleCount);
				if (!Double.isNaN(mrPollutant.getR())) {
					// 模型参数系数（按上述给定顺序）
					xPollutant_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[0]);
					airTemperature_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[1]);
					// high_AirTemperature_Coefficient = (decimal)mrPollutant.a[1];
					windSpeed_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[2]);
					humidity_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[3]);
					Intercept_Coefficient = BigDecimal.valueOf(mrPollutant.getA()[4]);
					// 模型性能参数
					Q = BigDecimal.valueOf(mrPollutant.getQ());
//					BigDecimal S = BigDecimal.valueOf(mrPollutant.getS());
//					BigDecimal R = BigDecimal.valueOf(mrPollutant.getR());
//					BigDecimal U = BigDecimal.valueOf(mrPollutant.getU());
				} else {
					continue;
				}

				fd.setTimePoint(getTimePoint());
				fd.setForTime(forTime);
				fd.setPolllutantName(pollutantName);
				BigDecimal fdValue = xPollutant_Coefficient.multiply(predictInput.get("xPollutant"))
						.add(airTemperature_Coefficient.multiply(predictInput.get("airTemperature")))
						.add(windSpeed_Coefficient.multiply(predictInput.get("windSpeed")))
						.add(humidity_Coefficient.multiply(predictInput.get("humidity")).add(Intercept_Coefficient));
				// fd.Value = Localization.Localize(fd.Value, pollutantName, forTime); //
				// 对预报数值进行本地化
				fd.setValue(MathUtil.roundBank(fdValue.doubleValue(), 3));
				BigDecimal floatingValue = BigDecimal.valueOf(Math.sqrt((1 / (this.sampleCount - this.factorCount - 1))) * Q.doubleValue());
				fd.setFloatingValue(MathUtil.roundBank(floatingValue.doubleValue(), 3));

				result.add(fd);

				dayindex++;
				predictInput.put("xPollutant", fd.getValue()); // 把前一天的预报值赋值给下一个预报日的输入参数中的xPollutant
			}
			forMetaDataList.addAll(result);
		}
	}

	public double[] getyPollutant() {
		return yPollutant;
	}

	public void setyPollutant(double[] yPollutant) {
		this.yPollutant = yPollutant;
	}

	public double[] getxPollutant() {
		return xPollutant;
	}

	public void setxPollutant(double[] xPollutant) {
		this.xPollutant = xPollutant;
	}

	public double[] getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(double[] windSpeed) {
		this.windSpeed = windSpeed;
	}

	public double[] getAirTemperature() {
		return airTemperature;
	}

	public void setAirTemperature(double[] airTemperature) {
		this.airTemperature = airTemperature;
	}

	public double[] getHumidity() {
		return humidity;
	}

	public void setHumidity(double[] humidity) {
		this.humidity = humidity;
	}
}