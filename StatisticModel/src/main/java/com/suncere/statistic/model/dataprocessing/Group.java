package com.suncere.statistic.model.dataprocessing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 每个分类的类
 */
public class Group {
	public ArrayList<HashMap<String, BigDecimal>> data = new ArrayList<HashMap<String, BigDecimal>>();
	public HashMap<String, BigDecimal> groupCenter = new HashMap<String, BigDecimal>();
	public int sampleCount = 0;

	public Group() {
	}

	public Group(HashMap<String, BigDecimal> center) {
		groupCenter = center;
	}

	public final void Add(HashMap<String, BigDecimal> x) {
		data.add(x);
		sampleCount = data.size();
	}

	public final void Add(ArrayList<HashMap<String, BigDecimal>> x) {
		for (int i = 0; i < x.size(); i++) {
			data.add(x.get(i));
		}
		sampleCount = data.size();
	}

	public final void Clear() {
		data.clear();
		sampleCount = 0;
	}

	// 计算类的中心
	public final void CalculateGroupCenter() {
		if (sampleCount == 0) {
			return;
		} else {
			ArrayList<String> factorList = new ArrayList<String>();
			for (String key : data.get(0).keySet()) {
				factorList.add(key);
			}

			for (String key : factorList) {
				BigDecimal averge = new BigDecimal(0);
				for (int r = 0; r < sampleCount; r++) {
					averge = averge.add(data.get(r).get(key));
				}
				averge = averge.divide(BigDecimal.valueOf(sampleCount),10, BigDecimal.ROUND_HALF_EVEN);
				if (groupCenter.isEmpty()) {
					groupCenter.put(key, averge);
				} else {
					groupCenter.put(key, averge);
				}
			}
		}
	}

	// 将一个分类合并到当前分类中（合并后的分类与被合并的分类additional_group不存在引用关系）
	public final void MergeGroup(Group additional_group) {
		Group cloneGroup = clone(additional_group);
		Add(cloneGroup.data);
		CalculateGroupCenter();
	}

	/// #region Group深复制
	/**
	 * 复制一个Group
	 * 
	 */
	public static Group clone(Group input) {
		Group output = new Group();
		for (int i = 0; i < input.sampleCount; i++) {
			output.Add(clone(input.data.get(i)));
		}
		output.groupCenter = clone(input.groupCenter);
		return output;
	}

	/**
	 * 作用为复制一个成为类中心的样本
	 * 
	 */
	public static HashMap<String, BigDecimal> clone(HashMap<String, BigDecimal> input) {
		HashMap<String, BigDecimal> output = new HashMap<String, BigDecimal>();
		for (String key : input.keySet()) {
			output.put(key, input.get(key));
		}
		return output;
	}

}