package com.suncere.statistic.serivce.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suncere.statistic.entity.CityMonitorDayPollutant;
import com.suncere.statistic.mapper.CityMonitorDayPollutantMapper;
import com.suncere.statistic.serivce.CityMonitorDayPollutantService;
import com.suncere.statistic.serivce.po.CityMonitorDayPollutantPo;
import com.suncere.statistic.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CityMonitorDayPollutantServiceImpl
			extends ServiceImpl<CityMonitorDayPollutantMapper, CityMonitorDayPollutant> 
				implements CityMonitorDayPollutantService{

	@Autowired
	private CityMonitorDayPollutantMapper ctyMonitorDayPollutantMapper;
//	@Autowired
//	private CityMonitorDayPollutantMapper ctyMonitorDayPollutantMapper;
//	/** 
//	 *  获取城市的污染物监测日均值
//	 * @param beginTime 开始日期
//	 * @param endTime 结束日期
//	 * @return 城市的污染物监测日均值数据列表
//	 */
//	public static ArrayList<CityMonitorDayPollutant> GetListBy(Date beginTime, Date endTime)
//	{
////C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
////		using (EntitiesModel dbContext = new EntitiesModel())
//		EntitiesModel dbContext = new EntitiesModel();
//		try
//		{
////C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//			list = dbContext.getCity_Monitor_Day_Pollutants().Where(o => o.TimePoint.Date >= beginTime.Date && o.TimePoint.Date <= endTime.Date).OrderBy(o => o.TimePoint).ToList();
//		}
//		finally
//		{
//			dbContext.dispose();
//		}
//		return list;
//	}
//
//	/** 
//	 获取城市污染物日均监测数据
//	 
//	 @param timePoint 日期
//	 @return 
//	*/
//	public static CityMonitorDayPollutant GetCityMonitorDayPollutant(Date timePoint)
//	{
////C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
////		using (EntitiesModel dbContext = new EntitiesModel())
//		EntitiesModel dbContext = new EntitiesModel();
//		try
//		{
////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//			var pollutantData = dbContext.getCity_Monitor_Day_Pollutants().FirstOrDefault(o => timePoint.equals(o.TimePoint));
//			return pollutantData;
//		}
//		finally
//		{
//			dbContext.dispose();
//		}
//	}
//
//	/** 
//	 获取城市的污染物监测日均值
//	 
//	 @param beginTime 开始日期
//	 @param endTime 结束日期
//	 @return 城市的污染物监测日均值数据列表
//	*/
//	public static ArrayList<CityMonitorDayPollutant> GetListBy(Date beginTime, Date endTime)
//	{
//		ArrayList<CityMonitorDayPollutant> list;
////C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
////		using (EntitiesModel dbContext = new EntitiesModel())
//		EntitiesModel dbContext = new EntitiesModel();
//		try
//		{
//			if (beginTime == null || endTime == null)
//			{
//				list = new ArrayList<CityMonitorDayPollutant>();
//			}
//			else
//			{
////C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//				list = dbContext.getCity_Monitor_Day_Pollutants().Where(o => o.TimePoint.Date >= beginTime.getValue().Date && o.TimePoint.Date <= endTime.getValue().Date).OrderBy(o => o.TimePoint).ToList();
//			}
//		}
//		finally
//		{
//			dbContext.dispose();
//		}
//		return list;
//	}

	@Override
	public ArrayList<CityMonitorDayPollutant> GetDayDataCompletion(String cityCode, LocalDateTime startTime,
			LocalDateTime endTime, BigDecimal defaultValue) {
		ArrayList<CityMonitorDayPollutant> compDatas = new ArrayList<CityMonitorDayPollutant>();

		CityMonitorDayPollutantPo po = new CityMonitorDayPollutantPo();
		po.setStartTime(startTime);
		po.setEndTime(endTime);
		po.setCityCode(cityCode);
//		List<CityMonitorDayPollutant> entityList = ctyMonitorDayPollutantMapper.list(po);

		//用city_aqi_history_day_publish表的数据
		List<CityMonitorDayPollutant> entityList = ctyMonitorDayPollutantMapper.listCityAqiHistoryDayPublish(DateUtils.asDate(startTime),DateUtils.asDate(endTime),cityCode);

		for (LocalDateTime t = startTime; t.compareTo(endTime) <= 0; t = t.plusDays(1)) {
			CityMonitorDayPollutant day = null;
			if (entityList != null) {
				LocalDateTime tt = t;
				day = entityList.stream().filter(p -> DateUtils.isSameDay(tt, p.getTimePoint()))
						.findFirst().orElse(null);
			}
			CityMonitorDayPollutant dat;
			if (day == null) {
				dat = new CityMonitorDayPollutant();
				dat.setCityCode(cityCode);
				dat.setTimePoint(t);
				dat.setCreateTime(LocalDateTime.now());
				dat.setSo2(defaultValue);
				dat.setNo2(defaultValue);
				dat.setCo(defaultValue);
				dat.setPm2_5(defaultValue);
				dat.setPm10(defaultValue);
				dat.setO3_8h(defaultValue);
			} else {
				dat = new CityMonitorDayPollutant();
				dat.setCityCode(day.getCityCode());
				dat.setTimePoint(day.getTimePoint());
				dat.setCreateTime(day.getCreateTime());
				dat.setSo2(day.getSo2() == null ? defaultValue : day.getSo2());
				dat.setNo2(day.getNo2() == null ? defaultValue : day.getNo2());
				dat.setCo(day.getCo() == null ? defaultValue : day.getCo());
				dat.setPm2_5(day.getPm2_5() == null ? defaultValue : day.getPm2_5());
				dat.setPm10(day.getPm10() == null ? defaultValue : day.getPm10());
				dat.setO3_8h(day.getO3_8h() == null ? defaultValue : day.getO3_8h());
			}
				compDatas.add(dat);
		}
		return compDatas;
	}

}