package com.suncere.statistic.serivce.enums;

public enum PollutantParam {

	SO2_1H, SO2, NO2_1H, NO2, PM10, CO, CO_1H, O3_1H, O3_8H, PM2_5, NO, NOx;
	private PollutantParam() {
	}
}