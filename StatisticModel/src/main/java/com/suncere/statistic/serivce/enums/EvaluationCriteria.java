package com.suncere.statistic.serivce.enums;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2019年12月31日
 */
public enum EvaluationCriteria {
	AQI(1),
	API(2);
	
	private int value;

	private EvaluationCriteria(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
