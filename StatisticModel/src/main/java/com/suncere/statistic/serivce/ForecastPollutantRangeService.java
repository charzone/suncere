package com.suncere.statistic.serivce;

import java.util.ArrayList;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suncere.statistic.entity.ForecastPollutantRange;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2020年1月8日
 */
public interface ForecastPollutantRangeService extends IService<ForecastPollutantRange> {

	ArrayList<ForecastPollutantRange> GetList();

}
