package com.suncere.statistic.serivce.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suncere.statistic.entity.CityWeatherForeastInfo;
import com.suncere.statistic.mapper.CityWeatherForeastInfoMapper;
import com.suncere.statistic.serivce.CityWeatherForeastInfoService;
import com.suncere.statistic.serivce.po.CityWeatherForeastInfoPo;
import com.suncere.statistic.utils.DateUtils;

@Service
public class CityWeatherForeastInfoServiceImpl
		extends ServiceImpl<CityWeatherForeastInfoMapper, CityWeatherForeastInfo>
			implements CityWeatherForeastInfoService{

	@Autowired
	private CityWeatherForeastInfoMapper cityWeatherForeastInfoMapper;

	private BigDecimal abnormalValue = new BigDecimal(-1000);

	@Override
	public ArrayList<CityWeatherForeastInfo> GetDayForDataCompletion(String cityCode, LocalDateTime baseTime,
			BigDecimal defaultValue) {
		ArrayList<CityWeatherForeastInfo> compDatas = new ArrayList<CityWeatherForeastInfo>();
		CityWeatherForeastInfoPo po = new CityWeatherForeastInfoPo();
		po.setCityCode(cityCode);
		po.setTimePoint(DateUtils.localDateTimeToDate(baseTime));
		List<CityWeatherForeastInfo> entityList = cityWeatherForeastInfoMapper.list(po);

		for (LocalDateTime t = baseTime; t.compareTo(baseTime.plusDays(6)) <= 0; t = t.plusDays(1)) {

			CityWeatherForeastInfo day = null;
			if (entityList != null) {
				LocalDateTime tt = t;
				day = entityList.stream().filter(entity -> DateUtils.isSameDay(tt, DateUtils.exchangeToLocalDateTime(entity.getTimePoint().getTime()))).findFirst()
						.orElse(null);
			}

			if (day == null) {
				CityWeatherForeastInfo emp = new CityWeatherForeastInfo();
				emp.setTimePoint(DateUtils.localDateTimeToDate(baseTime));
				emp.setForTime(DateUtils.localDateTimeToDate(t));
				emp.setCreateTime(DateUtils.localDateTimeToDate(LocalDateTime.now()));
				emp.setCityName(null);
				emp.setCityCode(cityCode);
				emp.setAirTemp(defaultValue);
				emp.setHighAirTemp(defaultValue);
				emp.setLowHighAirTemp(defaultValue);
				emp.setDayCondition(null);
				emp.setNightCondition(null);
				emp.setDayWindDirection(null);
				emp.setNightWindDirection(null);
				emp.setDayWindSpeed(null);
				emp.setNightWindSpeed(null);
				emp.setWindDirection(defaultValue);
				emp.setWindSpeed(defaultValue);
				emp.setAirPress(defaultValue);
				emp.setRelativeHumidity(defaultValue);
				emp.setCloundCover(defaultValue);
				emp.setVisibility(defaultValue);
				emp.setRainFall(defaultValue);
				compDatas.add(emp);
			} else {
				CityWeatherForeastInfo dat = new CityWeatherForeastInfo();
				dat.setTimePoint(DateUtils.localDateTimeToDate(baseTime));
				dat.setForTime(DateUtils.localDateTimeToDate(t));
				dat.setCreateTime(new Date());
				dat.setCityName(null);
				dat.setCityCode(cityCode);
				dat.setAirTemp((day.getAirTemp() == null || abnormalValue.equals(day.getAirTemp())) ? defaultValue
						: day.getAirTemp());
				dat.setHighAirTemp(
						(day.getHighAirTemp() == null || abnormalValue.equals(day.getHighAirTemp())) ? defaultValue
								: day.getHighAirTemp());
				dat.setLowHighAirTemp((day.getLowHighAirTemp() == null || abnormalValue.equals(day.getLowHighAirTemp()))
						? defaultValue
						: day.getLowHighAirTemp());
				dat.setDayCondition(day.getDayCondition());
				dat.setNightCondition(day.getNightCondition());
				dat.setDayWindDirection(
						(day.getDayWindDirection() == null || abnormalValue.equals(day.getDayWindDirection()))
								? defaultValue
								: day.getDayWindDirection());
				;
				dat.setNightWindDirection(
						(day.getNightWindDirection() == null || abnormalValue.equals(day.getNightWindDirection()))
								? defaultValue
								: day.getNightWindDirection());
				dat.setDayWindSpeed((day.getDayWindSpeed() == null) ? null : day.getDayWindSpeed());
				dat.setNightWindSpeed(day.getNightWindSpeed());
				dat.setWindDirection(
						(day.getWindDirection() == null || abnormalValue.equals(day.getWindDirection())) ? defaultValue
								: day.getWindDirection());
				dat.setWindSpeed((day.getWindSpeed() == null || abnormalValue.equals(day.getWindSpeed())) ? defaultValue
						: day.getWindSpeed());
				dat.setAirPress((day.getAirPress() == null || abnormalValue.equals(day.getAirPress())) ? defaultValue
						: day.getAirPress());
				dat.setRelativeHumidity(
						(day.getRelativeHumidity() == null || abnormalValue.equals(day.getRelativeHumidity()))
								? defaultValue
								: day.getRelativeHumidity());
				dat.setCloundCover(
						(day.getCloundCover() == null || abnormalValue.equals(day.getCloundCover())) ? defaultValue
								: day.getCloundCover());
				dat.setVisibility(
						(day.getVisibility() == null || abnormalValue.equals(day.getVisibility())) ? defaultValue
								: day.getVisibility());
				dat.setRainFall((day.getRainFall() == null || abnormalValue.equals(day.getRainFall())) ? defaultValue
						: day.getRainFall());
				compDatas.add(dat);
			}
		}
		return compDatas;
	}

//	/** 
//	 获取最新天气预报数据
//	 
//	 @return 
//	*/
//	public static ArrayList<CityWeatherForeastInfo> GetLastestData()
//	{
//		ArrayList<CityWeatherForeastInfo> list = new ArrayList<CityWeatherForeastInfo>();
////C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
////		using (EntitiesModel dbContext = new EntitiesModel())
//		EntitiesModel dbContext = new EntitiesModel();
//		try
//		{
////C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//			var lastestDate = dbContext.getCity_WeatherForeastInfos().Max(o => o.Timepoint);
////C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//			list = dbContext.getCity_WeatherForeastInfos().Where(o => o.Timepoint == lastestDate).ToList();
//		}
//		finally
//		{
//			dbContext.dispose();
//		}
//		return list;
//	}
//
//
//
//	/** 
//	 获取气象预报数据
//	 
//	 @param timePoint 日期
//	 @return 
//	*/
//	public static ArrayList<City_WeatherForeastInfo> GetListBy(Date timePoint)
//	{
//		ArrayList<City_WeatherForeastInfo> list = new ArrayList<City_WeatherForeastInfo>();
////C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
////		using (EntitiesModel dbContext = new EntitiesModel())
//		EntitiesModel dbContext = new EntitiesModel();
//		try
//		{
////C# TO JAVA CONVERTER TODO TASK: There is no Java equivalent to LINQ queries:
////C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
//			list = dbContext.getCity_WeatherForeastInfos().Where(o => timePoint.equals(o.Timepoint)).ToList();
//		}
//		finally
//		{
//			dbContext.dispose();
//		}
//		return list;
//	}

}