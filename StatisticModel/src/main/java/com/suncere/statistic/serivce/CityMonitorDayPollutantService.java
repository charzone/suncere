package com.suncere.statistic.serivce;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suncere.statistic.entity.CityMonitorDayPollutant;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2020年1月8日
 */
public interface CityMonitorDayPollutantService extends IService<CityMonitorDayPollutant> {

	/**
	 * 获取城市污染物日均值数据，并对缺失数据进行缺省值填充
	 * 
	 * @param cityCode     城市编号
	 * @param startTime
	 * @param endTime
	 * @param defaultValue
	 * @return
	 */
	ArrayList<CityMonitorDayPollutant> GetDayDataCompletion(String cityCode, LocalDateTime startTime,
			LocalDateTime endTime, BigDecimal defaultValue);

}
