package com.suncere.statistic.serivce.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suncere.statistic.entity.CityMonitorDayWeather;
import com.suncere.statistic.mapper.CityMonitorDayWeatherMapper;
import com.suncere.statistic.serivce.CityMonitorDayWeatherService;
import com.suncere.statistic.serivce.po.CityMonitorDayWeatherPo;
import com.suncere.statistic.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CityMonitorDayWeatherServiceImpl
		extends ServiceImpl<CityMonitorDayWeatherMapper, CityMonitorDayWeather>
			implements CityMonitorDayWeatherService{

	@Autowired
	private CityMonitorDayWeatherMapper cityMonitorDayWeatherMapper;

	@Override
	public ArrayList<CityMonitorDayWeather> GetDayMonDataCompletion(String cityCode, LocalDateTime startTime, LocalDateTime endTime,
			java.math.BigDecimal defaultValue) {
		ArrayList<CityMonitorDayWeather> compDatas = new ArrayList<CityMonitorDayWeather>();
		CityMonitorDayWeatherPo po = new CityMonitorDayWeatherPo();
		po.setStartTime(startTime);
		po.setEndTime(endTime);
		po.setCityCode(cityCode);
		List<CityMonitorDayWeather> entityList = cityMonitorDayWeatherMapper.list(po);
		for (LocalDateTime t = startTime; t.compareTo(endTime) <= 0; t = t.plusDays(1)) {
			CityMonitorDayWeather day = null;
			if (entityList != null) {
				//获取不重复的城市天气数据
				LocalDateTime tt = t;
				day = entityList.stream().filter(entity -> DateUtils.isSameDay(tt, DateUtils.exchangeToLocalDateTime(entity.getTimePoint().getTime()))).findFirst().orElse(null);
			}

			if (day == null) {
				CityMonitorDayWeather emp = new CityMonitorDayWeather();
				emp.setCityCode(cityCode);
				emp.setTimePoint(DateUtils.localDateTimeToDate(t));
				emp.setCreateTime(DateUtils.localDateTimeToDate(LocalDateTime.now()));
				emp.setWindDirection(defaultValue);
				emp.setWindSpeed(defaultValue);
				emp.setPressure(defaultValue);
				emp.setAirTemperature(defaultValue);
				emp.setHumidity(defaultValue);
				emp.setRainfall(defaultValue);
				emp.setApparent(defaultValue);
				compDatas.add(emp);
			} else {
				CityMonitorDayWeather dat = new CityMonitorDayWeather();
				dat.setCityCode(day.getCityCode());
				dat.setTimePoint(day.getTimePoint());
				dat.setCreateTime(day.getCreateTime());
				dat.setWindDirection(day.getWindDirection() == null ? defaultValue : day.getWindDirection());
				dat.setWindSpeed(day.getWindSpeed() == null ? defaultValue : day.getWindSpeed());
				dat.setPressure(day.getPressure() == null ? defaultValue : day.getPressure());
				dat.setAirTemperature(day.getAirTemperature() == null ? defaultValue : day.getAirTemperature());
				dat.setHumidity(day.getHumidity() == null ? defaultValue : day.getHumidity());
				dat.setRainfall(day.getRainfall() == null ? defaultValue : day.getRainfall());
				dat.setApparent(day.getApparent() == null ? defaultValue : day.getApparent());
				compDatas.add(dat);
			}

		}

		return compDatas;
	}
}