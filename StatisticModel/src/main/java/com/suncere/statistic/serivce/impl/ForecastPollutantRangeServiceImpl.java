package com.suncere.statistic.serivce.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suncere.statistic.entity.ForecastPollutantRange;
import com.suncere.statistic.mapper.ForecastPollutantRangeMapper;
import com.suncere.statistic.serivce.ForecastPollutantRangeService;

@Service
public class ForecastPollutantRangeServiceImpl
		extends ServiceImpl<ForecastPollutantRangeMapper, ForecastPollutantRange>
			implements ForecastPollutantRangeService{

	@Autowired
	private ForecastPollutantRangeMapper forecastPollutantRangeMapper;

	@Override
	public ArrayList<ForecastPollutantRange> GetList() {
		return forecastPollutantRangeMapper.list();
	}
}