package com.suncere.statistic.serivce;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suncere.statistic.entity.CityWeatherForeastInfo;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2020年1月8日
 */
public interface CityWeatherForeastInfoService extends IService<CityWeatherForeastInfo> {

	/**
	 * 获取某天发布的7天的气象预报数据
	 * 
	 * @param cityCode
	 * @param baseTime
	 * @param defaultValue 缺省值
	 * @return
	 */
	ArrayList<CityWeatherForeastInfo> GetDayForDataCompletion(String cityCode, LocalDateTime baseTime,
			BigDecimal defaultValue);

}
