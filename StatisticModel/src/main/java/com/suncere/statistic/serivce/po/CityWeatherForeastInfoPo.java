package com.suncere.statistic.serivce.po;

import java.time.LocalDateTime;

import com.suncere.statistic.entity.CityWeatherForeastInfo;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2019年12月25日
 */
public class CityWeatherForeastInfoPo extends CityWeatherForeastInfo {

	private static final long serialVersionUID = -4019048425572688332L;
	private LocalDateTime baseTime;
	public LocalDateTime getBaseTime() {
		return baseTime;
	}
	public void setBaseTime(LocalDateTime baseTime) {
		this.baseTime = baseTime;
	}
}
