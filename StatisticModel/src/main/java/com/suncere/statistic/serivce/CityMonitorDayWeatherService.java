package com.suncere.statistic.serivce;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suncere.statistic.entity.CityMonitorDayWeather;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2020年1月8日
 */
public interface CityMonitorDayWeatherService extends IService<CityMonitorDayWeather> {
	/**
	 * 获取城市气象参数的监测数据
	 * 
	 * @param stationCode
	 * @param startTime
	 * @param endTime
	 * @param defaultValue 缺省值
	 * @return
	 */
	ArrayList<CityMonitorDayWeather> GetDayMonDataCompletion(String cityCode, LocalDateTime startTime,
			LocalDateTime endTime, BigDecimal defaultValue);

}
