package com.suncere.statistic.serivce.po;

import java.time.LocalDateTime;

import com.suncere.statistic.entity.CityMonitorDayPollutant;

/**
 * <p>
 * 
 * </p>
 *
 * @author panxinkun
 * @date 2019年12月25日
 */
public class CityMonitorDayPollutantPo extends CityMonitorDayPollutant {

	private static final long serialVersionUID = -4019048425572688332L;
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	public LocalDateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	public LocalDateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
}
